package parsers.intrf;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

import types.Parameter;

public class ParmeterSearchObject {
	public String methodName;
	public Class<?> paramClass;
	public List<String> namePath;
	public Map<Class<?>, Constructor<?>> indexList;
	public Map<Class<?>, Parameter> paramMap;
	public int index;

	public ParmeterSearchObject(String methodName,
			Class<?> paramClass, List<String> namePath,
			Map<Class<?>, Constructor<?>> indexList,
			Map<Class<?>, Parameter> paramMap, int index) {
		this.methodName = methodName;
		this.paramClass = paramClass;
		this.namePath = namePath;
		this.indexList = indexList;
		this.paramMap = paramMap;
		this.index = index;
	}
}