package parsers.intrf;

import exceptions.Err;
import generator.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import options.Options;
import parsers.IParser;
import parsers.ParsedAttributes;
import types.Connection;
import types.Interface;
import types.Parameter;
import types.Type;
import types.intrf.CompositeParameter;
import types.intrf.PLAINInput;
import types.intrf.PLAINInterface;
import types.intrf.PLAINOutput;
import util.JavaUnitCompiler;

public class InterfaceParser implements IParser{
	private String parsedUnit = null;
	private String resetName = null;
	private Class<?> interfaceClass = null;
	int index = 0;
	private String classPath;
	private String buildPath;

	public InterfaceParser() {

	}

	public Interface parseClass(Class<?> klass) {
		Log.log("Parsing class "+klass.getCanonicalName());
		List<Connection> connections = new ArrayList<Connection>();
		
		for(Method method : klass.getDeclaredMethods()) {
			if(methodFilter(method)) { 
				Connection connection = parseMethod(method);
				connections.add(connection);
			}
		}
		Interface intrf = new PLAINInterface(klass.getSimpleName(), connections);
		return intrf;
	}
	
	private boolean methodFilter(Method method) {
		int modifiers = method.getModifiers();
		return !Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers);
	}
	
	private Connection parseMethod(Method method) {
		Log.log("Parsing method "+method.getName());
		PLAINInput inputMessage = getInputMessage(method);
		PLAINOutput outputMessage = getOutputMessage(method);
		return new Connection(inputMessage, outputMessage);
	}
	
	//only works with declared fields
	private PLAINOutput getOutputMessage(Method method) {
		index = 0;
		Class<?> returnType = method.getReturnType();
		types.Type estimatedReturnType = parseType(returnType);
		String messageName = "ret"+method.getName();
		PLAINOutput outputMessage = null;
		if(estimatedReturnType == types.Type.VOID)
		{
			outputMessage = new PLAINOutput(messageName);
		} else
		{
			Parameter param = constructOutputParameter(method.getName(), returnType);
			if(param == null) {
				Err.handleInterfaceParseException(returnType);
			}
			outputMessage = new PLAINOutput(messageName, param);
		}
		return outputMessage;
	}
	
	private PLAINInput getInputMessage(Method method) {
		index = 0;
		List<Parameter> parameters = new ArrayList<Parameter>();
		for (Class<?> paramType : method.getParameterTypes()) {
			Parameter param = constructInputParameter(method.getName(), paramType);
			if(param == null) {
				Err.handleInterfaceParseException(paramType);
			}
			parameters.add(param);
		}
		
		return new PLAINInput(method.getName(), parameters);
	}
	
	private Parameter constructInputParameter(String methodName, Class<?> klass) {
		ArrayList<String> namePath = new ArrayList<String>();
		namePath.add(klass.getSimpleName());
		return constructInputParameter(new ParmeterSearchObject(methodName, klass, namePath,
				new HashMap<Class<?>,Constructor<?>>(), new HashMap<Class<?>, Parameter>(), 0));
	}
	
	private Parameter constructOutputParameter(String methodName, Class<?> returnKlass) {
		ArrayList<String> namePath = new ArrayList<String>();
		namePath.add(returnKlass.getSimpleName());
		return constructOutputParameter(methodName, returnKlass, new ArrayList<Class<?>>(), namePath);
	}
	
	private Parameter constructOutputParameter(String methodName, Class<?> visitedClass, List<Class<?>> visitedClasses, List<String> namePath) {
		Type classType = parseType(visitedClass);
		if(visitedClasses.contains(visitedClass) == true && classType == Type.COMPOSITE) {
			return null;
		}
		if(classType != Type.COMPOSITE) {
			Parameter param = new Parameter(createParamName(methodName, false, namePath, index), classType, index);
			//visitedClasses.add(visitedClass);
			index ++;
			return param;
		}
		//if(hasAllFieldsValid(visitedClass)) {
			visitedClasses.add(visitedClass);
			List<Parameter> params = new ArrayList<Parameter>();
			boolean isValid = true;
			for(Field field : filterInvalidFields(visitedClass)) {
				if(isValid(field) == true) {
				Class<?> nextVisitedClass = field.getType();
				namePath.add(nextVisitedClass.getSimpleName());
				Parameter param = constructOutputParameter(methodName, nextVisitedClass, visitedClasses, namePath);
				namePath.remove(nextVisitedClass.getSimpleName());
				if(param == null) {
					isValid = false;
					break;
				} else {
					params.add(param);
				}
				}
			}	
			if(isValid == true) {
				String paramName = createParamName(methodName, true, namePath, 0);
				Parameter param = new CompositeParameter(paramName, visitedClass, 0, params);
				visitedClasses.add(visitedClass);
				return param;
			}
		//}
		return null;
	}
	
	private List<Field> filterInvalidFields(Class<?> klass) {
		List<Field> validFields = new ArrayList<Field>();
		for(Field field : klass.getDeclaredFields()) {
			if(isValid(field)) {
				validFields.add(field);
			}
		}
		return validFields;
	}
	
	private boolean isValid(Field field) {
		int modifiers = field.getModifiers();
		boolean isValid = !Modifier.isAbstract(modifiers) && !Modifier.isFinal(modifiers);
		return isValid;
	}

	
	private Parameter constructInputParameter(ParmeterSearchObject paramObj) {
		if(paramObj.indexList.containsKey(paramObj.paramClass) && paramObj.indexList.get(paramObj.paramClass) != null) {
			return null;
		}
		if(parseType(paramObj.paramClass) != Type.COMPOSITE) {
			Parameter param = new Parameter(createParamName(paramObj.methodName, true, paramObj.namePath, index), parseType(paramObj.paramClass), index);
			paramObj.paramMap.put(paramObj.paramClass, param);
			index++;
			return param;
		}
		List<Parameter> params = new ArrayList<Parameter>();
		for(Constructor<?> constructor : paramObj.paramClass.getConstructors()) {
			if(isValid(constructor) ) {
				paramObj.indexList.put(paramObj.paramClass, constructor);
				boolean isValid = true;
				params.clear();
				for(Class<?> klass : constructor.getParameterTypes()) {
					paramObj.namePath.add(klass.getSimpleName());
					Parameter param = constructInputParameter(new ParmeterSearchObject(
							paramObj.methodName, klass,
							paramObj.namePath,
							paramObj.indexList, paramObj.paramMap, index));
					
					paramObj.namePath.remove(klass.getSimpleName());
					if(param == null) {
						 isValid = false;
						 break;
					 } 
					else {
						 params.add(param);	 
					 }
					 
				}
				if(isValid == false) {
					continue;
				}
					String paramName = createParamName(paramObj.methodName, true, paramObj.namePath, 0);
					Parameter param = new CompositeParameter(paramName, paramObj.paramClass, 0, params);
					paramObj.paramMap.put(paramObj.paramClass, param);
					return param;
				} 
			}
		return null;
	}
	
	public String createParamName(String methodName, boolean isInput ,List<String> namePath, int index) {
		String name = (isInput ?"pin" : "pout") + methodName;
		for(String className : namePath) {
			name = name + "_"+className.toLowerCase();
		} 
		name = name + index;
		return name;
	}

	private boolean isValid(Constructor<?> constructor) {
		boolean result = true;
		for(Class<?> ctorParamType : constructor.getParameterTypes()) {
			if(!isValid(ctorParamType)) {
				result = false;
				break;
			}
		}
		return result;
	}
	
	private boolean isValid(Class <?> klass) {
		return !Modifier.isAbstract(klass.getModifiers()) || klass.isPrimitive();
	}
	private types.Type parseType(Class <?> klass) {
		types.Type type = types.Type.INT;
		if(klass != null) {
			if(klass.isPrimitive()) {
				return type;
			}
			else 
			{
				if(! Number.class.isAssignableFrom(klass) ) {
					if(String.class == klass) {
						type = types.Type.STRING;
					} 
					else {
						type = types.Type.COMPOSITE;
					}
				}
			}
		} else {
			type = types.Type.VOID;
		}
		return type;
	}
	
	public void loadOptions(Options options) throws Exception {
		switch (options.selectedTechnology) {
		case PLAIN:
			this.parsedUnit = options.plainOptions.className;
			this.resetName = options.plainOptions.resetName;
			this.classPath = options.plainOptions.classPath;
			this.buildPath = options.globalOptions.buildPath;
			break;
		case RMI:
			this.parsedUnit = options.rmiOptions.interfaceName;
			this.resetName = options.rmiOptions.resetName;
			this.classPath = options.rmiOptions.classPath;
			this.buildPath = options.globalOptions.buildPath;
			break;
		default:
			Log.err("Technology "+ options.selectedTechnology + " has not been implemented");
			break;
		}
	}
	
	public Interface parse(Options options) throws Exception {
		loadOptions(options);
		JavaUnitCompiler compiler = new JavaUnitCompiler();
		interfaceClass = compiler.compileJavaUnit(parsedUnit, classPath, buildPath);
		Interface intrf = parseClass(interfaceClass);
		if(resetName != null) {
			intrf.removeConnection(resetName);
		}
		intrf.parsedAttributes.put(ParsedAttributes.COMPILED_CLASS, interfaceClass);
		return intrf;
	}

}
