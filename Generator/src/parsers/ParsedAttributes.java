package parsers;

/**
 * The ParsedAttributes class was originally intended to maintain enumerations used to store information retrieved by the various Parsers that is required
 * further in the generation process. The enumerations could then be associated to either learners or technologies. 
 * <p>
 * As we only need to story one object so far, we commented out the inner enumerations in favor of making the outer class an enumeration.  
 *
 */
public enum ParsedAttributes {
	COMPILED_CLASS
//	public static enum RMI {
//		INTERFACE_CLASS
//	}
//	public static enum PLAIN {
//		INSTANCE_CLASS
//	}
}
