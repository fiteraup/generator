package parsers;

import options.Options;
import types.Interface;

/**
 * Parser interface. Given a set of options describing the sut, its interface is analyzed and parsed into an {@link types.Interface} object. Thus, we achieve partial
 * unification of the different types of sut possible. We say partial because technology specific elements of the Interface might still be needed/used
 * by the generation template classes.
 */
public interface IParser {
	public Interface parse(Options options) throws Exception;
}
