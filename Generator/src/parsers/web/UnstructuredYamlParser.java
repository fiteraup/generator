package parsers.web;

import exceptions.Err;
import generator.ILoc;
import generator.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import parsers.IParser;
import types.Parameter;
import types.http.HTTPAction;
import types.http.HTTPAuthentication;
import types.http.HTTPKeyValuePair;
import types.http.HTTPPage;

/***
 * @author Paul
 * A parser designed to load request and page descriptions, that will be used subsequently
 * to generate web drivers. The grammar implemented by the Yaml Web Configuration file is
 * as follows:
 * pages:
 *  page1_name: distinguishing_string1
 *  page2_name: distinguishing_string2
 *  ...
 * actions:
 *  - action
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class UnstructuredYamlParser extends AbstractYamlParser implements IParser{
	private static class Pair {
		public final String name;
		public final String value;
		public Pair(String name, String value) {
			this.name = name;
			this.value = value;
		}
	}
	protected Map<String, Object> stringMap(Object mapObject, String context) {
		return (Map<String,Object>) typeCheck(context, mapObject, Map.class);
	}
	
	protected List<Object> objectList(Object listObject, String context) {
		return (List<Object>) typeCheck(context, listObject, List.class);
	}
	
	private Pair fetchPair(Map<String,Object> pairMap, String context) {
		if(pairMap.size() != 1) {
			Log.err("One and only one value pair is needed for "+context);
		}
		Iterator<String> iter = pairMap.keySet().iterator();
		String key = iter.next();
		String value = typeCheck("password", pairMap.get(key), String.class);
		return new Pair(key, value);
	}
	
	private <T> T typeCheck(String objectName, Object objectValue, Class<T> klass) {
		if(objectValue == null) {
			return null;
		}
		if(!klass.isAssignableFrom(objectValue.getClass())) {
			Log.err(objectName + " should be of type "+klass.getSimpleName());
			Log.err("Instead it's of the type "+ objectValue.getClass().getName() + " with the string "+objectValue);			
			System.exit(0);
		}
		return klass.cast(objectValue);
	}
	
	private <T> Map<String,T> fetchAllOfType(Map<String,Object> map, Class<T> klass) {
		Map<String,T> cMap = new HashMap<String,T>();
		for(String key : map.keySet()) {
			Object value = map.get(key);
			if(klass.isAssignableFrom(value.getClass())) {
				cMap.put(key, klass.cast(value));
			}
		}
		return cMap;
	}
	
	private <T> T strongFetch(Map<String, Object> map, String context, String key, Class<T> klass) {
		Object fetchedObject = map.remove(key);
		if(fetchedObject == null) {
			Err.handleNotFoundField(context, key);
		}
		return typeCheck(key, fetchedObject, klass);
	}
	
	private <T> T weakFetch(Map<String, Object> map, String context, String key, Class<T> klass) {
		Object fetchedObject = map.remove(key);
		return typeCheck(key, fetchedObject, klass);
	}
	
	
	private Map<String, Object> strongMapFetch(Map<String, Object> fieldMap, String context, String field) {
		return strongFetch(fieldMap, context, field, Map.class);
	}
	
//	private Map<String, Object> weakMapFetch(Map<String, Object> fieldMap, String context, String field) {
//		return weakFetch(fieldMap, context, field, Map.class);
//	}
	
//	private List<Object> strongListFetch(Map<String, Object> map, String context, String field) {
//		return strongFetch(map, context, field, List.class);
//	}
	
	private String strongStringFetch(Map<String, Object> map, String context, String field) {
		return strongFetch(map, context, field, String.class);
	}
	
	public String weakStringFetch(Map<String, Object> map, String context, String field) {
		return weakFetch(map, context, field, String.class);
	}
	
	protected Map<String, Object> readHttpConfig(String path) throws Exception {
		Yaml yaml = new Yaml();
	    String webDoc = getFileString(path);
		Map<String, Object> webMap = typeCheck("web configuration", yaml.load(webDoc), Map.class) ;
	    return webMap;
	}
	
	public List<HTTPKeyValuePair> loadPairs(Map<String,Object> paramMap) {
		List<HTTPKeyValuePair> pairs = new ArrayList<HTTPKeyValuePair>();
		Map<String, String> map = fetchAllOfType(paramMap, String.class);
		for(String paramName : map.keySet()) {
			String value = map.get(paramName);
			HTTPKeyValuePair pair = new HTTPKeyValuePair(paramName, value);
			pairs.add(pair);
		}
		return pairs;
	}
	
	public List<Parameter> loadParams(Map<String,Object> paramMap) {
		List<Parameter> params = new ArrayList<Parameter>();
		Map<String, List> map = fetchAllOfType(paramMap, List.class);
		int paramIndex = 0;
		for(String paramName : map.keySet()) {
			Parameter param = new Parameter(paramName, paramIndex);
			paramIndex ++;
			List<Object> paramValues = map.get(paramName);
			List<String> paramStrings = new ArrayList<String>();
			for(Object value : paramValues) {
				String paramValue = value.toString();
				paramStrings.add(paramValue);
			}
			param.setDefaultValues(paramStrings);
		}
		return params;
	}
	
	private HTTPAction getNewAction(String name, String method, HTTPAction.Action actType,
			List<HTTPKeyValuePair> pairs, List<Parameter> params) {
		//no url defined <-
		HTTPAction action = newAction(name, params, method, actType, null);
		action.setKeyValuePairs(pairs);
		return action;
	}
	
	protected HTTPAction loadAction(Map<String, Object> actionMap) {
		String name = strongStringFetch(actionMap, "action attribute", "action");
		String method = weakStringFetch(actionMap, "method attribute", "method");
		actionMap.remove("action");
		actionMap.remove("method");
		List<HTTPKeyValuePair> pairs = loadPairs(actionMap); 
		List<Parameter> params = loadParams(actionMap);
		HTTPAction action = getNewAction(name, method, HTTPAction.Action.ACTION, pairs, params);		
		return action;
	}
	
	public HTTPAction loadSpecial(Map<String,Object> webMap, HTTPAction.Action actType) {
		HTTPAction action = null;
		String method = weakStringFetch(webMap, "method attribute", "method");
		List<Parameter> params = loadParams(webMap);
		if(params.isEmpty() == false) {
			Log.err(actType.name() +" actions can not be parameterized, so the parameters are ignored");
			params.clear();
		}
		List<HTTPKeyValuePair> pairs = loadPairs(webMap);
		action = getNewAction(actType.name(), method, actType, pairs, params);
		return action;
	}


	protected HTTPAction loadInitAction(Map<String,Object> webMap) {
		HTTPAction initAction = loadSpecial(webMap, HTTPAction.Action.INIT);
		return initAction;
	}
	
	protected HTTPAction loadResetAction(Map<String,Object> webMap) {
		HTTPAction resetAction = loadSpecial(webMap, HTTPAction.Action.RESET);
		return resetAction;
	}
	
	protected List<HTTPPage> loadPages(Map<String, Object> webMap) {
		Map<String,Object> pagesMap = strongMapFetch(webMap, "yaml file", "pages");
		List<HTTPPage> pages = new ArrayList<HTTPPage>();
		for(String pageName : pagesMap.keySet()) {
			Object pageContent = pagesMap.get(pageName);
			String contentString = typeCheck("parameter value", pageContent, String.class);
			HTTPPage page = new HTTPPage(pageName, contentString);
			pages.add(page);
		}
		return pages;
	}
	
	protected HTTPAuthentication loadAuthentication(Map<String, Object> authMap) {
		HTTPAuthentication auth = null;
		Pair pair = fetchPair(authMap, "authentication");
		auth = new HTTPAuthentication(pair.name, pair.value);
		return auth;
	}

	public static void main(String args[]) throws Exception {
		UnstructuredYamlParser parser = new UnstructuredYamlParser();
		Object object = parser.loadHttpConfig(ILoc.EXAMPLES_FOLDER+"/yamlconfig/unstructured.yaml");
		System.out.println(object.toString());
	}

}
