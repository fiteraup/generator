package parsers.web;

import exceptions.Err;
import generator.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kwalify.Util;
import options.Options;
import parsers.IParser;
import types.Interface;
import types.Parameter;
import types.http.HTTPAction;
import types.http.HTTPAction.Action;
import types.http.HTTPAction.Method;
import types.http.HTTPAuthentication;
import types.http.HTTPContentQuery;
import types.http.HTTPInterface;
import types.http.HTTPKeyQueryPair;
import types.http.HTTPPage;

@SuppressWarnings({ "unchecked" })
public abstract class AbstractYamlParser  implements IParser{
	protected int index = 0;
	protected String webUrl = null;
	protected abstract Map<String, Object> readHttpConfig(String path) throws Exception;
	
	protected Map<String, Object> stringMap(Object mapObject, String context) {
		return (Map<String,Object>) mapObject;
	}
	
	protected List<Object> objectList(Object listObject, String context) {
		return (List<Object>) listObject;
	}
	
	protected abstract List<HTTPPage> loadPages(Map<String, Object> webMap);
	
	protected abstract HTTPAction loadResetAction(Map<String, Object> actionMap);
	
	protected List<HTTPAction> loadResetActions(Map<String, Object> webMap) {
		return loadActions(webMap, ITag.RESET, new ResetCallback());
	}
	
	protected abstract HTTPAction loadInitAction(Map<String, Object> actionMap);

	protected List<HTTPAction> loadInitActions(Map<String, Object> webMap) {
		return loadActions(webMap, ITag.INIT, new InitCallback());
	}
	
	protected abstract HTTPAction loadAction(Map<String, Object> actionMap); 
	
	protected List<HTTPAction> loadActions(Map<String, Object> webMap) {
		return loadActions(webMap, ITag.ACTIONS, new ActionCallback());
	}
	
	protected List<HTTPAction> loadActions(Map<String, Object> webMap, String tag, Callback<HTTPAction> callback) {
		List<HTTPAction> actions = loadOfType(webMap, tag, callback);
		if(actions.isEmpty()) {
			Log.warn("No actions have been defined in cattegory "+tag);
		}
		return actions;
	}
	
	protected Map<String,HTTPKeyQueryPair> queryMap = new HashMap<String, HTTPKeyQueryPair>();
	
	private void fillMap(List<HTTPKeyQueryPair> queries) {
		for(HTTPKeyQueryPair query : queries) {
			if(queryMap.containsKey(query.getParameterName())) {
				Err.handleDuplicateParameter(query.getParameterName());
			} else {
				queryMap.put(query.getParameterName(), query);
			}
		}
	}
	
	
	protected List<HTTPKeyQueryPair> loadQueries(Map<String, Object> webMap) {
		List<HTTPKeyQueryPair> queries = loadOfType(webMap, ITag.NQPAIRS, new QueryCallback());
		return queries;
	}
	
	protected HTTPKeyQueryPair loadNameQuery(Map<String, Object> pairMap) {
		if(pairMap != null) {
			String name = (String) pairMap.get("name");
			Map<String, Object> queryObject = (Map<String, Object>) pairMap.get(ITag.QUERY);
			HTTPContentQuery query = loadContentQuerty(queryObject);
			HTTPKeyQueryPair pair = new HTTPKeyQueryPair(name, query);
			return pair;
		}
		return null;
	}
	
	private HTTPContentQuery loadContentQuerty(Map<String, Object> queryObject) {
		String paramName = (String) queryObject.get("param");
		Boolean updateable = (Boolean) queryObject.get("updateable");
		updateable = (updateable == null)? false : updateable;
		List<Object> hintObjects = (List<Object>) queryObject.get("hints");
		List<String> hints = toStringList(hintObjects);
		HTTPContentQuery query = new HTTPContentQuery(paramName, hints, updateable);
		return query;
	}
	
	protected <T> List<T> loadOfType(Map<String, Object> map, String tag, Callback<T> callback) {
		List<T> listOfTypeT = new ArrayList<T>();
		List<Object> tObjects = objectList( map.get(tag), tag);
		//DEBUG: System.err.println("tag "+tag+"  "+map);
		if(tObjects != null) {
			index = 0;
			for(Object tObject : tObjects) {
				Map<String,Object> tMap = stringMap( tObject, tag);
				T tConcrete = callback.parseMap(tMap);
				listOfTypeT.add(tConcrete);
				index ++;
			}
		}
		return listOfTypeT;
	}
	
	protected List<String> toStringList(List<Object> values) {
		List<String> strings = new ArrayList<String>();
		if(values != null)
			for(Object value : values) {
				strings.add(value.toString());
			}
		return strings;
	}
	
	protected Interface loadHttpConfig(String interfacePath) throws Exception {
		Object configuration = readHttpConfig(interfacePath);
		Map<String, Object> webMap = stringMap(configuration, "web configuration");
		List<HTTPKeyQueryPair> queries = loadQueries(webMap);
		fillMap(queries); 
		List<HTTPAction> actions = loadActions(webMap);
	    List<HTTPAction> initActions = loadInitActions(webMap);
	    List<HTTPAction> resetActions = loadResetActions(webMap);
	    List<HTTPPage> pages = loadPages(webMap);
	    String name = loadInterfaceName(webMap);
	    HTTPAuthentication auth = loadMainAuthentication(webMap);
	    HTTPInterface httpIntrf = new HTTPInterface(name,actions,pages);
	    httpIntrf.setInitActions(initActions);
	    httpIntrf.setResetActions(resetActions);
	    httpIntrf.setAuth(auth);
	    return httpIntrf;
	}
	
	private HTTPAuthentication loadMainAuthentication(Map<String, Object> webMap) {
		HTTPAuthentication auth = null;
		if(webMap.containsKey(ITag.AUTH)) {
			Map<String, Object> authMap = stringMap(webMap.get(ITag.AUTH), "loading auth map");
			auth = loadAuthentication(authMap);
		}
		return auth;
	}

	protected abstract HTTPAuthentication loadAuthentication(Map<String, Object> authMap);

	private String loadInterfaceName(Map<String, Object> webMap) {
		return webMap.containsKey(ITag.INTFNAME) ? (String)webMap.get(ITag.INTFNAME) : "web" ;
	}

	protected String getFileString(String filePath){
		
		String doc = "";
		File fileAtPath = new File(filePath);
		if(fileAtPath.exists() == false) {
			Err.handleNoFileFound(filePath);
		}
		try {
			doc = Util.readFile(filePath);
		} catch (Exception e) {
			Err.handleInternal(e);
			
		}
		return doc;
	}

	public final Interface parse(Options options) throws Exception {
		String interfacePath = options.httpOptions.descriptionFile;
		this.webUrl = options.httpOptions.getLink();
		Interface intrf = loadHttpConfig(interfacePath);
		return intrf;
	}
	
	protected interface Callback<T> {
		T parseMap(Map<String,Object> map);
	}
	
	private class ActionCallback implements Callback<HTTPAction> {
		public HTTPAction parseMap(Map<String,Object> map) {
			return AbstractYamlParser.this.loadAction(map);
		}
	}
	
	private class ResetCallback implements Callback<HTTPAction> {
		public HTTPAction parseMap(Map<String,Object> map) {
			return AbstractYamlParser.this.loadResetAction(map);
		}
	}
	
	private class InitCallback implements Callback<HTTPAction> {
		public HTTPAction parseMap(Map<String,Object> map) {
			return AbstractYamlParser.this.loadInitAction(map);
		}
	}
	
	protected class QueryCallback implements Callback<HTTPKeyQueryPair> {
		public HTTPKeyQueryPair parseMap(Map<String, Object> map) {
			return loadNameQuery(map);
		}
	} 
	
	protected HTTPAction newAction(String actionName, List<Parameter> parameters, String method, Action action, String url) {
		Method reqMethod = (method == null) ? Method.GET : Method.valueOf(method);
		Action reqType = (action == null) ? Action.ACTION : action;
		String reqUrl = (url == null) ? webUrl : url;
		List<Parameter> reqParam = (parameters == null) ? new ArrayList<Parameter>() : new ArrayList<Parameter>(parameters);
		return new HTTPAction(actionName,
				reqParam,
				reqMethod, 
				reqType,
				reqUrl);
	}
	
	private List<HTTPKeyQueryPair> fetchQueries(List<String> queryStrings) {
		List<HTTPKeyQueryPair> queries = new ArrayList<HTTPKeyQueryPair>();
		for(String queryString : queryStrings) {
			if(queryMap.containsKey(queryString) == false) {
				Err.handleQueryNotDefined(queryString);
			}
			HTTPKeyQueryPair query = queryMap.get(queryString);
			queries.add(query);
		}
		return queries;
	}
	
	protected List<HTTPKeyQueryPair> loadQueryUpdates(Map<String, Object> actionMap) {
		List<Object> queryObjects = (List<Object>) actionMap.get(ITag.UPQUERY);
		List<String> queryStrings = toStringList(queryObjects);
		List<HTTPKeyQueryPair> queries = fetchQueries(queryStrings);
		return queries;
	}
	
	protected List<HTTPKeyQueryPair> loadQueryReferrences(Map<String, Object> actionMap) {
		List<Object> queryObjects = (List<Object>) actionMap.get(ITag.REFQUERY);
		List<String> queryStrings = toStringList(queryObjects);
		List<HTTPKeyQueryPair> queries = fetchQueries(queryStrings);
		return queries;
	}
}
