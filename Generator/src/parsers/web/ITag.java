package parsers.web;

public interface ITag {
	public static final String NQPAIRS = "nqpairs";
	public static final String REFQUERY = "refqueries";
	public static final String UPQUERY = "upqueries";
	public static final String NVPAIRS = "nvpairs";
	public static final String ACTIONS = "actions";
	public static final String PARAMS = "parameters";
	public static final String PAGES = "pages";
	public static final String RESET = "reset";
	public static final String INIT = "init";
	public static final String QUERY = "query";
	public static final String INTFNAME = "name";
	public static final String AUTH = "auth";
	public static final String AUTHUSER = "user";
	public static final String AUTHPWD = "password";
}
