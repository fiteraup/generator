package parsers.web;

import generator.ILoc;
import generator.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import parsers.IParser;

import kwalify.ValidationException;
import kwalify.Validator;
import kwalify.YamlParser;
import types.IMessage;
import types.Parameter;
import types.http.HTTPAction;
import types.http.HTTPAction.Action;
import types.http.HTTPAuthentication;
import types.http.HTTPInterface;
import types.http.HTTPKeyQueryPair;
import types.http.HTTPKeyValuePair;
import types.http.HTTPPage;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class StructuredYamlParser extends AbstractYamlParser  implements IParser{


	private Object getSchema() throws Exception {
		String schema_str = getFileString(ILoc.FILES_FOLDER
				+ "/schemas/web.schema.yaml");
		Object schema = new YamlParser(schema_str).parse();
		return schema;
	}

	protected Map<String,Object> readHttpConfig(String path) throws Exception {
		Object schema = getSchema();
		String document_str = getFileString(path);
		YamlParser parser = new YamlParser(document_str);
		Object document = parser.parse();
		validateDocument(schema, document, parser);
		return stringMap(document, "web configuration");
	}

	private void validateDocument(Object schema, Object document,
			YamlParser parser) {
		Validator validator = new Validator(schema);
		List errors = validator.validate(document);
		if (errors != null && errors.size() > 0) {
			parser.setErrorsLineNumber(errors);
			Collections.sort(errors);
			for (Iterator it = errors.iterator(); it.hasNext();) {
				ValidationException error = (ValidationException) it.next();
				int linenum = error.getLineNumber();
				String path = error.getPath();
				String mesg = error.getMessage();
				Log.err("- " + linenum + ": [" + path + "] " + mesg);
			}
			System.exit(0);
		}
	}
	
	@Override
	protected List<HTTPPage> loadPages(Map<String, Object> webMap) {
		List<HTTPPage> pages = loadOfType(webMap, ITag.PAGES, new PageCallback());
		return pages;
	}
	
	protected HTTPPage loadPage(Map<String, Object> pageMap) {
		String name = (String) pageMap.get("name");
		String content = pageMap.get("content").toString();
		HTTPPage page = new HTTPPage(name, content);
		return page;
	}

	@Override
	protected HTTPAction loadResetAction(Map<String, Object> actionMap) {
		HTTPAction resetAction = loadSpecialAction(actionMap, "reset"+index, Action.RESET);
		List<HTTPKeyQueryPair> referredQueries = loadQueryReferrences(actionMap);
		resetAction.setReferredQueries(referredQueries);
		return resetAction;
	}

	@Override
	protected HTTPAction loadInitAction(Map<String, Object> actionMap) {
		HTTPAction initAction = loadSpecialAction(actionMap, "init"+index, Action.INIT);
		List<HTTPKeyQueryPair> updateQueries = loadQueryUpdates(actionMap);
		initAction.setUpdatedQueries(updateQueries);
		return initAction;
	}
	
	protected HTTPAction loadSpecialAction(Map<String, Object> actionMap, String actionName, Action actionType) {
		String method = (String) actionMap.get("method");
		String url = (String) actionMap.get("url");
		List<HTTPKeyValuePair> pairs = loadNameValuePairs(actionMap);
		HTTPAction action = newAction(actionName, new ArrayList<Parameter>(), method, actionType, url);
		action.setKeyValuePairs(pairs);
		return action;
	}
	
	private List<Parameter> loadParameters(Map<String,Object> actionMap) {
		List<Parameter> params = new ArrayList<Parameter>();
		List<Object> parametersObject = (List<Object>) actionMap.get("parameters");
		int index = 0;
		for(Object parameterObject : parametersObject) {
			Map<String,Object> paramMap = (Map<String, Object>) parameterObject;
			Parameter param = loadParameter(paramMap, index);
			params.add(param);
			index ++;
		}
		return params;
	}
	
	private Parameter loadParameter(Map<String, Object> paramMap, int index) {
		String name = (String) paramMap.get("name");
		List<String> values = new ArrayList<String>();
		List<Object> valuesObject = (List<Object>) paramMap.get("values");
		for(Object valueObject : valuesObject) {
			String value = valueObject.toString();
			values.add(value);
		}
		Parameter param = new Parameter(name,index);
		param.setDefaultValues(values);
		return param;
	}
	
	private List<HTTPKeyValuePair> loadNameValuePairs(Map<String,Object> actionMap) {
		List<HTTPKeyValuePair> pairs = new ArrayList<HTTPKeyValuePair>(); 
		List<Object> pairObjects = (List<Object>) actionMap.get("nvpairs");
		if(pairObjects != null) {
			for(Object pairObject : pairObjects) {
				Map<String, Object> pairMap = (Map<String, Object>) pairObject;
				HTTPKeyValuePair pair = loadPair(pairMap);
				pairs.add(pair);
			} 
		}
		return pairs;
	}
	
	private HTTPKeyValuePair loadPair(Map<String,Object> pairMap) {
		String name = (String) pairMap.get("name");
		Object objectValue = pairMap.get("value");
		String value = objectValue != null? objectValue.toString() : null;
		HTTPKeyValuePair pair = new HTTPKeyValuePair(name, value);
		return pair;
	}

	protected HTTPAction loadAction(Map<String,Object> actionMap) {
		String name = (String) actionMap.get("name");
		String method = (String) actionMap.get("method");
		String url = (String) actionMap.get("url");
		List<Parameter> params = loadParameters(actionMap);
		List<HTTPKeyValuePair> pairs = loadNameValuePairs(actionMap);
		List<HTTPKeyQueryPair> queryReferrences= loadQueryReferrences(actionMap);
		HTTPAction action = newAction(name, params, method, Action.ACTION, url); 
		action.setKeyValuePairs(pairs);
		action.setReferredQueries(queryReferrences);
		return action;
	}
	
	protected class PageCallback implements Callback<HTTPPage> {
		public HTTPPage parseMap(Map<String, Object> map) {
			return loadPage(map);
		}
	}
	
	protected HTTPAuthentication loadAuthentication(Map<String, Object> authMap) {
		HTTPAuthentication auth = null;
		auth = new HTTPAuthentication((String) authMap.get(ITag.AUTHUSER), (String) authMap.get(ITag.AUTHPWD));
		return auth;
	}
	
	public static void main(String args[]) throws Exception {
		StructuredYamlParser parser = new StructuredYamlParser();
		//Object object = parser.loadHttpConfig(ILoc.EXAMPLES_FOLDER+"/yamlconfig/structured.yaml");
		HTTPInterface httpInterface = (HTTPInterface) parser.loadHttpConfig("structweb.yaml");
		List<Parameter> pList = new ArrayList<Parameter>();
		for(IMessage message : httpInterface.getAllActions()) {
			for(Parameter param : message.getParameters()) {
				if(pList.contains(param) == false) {
					pList.add(param);
				}
			}
		}
		System.out.println(httpInterface);
		System.out.println(pList);
	}
}
