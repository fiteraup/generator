package types.intrf;

import java.util.List;

import exceptions.Err;

import types.Connection;
import types.IOutput;
import types.Interface;
import types.Message;
import types.Parameter;

public class PLAINInterface extends Interface{

	public PLAINInterface(List<Connection> connections) {
		super(connections);
	}
	public PLAINInterface(String name, List<Connection> connections) {
		super(name, connections);
	}
	
	public List<Parameter> getParameters(Message message) {
		List<Parameter> parameters = null;
		if(message instanceof PLAINMessage) {
			parameters = ((PLAINMessage) message).getPrimitiveParameters();
		} else {
			Err.invalidMessageParsed(message, "plain interface");
		}
		
		return parameters;
	}
	
	public List<PLAINInput> getPLAINInputs() {
		List<PLAINInput> inputs = this.castToList(this.getInputMessages(), PLAINInput.class);
		return inputs;
	}
	
	public List<PLAINOutput> getPLAINOutputs() {
		List<PLAINOutput> inputs = this.castToList(this.getOutputMessages(), PLAINOutput.class);
		return inputs;
	}
	
	public PLAINOutput getPLAINOutput(PLAINInput input) {
		List<IOutput> outputMessage = this.getOutputMessages(input);
		return (PLAINOutput) outputMessage.get(0);
	}
}
