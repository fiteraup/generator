package types.intrf;

import java.util.ArrayList;
import java.util.List;

import types.IInput;
import types.Parameter;

public class PLAINInput extends PLAINMessage implements IInput{

	public PLAINInput(String name, List<Parameter> parameters) {
		super(name, parameters);
	}
	
	public PLAINInput(String name, Parameter parameter) {
		super(name, parameter);
	}
	
	public PLAINInput(String name) {
		super(name, new ArrayList<Parameter>());
	}
	
	public List<Parameter> getInputParameters() {
		return parameters;
	}

}
