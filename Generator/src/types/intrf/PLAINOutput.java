package types.intrf;

import java.util.ArrayList;

import types.IOutput;
import types.Parameter;

public class PLAINOutput extends PLAINMessage implements IOutput{
	public PLAINOutput(String name) {
		super(name, new ArrayList<Parameter>());
		this.setType(Type.OUTPUT);
	}
	
	public PLAINOutput(String name, Parameter parameter) {
		super(name,parameter);
	}
	
	public Parameter getOutputParameter() {
		if(parameters.isEmpty() == false)
			return parameters.get(0);
		return null;
	}
}
