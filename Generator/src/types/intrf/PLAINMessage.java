package types.intrf;

import java.util.ArrayList;
import java.util.List;

import types.Message;
import types.Parameter;

public class PLAINMessage extends Message {
	
	public PLAINMessage(String name, Parameter parameter) {
		super(name,parameter);
	}

	public PLAINMessage(String name, List<Parameter> parameters) {
		super(name, parameters);
	}
	
	public List<Parameter> getParameters() {
		return getPrimitiveParameters();
	}
	
	public List<Parameter> getAllParameters() {
		return super.parameters;
	}
	
	public List<Parameter> getPrimitiveParameters() {
		List<Parameter> params = new ArrayList<Parameter>();
		for(Parameter param : this.parameters) {
			updateParamListWithParam(param, params);
		}
		return params;
	}
	
	private List<Parameter> getParameters(CompositeParameter parameter) {
		List<Parameter> params = new ArrayList<Parameter>();
		for(Parameter param : parameter.parameters) {
			updateParamListWithParam(param, params);
		}
		return params;
	}
	
	private void updateParamListWithParam(Parameter newParameter, List<Parameter> paramList) {
		if(newParameter.type == types.Type.COMPOSITE) {
			paramList.addAll(getParameters((CompositeParameter) newParameter));
		} else {
			paramList.add(newParameter);
		}
	}
}
