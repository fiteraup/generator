package types.intrf;

import java.util.Collection;

import types.Parameter;
import types.Type;

public class CompositeParameter extends Parameter {
	public final Parameter [] parameters;
	public final Class<?> compositeType;
	public CompositeParameter(String name, Class<?> type, int index, Collection<Parameter> parameters) {
		super(name, Type.COMPOSITE, index);
		this.parameters = parameters.toArray(new Parameter[parameters.size()]);
		this.compositeType = type;
	}
	
	public String toString() {
		String paramString = "";
		for(Parameter parameter : parameters) {
			paramString = paramString + ";" + parameter.toString();
		}
		return "PARAMETER[name="+name+";type="+type+";\nparameters="+paramString+"]";
	}
}
