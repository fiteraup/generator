package types.http;


public class HTTPKeyValuePair{

	private final String key;
	private final String value;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HTTPKeyValuePair other = (HTTPKeyValuePair) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public HTTPKeyValuePair(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getKey() {
		return key;
	}
	
	public boolean isAssigned() {
		return value != null;
	}
	
}
