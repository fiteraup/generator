package types.http;

import java.util.ArrayList;
import java.util.List;

import types.Connection;
import types.IMessage;
import types.IOutput;
import types.Interface;

public class HTTPInterface extends Interface{
	private HTTPAuthentication auth = null; 
	private List<HTTPAction> initActions = new ArrayList<HTTPAction>();
	private List<HTTPAction> resetActions = new ArrayList<HTTPAction>();
	
	public List<HTTPAction> getInitActions() {
		return new ArrayList<HTTPAction>(initActions);
	}

	public void setInitActions(List<HTTPAction> initActions) {
		clearOwner(new ArrayList<IMessage>(this.initActions));
		this.initActions.clear();
		this.initActions.addAll(initActions);
		setAsOwner(new ArrayList<IMessage>(this.initActions));
	}

	public List<HTTPAction> getResetActions() {
		return new ArrayList<HTTPAction>(resetActions);
	}

	public void setResetActions(List<HTTPAction> resetActions) {
		clearOwner(new ArrayList<IMessage>(this.resetActions));
		this.resetActions.clear();
		this.resetActions.addAll(resetActions);
		setAsOwner(new ArrayList<IMessage>(this.resetActions));
	}

	public HTTPInterface(String name, List<Connection> connections) {
		super(name, connections);
	}
	
	public HTTPInterface(String name, List<HTTPAction> actions, List<HTTPPage> pages) {
		super(name, new ArrayList<Connection>());
		List<Connection> httpConnections = buildConnections(actions, pages);
		addConnections(httpConnections);
	}
	
	private List<Connection> buildConnections(List<HTTPAction> actions, List<HTTPPage> pages) {
		List<Connection> connections = new ArrayList<Connection>();
		List<IOutput> abstractedPages = new ArrayList<IOutput>();
		abstractedPages.addAll(pages);
		for(HTTPAction action : actions) {
			Connection connection = new Connection(action, abstractedPages);
			connections.add(connection);
		}
		return connections;
	}
	
	public List<HTTPAction> getAllActions() {
		List<HTTPAction> actions = castToList(this.getInputMessages(), HTTPAction.class);
		actions.addAll(getInitActions());
		actions.addAll(getResetActions());
		return actions;
	}
	
	public List<HTTPAction> getActions() {
		List<HTTPAction> actions = castToList(this.getInputMessages(), HTTPAction.class);
		return actions;
	}
		
	public List<HTTPPage> getPages() {
		List<HTTPPage> pages = castToList(this.getOutputMessages(), HTTPPage.class);
		return pages;
	}
	
	public String toString(List<HTTPAction> actions) {
		String ret = "";
		for(HTTPAction action : actions) {
			ret += "	" + action.toString() + "\n";
		}
		return ret;
	}
	
	public String toString() {
		String ret = "ACTIONS\n" + toString(getActions());
		ret += "\nRESET ACTIONS\n" + toString(resetActions);
		ret += "\nINIT ACTIONS\n" + toString(initActions);
		return ret;
	}
	
	public static void main(String args [] ) throws Exception{
	}

	public HTTPAuthentication getAuth() {
		return auth;
	}

	public void setAuth(HTTPAuthentication auth) {
		this.auth = auth;
	}
}
