package types.http;

public class HTTPKeyQueryPair {
	private String parameterName;
	private HTTPContentQuery query;
	public HTTPKeyQueryPair(String parameterName, HTTPContentQuery query) {
		this.parameterName = parameterName;
		this.query = query;
	}
	
	public String getParameterName() {
		return parameterName;
	}

	public HTTPContentQuery getQuery() {
		return query;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((parameterName == null) ? 0 : parameterName.hashCode());
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HTTPKeyQueryPair other = (HTTPKeyQueryPair) obj;
		if (parameterName == null) {
			if (other.parameterName != null)
				return false;
		} else if (!parameterName.equals(other.parameterName))
			return false;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		return true;
	}

	public String toString() {
		return parameterName;
	}
}
