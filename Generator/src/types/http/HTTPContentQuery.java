package types.http;

import java.util.ArrayList;
import java.util.List;

public class HTTPContentQuery {
	private String name;
	private List<String> hints;
	private boolean isUpdateable;
	
	public HTTPContentQuery(String name, List<String> hints, boolean isUpdateable) {
		this.name = name;
		this.hints = new ArrayList<String>(hints);
		this.isUpdateable = isUpdateable;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isUpdateable() {
		return isUpdateable;
	}
	
	public List<String> getHints() {
		return new ArrayList<String>(hints);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hints == null) ? 0 : hints.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HTTPContentQuery other = (HTTPContentQuery) obj;
		if (hints == null) {
			if (other.hints != null)
				return false;
		} else if (!hints.equals(other.hints))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
