package types.http;

import java.util.Arrays;

import types.IOutput;
import types.Message;
import types.Parameter;

public class HTTPPage extends Message implements IOutput{
	public final String discerningString;
	public HTTPPage(String name, String discerningString) {
		super(name, Arrays.asList(new Parameter("code",types.Type.STRING,0)));
		this.discerningString = discerningString;
	}
}
