package types.http;

import java.util.ArrayList;
import java.util.List;

import types.IInput;
import types.Message;
import types.Parameter;

public class HTTPAction extends Message implements IInput{
	public static enum Method {
		GET,
		POST
	}
	public static enum Action {
		ACTION,
		RESET,
		INIT
	}
	
	public final Method methodType;
	public final Action actionType;
	public final String url;
	
	protected final List<HTTPKeyValuePair> pairs = new ArrayList<HTTPKeyValuePair>();
	protected final List<HTTPKeyQueryPair> updatedQueries = new ArrayList<HTTPKeyQueryPair>();
	protected final List<HTTPKeyQueryPair> referredQueries = new ArrayList<HTTPKeyQueryPair>(); 
	
	public HTTPAction(String actionName, List<Parameter> parameters, Method type, String url) {
		super(actionName, parameters);
		this.methodType = type;
		this.actionType = Action.ACTION;
		this.url= url;
	}
	
	public HTTPAction(String actionName, List<Parameter> parameters, Method methType, Action actType, String url) {
		super(actionName, parameters);
		this.methodType = methType;
		this.actionType = actType;
		this.url = url;
	}
	
	public List<HTTPKeyValuePair> getKeyValuePairs() {
		return new ArrayList<HTTPKeyValuePair>(pairs);
	}
	
	public void setKeyValuePairs(List<HTTPKeyValuePair> pairs) {
		this.pairs.clear();
		this.pairs.addAll(pairs);
	}
	
	public List<HTTPKeyQueryPair> getUpdatedQueries() {
		return new ArrayList<HTTPKeyQueryPair>(updatedQueries);
	}
	
	public void setUpdatedQueries(List<HTTPKeyQueryPair> infValues) {
		this.updatedQueries.clear();
		this.updatedQueries.addAll(infValues);
	}
	
	public List<HTTPKeyQueryPair> getReferredQueries() {
		return new ArrayList<HTTPKeyQueryPair>(referredQueries);
	}
	
	public void setReferredQueries(List<HTTPKeyQueryPair> infValues) {
		this.referredQueries.clear();
		this.referredQueries.addAll(infValues);
	}
	
	public boolean isReset() {
		return actionType == Action.RESET;
	}
	
	public boolean isInit() {
		return actionType == Action.INIT;
	}
	
	public String toString() {
		return "ACTION[name="+name+";type="+methodType+";params="+ parameters.toString()+";" +
				"updatedqueries="+updatedQueries.toString()+";refqueries="+referredQueries.toString()+"]";
	}
}
