package types.http;

import sun.misc.BASE64Encoder;

public class HTTPAuthentication {
	private String username;
	private String password;
	private Type authType;
	public static enum Type {
		BASIC_AUTH
	}
	public HTTPAuthentication(String username, String password) {
		this.username = username;
		this.password = password;
		this.authType = Type.BASIC_AUTH;
	}
	
	public String getAuthString() {
		BASE64Encoder encoder = new BASE64Encoder(); 
		String authString = "Basic "+encoder.encode((username+":"+password).getBytes());
		return authString;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	public Type getAuthType() {
		return authType;
	}
}
