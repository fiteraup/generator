package types;

import java.util.List;

public interface InterfaceProvider {
	List<IInput> getInputMessages();
	List<IOutput> getOutputMessages();
	List<Parameter> getParameters(IMessage message);
	String getName();
}
