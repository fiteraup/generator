package types;

import java.util.ArrayList;
import java.util.List;

public class Parameter {
	public final Type type;
	public final String name;
	public final int index;
	protected final List<String> defaultValues = new ArrayList<String>();
	protected IMessage ownerMessage;
	
	public Parameter(String name, int index) {
		this(name, Type.STRING,index);
	}
	
	public Parameter(String name,Type type, int index) {
		this.name = name;
		this.type = type;
		this.index = index;
	}
	
	public void setDefaultValues(List<String> defaultValues) {
		this.defaultValues.clear();
		this.defaultValues.addAll(defaultValues);
	}
	
	public List<String> getDefaultValues() {
		return new ArrayList<String>(defaultValues);
	}
	
	void setOwner(IMessage message) {
		this.ownerMessage = message;
	}
	
	public IMessage getOwner() {
		return ownerMessage;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + index;
		result = prime * result + ((ownerMessage == null) ? 0 : ownerMessage.getName().hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parameter other = (Parameter) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return index == other.index && ownerMessage.getName().equals(other.getOwner().getName());
	}
	
	public String toString() {
		return "PARAMETER[name="+name+";type="+type+";owner="+ownerMessage.getName()+";defVal="+defaultValues+"]";
	}
}
