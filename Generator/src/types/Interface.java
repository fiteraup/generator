package types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Interface class is an abstraction used to universally describe the interface of the service. This abstraction
 * is inferred through parsing by parsers specific to the various technologies used. (such as RMI, WSDL...)
 * Key components are: the name of the interface, the messages sent or received and a map of parsed attributes,
 * that will be completed by the parsers. This map will subsequently be used in the generation process.
 * @author Paul
 */

public class Interface implements InterfaceProvider{
	public final String name;
	private final List<Connection> connections;
	public final Map<Enum<?>,Object> parsedAttributes = new HashMap<Enum<?>,Object>();
	
	public Interface(String name, List<Connection> connections) {
		this.name = name;
		this.connections = connections;
		setAsOwner();
	}
	public Interface(List<Connection> connections)
	{
		this("Default", connections);
	}
	
	protected void setAsOwner() {
		List<IMessage> messages = getAllMessages();
		setAsOwner(messages);
	}
	
	protected void clearOwner(List<IMessage> messages) {
		for(IMessage message : messages) {
			message.setOwner(null);
		}
	}
	
	protected void setAsOwner(List<IMessage> messages) {
		for(IMessage message : messages) {
			message.setOwner(this);
		}
	}
	
	public List<IMessage> getAllMessages() {
		List<IMessage> allMessages = new ArrayList<IMessage>();
		allMessages.addAll(getInputMessages());
		allMessages.addAll(getOutputMessages());
		return allMessages;
	}
	
	public List<IInput> getInputMessages() {
		List<IInput> inputMessages = new ArrayList<IInput>();
		for(Connection connection : connections) {
			if(inputMessages.contains(connection.inputMessage) == false)
				inputMessages.add(connection.inputMessage);
		}
		return inputMessages;
	}
	
	public List<IOutput> getOutputMessages() {
		List<IOutput> outputMessages = new ArrayList<IOutput>();
		for(Connection connection : connections) {
			for(IOutput outputMessage : connection.outputMessages) {
				if(outputMessages.contains(outputMessage) == false)
					outputMessages.add(outputMessage);
			}
		}
		return outputMessages;
	}
	
	public Connection getConnection(String inputMessageName) {
		Connection ret = null;
		for(Connection connection : connections) {
			if(inputMessageName.equals(connection.inputMessage.getName()) == true)
			{
				ret = connection;
				break;
			}
		}
		return ret;
	}
	
	public void addConnection(Connection connection) {
		connections.add(connection);
		connection.setOwner(this);
	}
	
	public void addConnections(List<Connection> connections) {
		for (Connection connection : connections) {
			addConnection(connection);
		}
	}
	
	public void removeConnection(String inputMessageName) {
		Connection connection = getConnection(inputMessageName);
		if(connection != null) {
			connections.remove(connection);
		}
	}
	
	public List<IOutput> getOutputMessages(IInput inputMessage) {
		List<IOutput> outputMessages = new ArrayList<IOutput>();
		for(Connection connection : connections) {
			if(connection.inputMessage.equals(inputMessage)) {
				outputMessages.addAll(Arrays.asList(connection.outputMessages));
			}
		}
		return outputMessages;
	}
	
	public List<Parameter> getParameters(IMessage message) {
		return message.getParameters();
	}
	
	public boolean hasInputType(Type type) {
		return hasStringType(new ArrayList<IMessage>(getInputMessages()), type);
	}
	
	public boolean hasOutputType(Type type) {
		return hasStringType(new ArrayList<IMessage>(getOutputMessages()), type);
	}
	
	private boolean hasStringType(List<IMessage> messages, Type type) {
		boolean hasType = false;
		for(IMessage massage : messages) {
			for(Parameter parameter : massage.getParameters()) {
				if(parameter.type == type) {
					hasType = true;
				}
			}
		}
		return hasType;
	}
	
	public String toDisplay() {
		String display = "";
		List<IMessage> inputMessages = new ArrayList<IMessage>(this.getInputMessages());
		List<IMessage> outputMessages = new ArrayList<IMessage>(this.getOutputMessages());
		display += "INPUT MESSAGES \n" + displayMessages(inputMessages);
		display += "\nOUTPUT MESSAGES \n" + displayMessages(outputMessages);
		return display.trim();
	}
	
	private String displayMessages(List<IMessage> messages) {
		String display = "";
		for(IMessage method : messages) {
			display = display + "MESSAGE: " + method.getName() + "[";
			if(method.getParameters() != null && method.getParameters().isEmpty() == false) {
				for(Parameter parameter : method.getParameters()) {
					display = display + "(" + parameter.name + ";"+ parameter.type+ ")";
				}
			}
			display = display + "]";
		}
		return display;
	}
	
	protected <T> List<T> castToList(List<?> a, Class<T> klass) {
		List<T> list = new ArrayList<T>();
		for(Object object : a) {
			list.add(klass.cast(object));
		}
		return list;
	}
	
	public String getName() {
		return name;
	}
}
