package types;

public enum Type {
	INT,
	STRING,
	VOID,
	COMPOSITE
}
