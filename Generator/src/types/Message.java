package types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Message implements IMessage{
	protected final String name;
	protected final List<Parameter> parameters;
	protected InterfaceProvider ownerInterface;
	private Type type = null;
	
	protected enum Type {
		INPUT,
		OUTPUT
	}
	
	public Message(String name) {
		this.name = name;
		this.parameters = null;
	}
	
	public Message(String name, List<Parameter> parameters) {
		  this.name = name;
		  this.parameters = parameters;
		  setAsOwner(parameters);
	}
	
	public Message(String name, Parameter parameter) {
		this(name, Arrays.asList(parameter));
	}
	
	public void setOwner(InterfaceProvider provider) {
		this.ownerInterface = provider;
	}
	
	public InterfaceProvider getOwner() {
		return ownerInterface;
	}
	
	private void setAsOwner(List<Parameter> parameters) {
		for(Parameter param : parameters) {
			param.setOwner(this);
		}
	}
	
	public List<Parameter> getParameters() {
		return new ArrayList<Parameter>(parameters);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((parameters == null) ? 0 : parameters.hashCode());
		return result;
	}
	
	protected void setType(Type type) {
		this.type = type;
	}
	
	public boolean isInput() {
		return this.type == Type.INPUT;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parameters == null) {
			if (other.parameters != null)
				return false;
		} else if (!parameters.equals(other.parameters))
			return false;
		return true;
	}
	
	public String getName() {
		return name;
	}
}
