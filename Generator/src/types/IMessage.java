package types;

import java.util.List;

public interface IMessage {
	public String getName();
	public List<Parameter> getParameters();
	public void setOwner(InterfaceProvider owner);
	public InterfaceProvider getOwner();
}
