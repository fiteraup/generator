package types;

import java.util.Arrays;
import java.util.List;

public class Connection {
	public final IInput inputMessage;
	public final IOutput [] outputMessages;

	public Connection(IInput inputMessage, IOutput [] outputMessages) {
		super();                            
	    this.inputMessage = inputMessage;   
	    this.outputMessages = outputMessages;
	}
	
	public Connection(IInput inputMessage, List<IOutput> outputMessages) {
		this(inputMessage,  outputMessages != null ? outputMessages.toArray(new IOutput [outputMessages.size()]) 
				: null);
	}
	
	public Connection(IInput inputMessage, IOutput outputMessage) {
		this(inputMessage, new IOutput[] {outputMessage});
	}
	
	public void setOwner(InterfaceProvider provider) {
		inputMessage.setOwner(provider);
		for(IOutput outputMessage : outputMessages) {
			outputMessage.setOwner(provider);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((inputMessage == null) ? 0 : inputMessage.hashCode());
		result = prime * result
				+ ((outputMessages == null) ? 0 : outputMessages.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Connection other = (Connection) obj;
		if (inputMessage == null) {
			if (other.inputMessage != null)
				return false;
		} else if (!inputMessage.equals(other.inputMessage))
			return false;
		if (outputMessages == null) {
			if (other.outputMessages != null)
				return false;
		} else if (!Arrays.equals(outputMessages,other.outputMessages))
			return false;
		return true;
	}
	
	
}
