package generator;

public interface ILoc {
	public static String EXAMPLES_FOLDER = "examples";
	public static String DEFAULT_CLASSPATH = "interfaces";
	public static String FILES_FOLDER = "files";
	public static String GENERATED_FOLDER = "generated";
}
