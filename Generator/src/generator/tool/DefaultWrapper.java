package generator.tool;

import exceptions.Err;
import types.IInput;
import types.IMessage;
import types.IOutput;

public class DefaultWrapper implements IWrap {

	public String wrap(IInput input) {
		return input.getName();
	}

	public String wrap(IOutput output) {
		return output.getName();
	}

	public String wrap(IMessage message) {
		if(message instanceof types.IInput) {
			return wrap((types.IInput) message);
		} else
		if(message instanceof types.IOutput) {
			return wrap((types.IOutput) message);
		}
		Err.handleInternal("Message is neither input nor output");
		return null;
	}

}
