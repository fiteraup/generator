package generator.tool;

import generator.technology.ITechnologyTemplate;
import options.Options;

public interface IGenerator {
	public void constructSUT(Options options, ITechnologyTemplate technologyTemplate) throws Exception;
}
