package generator.tool;

import types.Type;
import generator.IGlobal;

import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JType;

public interface IParamTemplate extends IGlobal{
	public JType getType();
	public JType getParamType();
	public JInvocation createParam(JExpression value, int index, Type type);
}
