package generator.tool.tomte;

import types.IInput;
import types.IOutput;
import generator.tool.DefaultWrapper;

public class TOMTEWrapper extends DefaultWrapper{

	public String wrap(IInput input) {
		return "I"+input.getName().toUpperCase();
	}

	public String wrap(IOutput output) {
		return "O"+output.getName().toUpperCase();
	}

}
