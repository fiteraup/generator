package generator.tool.tomte;

import generator.tool.IWrap;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.yaml.snakeyaml.Yaml;

import types.IMessage;
import types.InterfaceProvider;
import types.Parameter;
import types.Type;

/**
 * A YamlBuilder constructs the yaml file needed for tomte from a list of messages.
 * @author Paul
 *
 */
public class TOMTEConfigBuilder {
	
	private IWrap wrapper = null;
	
	public TOMTEConfigBuilder(IWrap wrapper) {
		this.wrapper = wrapper;
	}
	
	public void testLoad() throws IOException {
		testLoad("web.yaml");
	}
	
	public void testLoad(String filePath) throws IOException {
			Scanner scanner = new Scanner(new File(filePath));
			String doc = "";
			doc = scanner.findWithinHorizon("([\\s]|.)*", 0);
		    Yaml yaml = new Yaml();
		    @SuppressWarnings("unchecked")
			Map<Object, Object> object = (Map<Object, Object>) yaml.load(doc);
		    System.out.println(object);
		    scanner.close();
	}
	
	private String type2yaml(Type paramType) {
		return "Integer";
	} 
	
	private Map<String,Object> createMapForMessages(List<IMessage> messages) {
		Map<String,Object> messageMap = new LinkedHashMap<String,Object>();
		for(IMessage message : messages) {
			List<String> inputTypes = new ArrayList<String>();
			for(Parameter parameter : message.getParameters()) {
				inputTypes.add(type2yaml(parameter.type));
			}
			messageMap.put(wrapper.wrap(message), inputTypes);
		}
		return messageMap;
	}
	
	public Map<String, Object> parseInterfaceFile(InterfaceProvider interf) {
		Map<String, Object> yamlMap = new LinkedHashMap<String, Object>();
		System.out.println(interf.getInputMessages());
		Map<String,Object> inputInterfaces = createMapForMessages(new ArrayList<IMessage>(interf.getInputMessages()));
		Map<String,Object> outputInterfaces = createMapForMessages(new ArrayList<IMessage>(interf.getOutputMessages()));
		List<String> constants = new ArrayList<String>();
		yamlMap.put("name", interf.getName());
		yamlMap.put("constants", constants);
		yamlMap.put("inputInterfaces", inputInterfaces);
		yamlMap.put("outputInterfaces", outputInterfaces);
		return yamlMap;
	}
	
	public void buildFile(Map<String, Object> yamlMap, String filePath) throws Exception{
		File file = new File(filePath);
		if(file.exists() == false) {
			file.createNewFile();
		}
		Yaml yaml = new Yaml();
		yaml.dump(yamlMap, new PrintWriter(file));
	}
	
	public void buildSutInfoFromInterface(InterfaceProvider intrf, String filePath) throws Exception{
		Map<String, Object> valueMap = parseInterfaceFile(intrf);
		buildFile(valueMap,filePath);
	}
	
	
	public static void main(String args[]) throws Exception {
		//RMIParser parser = new RMIParser();
		TOMTEConfigBuilder builder = new TOMTEConfigBuilder(new TOMTEWrapper());
//		TomteTemplate generator = new TomteTemplate(parser, builder);
//		generator.generateSUT("A.java");
		builder.testLoad();
//		builder.testLoad("generated\\sutinfo.yaml");
//		System.out.println();
		//System.out.println(parser.getPackageString(testFile));
		//System.out.println(parser.getClassNameFromPath(testFile.getPath()));
	}

}