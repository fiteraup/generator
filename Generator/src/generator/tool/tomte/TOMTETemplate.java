package generator.tool.tomte;

import sut.*;
import generator.ILoc;
import generator.mappers.TOMTEMapper;
import generator.naming.ITOMTENaming;
import generator.tool.AbstractToolTemplate;
import generator.tool.IInput;
import generator.tool.IOutput;
import generator.tool.IToolTemplate;
import generator.tool.IWrap;

import java.util.ArrayList;

import options.Tool;

import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
/**
 * TomteTemplate class, constructs the main methods that make up a Tomte Sut wrapper. 
 * @author Paul
 *
 */
public class TOMTETemplate extends AbstractToolTemplate implements ITOMTENaming, IToolTemplate {

	private static final TOMTEMapper tomteMapper = new TOMTEMapper(codeModel); 
	
	protected JMethod getIntMethod;

	private TOMTEConfigBuilder tomteConfigBuilder;

	
	public TOMTETemplate() {
		super(Tool.TOMTE);
		this.tomteConfigBuilder = new TOMTEConfigBuilder(wrapper);
	}
	
	protected JDefinedClass constructClass() throws Exception {
		ArrayList<Class<?>> interfaces = new ArrayList<Class<?>>();
		interfaces.add(SutInterface.class);
		return constructClass("Sut",null, interfaces, "sut");
	}
	
	protected void constructReset() {
		super.constructReset(RESET_METHOD);
	}
		
	protected void constructExecute(){
		JMethod parseMethod = sut.method(JMod.PUBLIC, tomteMapper.outputType, PARSE_METHOD);
		technologyTemplate.constructExecute(parseMethod);
	}
	
	
	
	protected void writeFiles() throws Exception{
		super.writeFiles();
		tomteConfigBuilder.buildSutInfoFromInterface(intrf, ILoc.GENERATED_FOLDER + "/sutinfo.yaml");
	}
	
	public IWrap wrapper() {
		return new TOMTEWrapper();
	}
	
	public IInput abstractInput() {
		return new TOMTEInput(tomteMapper, sut);
	}

	public IOutput abstractOutput() {
		return new TOMTEOutput(tomteMapper, sut);
	}
}
