package generator.tool.tomte;

import generator.mappers.TOMTEMapper;
import generator.naming.ITOMTENaming;
import generator.tool.AbstractOutput;
import types.Type;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JConditional;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class TOMTEOutput  extends AbstractOutput implements ITOMTENaming{
	private final TOMTEMapper tomteMapper;
	private final JDefinedClass sut;
	private JMethod stringMapMethod;
	public TOMTEOutput(TOMTEMapper tomteMapper, JDefinedClass sut) {
		this.tomteMapper = tomteMapper;
		this.sut = sut;
	}
	
	public JType getType() {
		return tomteMapper.outputType;
	}
	
	public JType getParamType() {
		return tomteMapper.paramClass;
	}
	
	public JExpression createNew() {
		return JExpr._new(getType()).arg(JExpr.lit("OERROR"));
	}

	public JInvocation addParam(JVar context, JExpression param) {
		return context.invoke(GETPARAM_METHOD).invoke("add").arg(param);
	}
	
	private JExpression processValue(JExpression value, Type type) {
		JExpression result = null;
		switch(type) {
		case INT: result = value; break;
		case STRING: result = JExpr.invoke(getMapStringMethod()).arg(value); break;
		default: 
			System.err.println("Error at processValue");
			System.exit(0);
		}
		return result;
	}
		
	private JMethod getMapStringMethod() {
		if(stringMapMethod != null) {return stringMapMethod;}
		JClass getStringMapType = globalMapper.hashMapClass.narrow(globalMapper.stringClass, globalMapper.integerClass );  
		JFieldVar map = sut.field(JMod.STATIC | JMod.PRIVATE, getStringMapType, "mapStringMap", JExpr._new(getStringMapType));
		stringMapMethod = sut.method(JMod.PRIVATE, globalMapper.integerClass, "mapString");
		JVar strValue = stringMapMethod.param(globalMapper.stringClass, "stringValue");
		JVar intValue = stringMapMethod.body().decl(globalMapper.integerClass, "intValue", JExpr._null());
		JConditional ifBranch = stringMapMethod.body()._if(map.invoke("containsKey").arg(strValue).not());
		ifBranch._then().block().assign(intValue, map.invoke("size"));
		ifBranch._then().block().assign(intValue, intValue.incr());
		ifBranch._then().block().add(map.invoke("put").arg(strValue).arg(intValue));
		ifBranch._else().block().assign(intValue, map.invoke("get").arg(strValue));
		stringMapMethod.body()._return(intValue);
		return stringMapMethod;
	}

	@Override
	public JType getTypeMapping(Type type) {
		return codeModel.INT;
	}

	public JInvocation createParam(JExpression value, int index, Type type) {
		return JExpr._new(tomteMapper.paramClass).arg(processValue(value,type)).arg(JExpr.lit(index));
	}

	
/*
 * The input output from util packaged have been modified so this construct is no longer
 * needed. :)
 */
//	public JExpression createNewSingle(String symbol, JExpression param) {
//		JExpression paramList = globalMapper.arraysClass.staticInvoke("asList").arg(param);
//		return JExpr._new(tomteMapper.paramClass).arg(JExpr.lit(symbol)).arg(paramList);
//	}
}
