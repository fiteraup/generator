package generator.tool.tomte;

import exceptions.Err;
import generator.TypeMapper;
import generator.mappers.TOMTEMapper;
import generator.naming.ITOMTENaming;
import generator.tool.IInput;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import types.Parameter;
import types.Type;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JConditional;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JOp;
import com.sun.codemodel.JType;
import com.sun.codemodel.JTypeVar;
import com.sun.codemodel.JVar;

public class TOMTEInput implements IInput, ITOMTENaming {
	private final TOMTEMapper tomteMapper;
	private final JDefinedClass sut;
	private JMethod getIntMethod;
	private JMethod stringFetchMethod;
	private Map<Parameter, JFieldVar> defParamMap = new HashMap<Parameter, JFieldVar>();
	private JMethod getDefaultValueMethod;

	public TOMTEInput(TOMTEMapper tomteMapper, JDefinedClass sut) {
		this.tomteMapper = tomteMapper;
		this.sut = sut;
	}

	public JType getType() {
		return tomteMapper.inputType;
	}

	public JType getParamType() {
		return tomteMapper.inputType;
	}

	public JInvocation getInputSymbol(JExpression var) {
		return var.invoke(GETMETHODNAME_METHOD);
	}

	public JExpression getParameterValue(JExpression input, Parameter param) {
		JExpression value = null;
		if(param.getDefaultValues().isEmpty() == true) {
			value = getGeneratedValue(input, param);
		} else {
			value = getDefaultValue(input, param);
		}
		return value;
	}

	private JExpression getGeneratedValue(JExpression input, Parameter param) {
		JExpression value = null;
		switch (param.type) {
		case INT:
			value = getInt(input, param);
			break;
		case STRING:
			value = JExpr.invoke(fetchGetStringMethod()).arg(
					getInt(input, param));
			break;
		case COMPOSITE:
			Err.handleInternal("Internal error at getParameterValue, shouldn't have composite around here");
			break;
		default:
			Err.handleInternal("Internal error at getParameterValue");
			break;
		}
		return value;
	}
	
	private JInvocation getInt(JExpression input, Parameter param) {
		return JExpr.invoke(fetchGetIntMethod()).arg(input)
				.arg(JExpr.lit(param.index));
	}

	private JFieldVar getDefValueList(Parameter param) {
		String listName = param.getOwner().getName().toLowerCase() + param.name
				+ "ValueList";
		JFieldVar listField = defParamMap.get(param);
		if (listField == null) {
			if (param.type == Type.COMPOSITE) {
				Err.handleNotSupported(" composite default values ");
			}
			JExpression listFieldValues = getDefaultValuesList(param);
			listField = sut.field(JMod.STATIC | JMod.PRIVATE,
					globalMapper.abstractListClass.narrow(TypeMapper.getType(param)), listName, listFieldValues);
			defParamMap.put(param, listField);
		} 
		return listField;
	}

	private JExpression getDefaultValuesList(Parameter param) {
		List<String> defValues = param.getDefaultValues();
		JInvocation createListInvocation = globalMapper.arraysClass.staticInvoke("asList");
		for (String value : defValues) {
			createListInvocation = createListInvocation.arg(TypeMapper.getValueFromString(value, param));
		}
		return createListInvocation;
	}

	private JExpression getDefaultValue(JExpression input, Parameter param) {
		JFieldVar listField = getDefValueList(param);
		JExpression defaultValue = JExpr.invoke(getDefaultValueMethod()).arg(listField).arg(getInt(input,param));
		return defaultValue;
	}
	
	private JMethod getDefaultValueMethod() {
		if(getDefaultValueMethod != null) {
			return getDefaultValueMethod;
		}
		getDefaultValueMethod = sut.method(JMod.PRIVATE, globalMapper.objectClass, "getValue");
		JTypeVar t = getDefaultValueMethod.generify("T");
		getDefaultValueMethod.type(t);
		JVar valueList = getDefaultValueMethod.param(globalMapper.abstractListClass.narrow(t), "defaultValueList");
		JVar index = getDefaultValueMethod.param(codeModel.INT, "index");
		JVar length = getDefaultValueMethod.body().decl(codeModel.INT, "defaultValuesNumber", valueList.invoke("size"));
		JExpression defaultValue = JOp.cond(length.gt(index), valueList.invoke("get").arg(index) , 
				valueList.invoke("get").arg(length.minus(JExpr.lit(1))));
		getDefaultValueMethod.body()._return(defaultValue);
		return getDefaultValueMethod;
	}

	private JMethod fetchGetIntMethod() {
		if (getIntMethod != null) {
			return getIntMethod;
		}
		getIntMethod = sut.method(JMod.PRIVATE, codeModel.INT, GETINT_METHOD);
		JVar param = getIntMethod.param(getType(), "params");
		JVar position = getIntMethod.param(codeModel.INT, "pos");
		JVar integerValue = getIntMethod.body().decl(
				globalMapper.integerClass,
				"integerValue",
				param.invoke(GETPARAM_METHOD).invoke("get").arg(position)
						.invoke("getValue"));
		getIntMethod.body()._return(integerValue);
		return getIntMethod;
	}

	private JMethod fetchGetStringMethod() {
		if (stringFetchMethod != null) {
			return stringFetchMethod;
		}
		JClass getStringMapType = globalMapper.hashMapClass.narrow(
				globalMapper.integerClass, globalMapper.stringClass);
		JFieldVar map = sut.field(JMod.STATIC | JMod.PRIVATE, getStringMapType,
				"getStringMap", JExpr._new(getStringMapType));
		stringFetchMethod = sut.method(JMod.PRIVATE, globalMapper.stringClass,
				GETSTRING_METHOD);
		JVar intValue = stringFetchMethod.param(globalMapper.integerClass,
				"intValue");
		JVar strValue = stringFetchMethod.body().decl(globalMapper.stringClass,
				"stringValue", JExpr._null());
		JConditional ifBranch = stringFetchMethod.body()._if(
				map.invoke("containsKey").arg(intValue));
		ifBranch._then().assign(strValue, map.invoke("get").arg(intValue));
		ifBranch._else().assign(strValue, JExpr.lit("GENSTR").plus(intValue));
		ifBranch._else().add(map.invoke("put").arg(intValue).arg(strValue));
		stringFetchMethod.body()._return(strValue);
		return stringFetchMethod;
	}

	@Override
	public JInvocation createParam(JExpression value, int index, Type type) {
		return null;
	}

}
