package generator.tool;

import types.Type;

import com.sun.codemodel.JExpression;
import com.sun.codemodel.JType;


public interface IOutput extends IParamTemplate {
	public JExpression createNew();
	public JExpression createNew(String symbol);
	public JExpression createNewList(String symbol, JExpression params);
	public JExpression createNewSingle(String symbol, JExpression param);
	public JType getTypeMapping(Type type);
}
