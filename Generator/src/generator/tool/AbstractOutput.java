package generator.tool;

import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;

public abstract class AbstractOutput implements IOutput{
	
	public JExpression createNew() {
		return JExpr._new(getType());
	}
	
	public JExpression createNew( String symbol) {
		return JExpr._new(getType()).arg(JExpr.lit(symbol));
	}
	
	public JExpression createNewList(String symbol, JExpression params) {
		return JExpr._new(getType()).arg(JExpr.lit(symbol)).arg(params);
	}
	
	public JExpression createNewSingle(String symbol, JExpression param) {
		return JExpr._new(getType()).arg(JExpr.lit(symbol)).arg(param);
	}
	
		
	protected void notSet(String element) {
		System.err.println(element + " not set");
		System.exit(0);
	}
}
