package generator.tool;

import types.Parameter;
import types.Type;

import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;

public interface IInput extends IParamTemplate{
	public JInvocation getInputSymbol(JExpression var);
	public JExpression getParameterValue(JExpression var, Parameter parameter);
	public JInvocation createParam(JExpression value, int index, Type type);
}
