package generator.tool;

import generator.IGlobal;
import generator.technology.ICallback;

public interface IToolTemplate extends IGlobal, IGenerator, ICallback, IWrap{
	
}
