package generator.tool.simpa;

import generator.mappers.SIMPAMapper;
import generator.naming.ISIMPANaming;
import generator.technology.ICallback;
import generator.tool.AbstractToolTemplate;
import generator.tool.IInput;
import generator.tool.IOutput;
import generator.tool.IToolTemplate;

import java.util.ArrayList;
import java.util.List;

import options.Tool;
import types.IMessage;
import types.Parameter;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

import drivers.efsm.EFSMDriver;

/**
 * SimpaTemplate class, constructs the main methods that make up a Simpa Driver. It is applicable to both 
 * the generation of EFSM Drivers and that of Web Drivers. 
 * @author Paul
 *
 */
public class SIMPATemplate extends AbstractToolTemplate implements IToolTemplate, ISIMPANaming, ICallback{
	
	public SIMPATemplate() {
		super(Tool.SIMPA);
	}

	private static final SIMPAMapper simpaMapper = new SIMPAMapper(codeModel);
	
	protected void constructConstructor() {
		super.constructConstructor(true);
	}

	
	protected void constructImplementationMethods() {
		constructGetSystemName();
		constructGetInputSymbols();
		constructGetOutputSymbols();
		constructGetDefaultParameterValues();
		constructGetParameterNames();
		constructExecute();
	}

	private void constructGetParameterNames() {
		JClass paramListType = globalMapper.abstractListClass.narrow(globalMapper.stringClass);
		JClass returnedType = globalMapper.treeMapClass.narrow(globalMapper.stringClass, paramListType);
		JMethod getDefParamNamesMethod = sut.method(JMod.PUBLIC, returnedType , GETPARAMNAMES_METHOD);
		JVar paramNames = getDefParamNamesMethod.body().decl(
				returnedType, DEFPARAMNAMES_FIELD, JExpr._new(returnedType));
		putMessagesInMap(getDefParamNamesMethod,paramNames, new ArrayList<IMessage>(intrf.getInputMessages()));
		putMessagesInMap(getDefParamNamesMethod,paramNames, new ArrayList<IMessage>(intrf.getOutputMessages()));
		getDefParamNamesMethod.body()._return(paramNames);
	}
	
	private void putMessagesInMap(JMethod method, JVar map, List<IMessage> messages) {
		for(IMessage message : messages) {
			method.body().invoke(map, "put").arg(JExpr.lit(wrap(message))).arg(createParamList(message));
		}
	}
	
	private JInvocation createParamList(IMessage message) {
		JInvocation utilsInvocation = simpaMapper.utilsClass.staticInvoke("createArrayList");
		List<Parameter> parameters = intrf.getParameters(message);
		String mName = message.getName();
		if(parameters.isEmpty() == false) {
			for(Parameter parameter : parameters) {
				String pName = mName +parameter.name;
				utilsInvocation.arg(JExpr.lit(pName));
			}
		}
		else {
			utilsInvocation.arg(JExpr.lit(message.getName().toLowerCase()+"Param"));
		}
		return utilsInvocation;
	}


	protected void constructExecute() {
		JType returnedType = simpaMapper.outputType;
		JMethod executeMethod = sut.method(JMod.PUBLIC, returnedType , EXECUTE_METHOD);
		technologyTemplate.constructExecute(executeMethod);
	}

	private void constructGetDefaultParameterValues() {
		JClass typeList = globalMapper.abstractListClass;
		JClass concTypeList = globalMapper.arrayListClass;
		JClass paramListType = typeList.narrow(concTypeList.narrow(simpaMapper.paramClass));
		JClass concParamListType = concTypeList.narrow(concTypeList.narrow(simpaMapper.paramClass));
		JClass returnedType = globalMapper.hashMapClass.narrow(globalMapper.stringClass, paramListType);
		JMethod getDefParamValuesMethod = sut.method(JMod.PUBLIC, returnedType , GETDEFPARAMVALUES_METHOD);
		JVar paramValues = getDefParamValuesMethod.body().decl(
				returnedType, DEFPARAMVALUES_FIELD, JExpr._new(returnedType));
		JVar params = getDefParamValuesMethod.body().decl(
				concParamListType, "param", JExpr._null());
		for(types.IInput message : intrf.getInputMessages()) {
			getDefParamValuesMethod.body().directStatement("//"+message.getName());
			getDefParamValuesMethod.body().directStatement("//");
			getDefParamValuesMethod.body().assign(params, JExpr._new(concParamListType));
			addAllParam(getDefParamValuesMethod.body(), params, intrf.getParameters(message), 0, new ArrayList<String>());
			getDefParamValuesMethod.body().add(JExpr.invoke(paramValues, "put").arg(JExpr.lit(wrap(message))).arg(params));
		}
		getDefParamValuesMethod.body()._return(paramValues);
	}

	private void addAllParam(JBlock body,JVar params, List<Parameter> paramList, int level, List<String> iList) {
		if(level < paramList.size()) {
			for(int i = 0; i < getLength(paramList, level); i ++) {
				if(iList.size() > level) {
					iList.remove(level);
				}
				iList.add(level, getValue(paramList, level, i));
				addAllParam(body, params, paramList, level+1,iList);
			}
			
		} else {
			if(paramList.size() > 0)
				addValuesInvocation(body, params, iList);
		}
	}
	
	private void addValuesInvocation(JBlock body, JVar params, List<String> values) {
		JInvocation invocation = simpaMapper.utilsClass.staticInvoke("createArrayList");
		for(String value : values) {
			invocation.arg(abstractOutput().createParam(JExpr.lit(value), 0, types.Type.STRING));
		}
		
		body.add(JExpr.invoke(params, "add").arg(invocation));
	}
	
	private String getValue(List<Parameter> paramList, int level, int index) {
		return paramList.get(level).getDefaultValues().get(index);
	}

	private int getLength(List<Parameter> paramList, int level) {
		Parameter param = paramList.get(level);
		return param.getDefaultValues() != null ? param.getDefaultValues().size() : 0;
	}

	
	private void constructGetOutputSymbols() {
		List<String> symbols = new ArrayList<String>();
		for(types.IOutput message : intrf.getOutputMessages()) {
			symbols.add(wrap(message));
		}
		getSymbolsTemplate(GETOUTPUTSYMBOLS_METHOD, OUTPUTSYMBOLS_FIELD, symbols);
	}


	private void constructGetInputSymbols() {
		List<String> symbols = new ArrayList<String>();
		for(types.IInput message : intrf.getInputMessages()) {
			symbols.add(wrapper.wrap(message));
		}
		getSymbolsTemplate(GETINPUTSYMBOLS_METHOD, INPUTSYMBOLS_FIELD, symbols);
	}
	
	private void getSymbolsTemplate(String name, String fieldName, List<String> symbols) {
		JType returnedType = globalMapper.arrayListClass.narrow(globalMapper.stringClass);
		JMethod getSymbolsMethod = sut.method(JMod.PUBLIC, returnedType , name);
		JVar symbolList = getSymbolsMethod.body().decl(
				returnedType, fieldName, JExpr._new(returnedType));
		for(String symbol : symbols) {
			getSymbolsMethod.body().invoke(symbolList, "add").arg(JExpr.lit(symbol));
		}
		getSymbolsMethod.body()._return(symbolList);
	}

	private void constructGetSystemName() {
		JMethod getSystemNameMethod = sut.method(JMod.PUBLIC, globalMapper.stringClass , GETSYSNAME_METHOD);
		getSystemNameMethod.body()._return(JExpr.lit(intrf.getName().toUpperCase()));
	}


	protected void constructReset() {
		constructReset(RESET_METHOD);
	}

	protected JDefinedClass constructClass() throws Exception{
		String packageName =  "drivers.efsm.real";
		Class<?> superClass = EFSMDriver.class;
		return constructClass("Driver", superClass, null, packageName);
	}
	
	
	@Override
	public IInput abstractInput() {
		return new SIMPAInput(simpaMapper);
	}

	@Override
	public IOutput abstractOutput() {
		return new SIMPAOutput(simpaMapper);
	}
}
