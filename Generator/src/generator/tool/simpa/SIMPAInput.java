package generator.tool.simpa;

import types.Type;
import types.Parameter;
import generator.mappers.SIMPAMapper;
import generator.tool.IInput;

import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JType;

public class SIMPAInput implements IInput{
	private final SIMPAMapper simpaMapper;
	public SIMPAInput(SIMPAMapper simpaMapper) {
		this.simpaMapper = simpaMapper;
	}

	public JType getType() {
		return simpaMapper.inputType;
	}
	
	public JType getParamType() {
		return simpaMapper.paramClass;
	}

	public JInvocation getNew() {
		return JExpr._new(simpaMapper.inputType);
	}

	@Override
	public JInvocation getInputSymbol(JExpression var) {
		return var.invoke("getInputSymbol");
	}

	@Override
	public JExpression getParameterValue(JExpression var, Parameter param) {
		JInvocation invocation = var.invoke("getParameterValue").arg(JExpr.lit(param.index));
		if(param.type == Type.INT)
			invocation = globalMapper.integerClass.staticInvoke("valueOf").arg(invocation);
		return invocation;
	}

	@Override
	public JInvocation createParam(JExpression value, int index, Type type) {
		return null;
	}

}
