package generator.tool.simpa;

import generator.mappers.SIMPAMapper;
import generator.tool.AbstractOutput;
import generator.tool.IOutput;
import types.Type;

import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class SIMPAOutput extends AbstractOutput implements IOutput {
	private final SIMPAMapper simpaMapper;
	public SIMPAOutput(SIMPAMapper simpaMapper) {
		this.simpaMapper = simpaMapper;
	}

	public JType getType() {
		return simpaMapper.outputType;
	}
	
	public JType getParamType() {
		return simpaMapper.paramClass;
	}
	
	public JInvocation getNew() {
		return JExpr._new(simpaMapper.outputType);
	}

	public JInvocation addParam(JVar context, JExpression param) {
		return context.invoke("getParameters").invoke("add").arg(param);
	}

	@Override
	public JType getTypeMapping(Type type) {
		return globalMapper.stringClass;
	}

	@Override
	public JInvocation createParam(JExpression value, int index, Type type) {
		JExpression stringValue = type == Type.STRING ? value : globalMapper.stringClass.staticInvoke("valueOf").arg(value);
		return JExpr._new(simpaMapper.paramClass).arg(stringValue).arg(simpaMapper.typesClass.staticRef("STRING"));
	}

}
