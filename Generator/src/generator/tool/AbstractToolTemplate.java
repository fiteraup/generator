package generator.tool;

import exceptions.Err;
import generator.FileManager;
import generator.ILoc;
import generator.naming.IToolNaming;
import generator.technology.ITechnologyTemplate;

import java.util.List;

import options.Options;
import options.Tool;
import types.InterfaceProvider;

import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;

public abstract class AbstractToolTemplate implements IToolTemplate, IToolNaming {
	protected final Tool learner;
	protected InterfaceProvider intrf;
	protected ITechnologyTemplate technologyTemplate;
	protected JDefinedClass sut;
	protected JMethod ctor;
	protected IWrap wrapper = wrapper();
	protected boolean copyRequired = false;
	protected String buildDirectory = null;
	
	public AbstractToolTemplate(Tool learner) {
		this.learner = learner;
	}
	
	@Override
	public final void constructSUT(Options options,
			ITechnologyTemplate technologyTemplate) throws Exception {
		setup(options, technologyTemplate);
		constructFields();
		constructConstructor();
		constructReset();
		constructExecute();
		constructImplementationMethods();
		writeFiles();
	}

	protected void constructImplementationMethods() {
	}
	
	// To be implemented by any extending learner
	protected abstract void constructExecute();
	protected abstract void constructReset();
	protected abstract JDefinedClass constructClass() throws Exception;

	protected void setup(Options options, ITechnologyTemplate instanceBuilder) throws Exception{
		this.technologyTemplate = instanceBuilder;
		this.sut = constructClass();
		this.buildDirectory = options.globalOptions.buildPath;
		this.copyRequired = true;
		instanceBuilder.prepareTemplate(this, options);
		this.intrf = instanceBuilder.getInterfaceProvider();
	}

	protected JDefinedClass constructClass(String className, Class<?> extendedClass, List<Class<?>> implementedInterfaces, String packageName) throws Exception {
		JDefinedClass sut;
		if(packageName != null) {
			JPackage pkg = codeModel._package(packageName);
			sut = pkg._class(className);
		}
		else {
			sut = codeModel._class(className);
		}
		if(implementedInterfaces != null && implementedInterfaces.size() > 0) {
			for(Class<?> interfaceClass : implementedInterfaces) {
				sut._implements(interfaceClass);
			}
		}
		if( extendedClass != null) {
			sut._extends(extendedClass);
		}
		return sut;
	} 
	
	public JDefinedClass sut() {
		return sut;
	}
	
	protected void constructFields() {
		technologyTemplate.constructFields(sut);
		
	}
	
	protected void constructConstructor() {
		constructConstructor(false);
	}
	
	protected void constructConstructor(boolean callSuper) {
		ctor = sut.constructor(JMod.PUBLIC);
		if(callSuper == true) {
			ctor.body().add(JExpr.invoke("super"));
			}
		technologyTemplate.constructConstructor(ctor);
	}
	
	protected void constructReset(String methodName) {
		JMethod resetMethod = sut.method(JMod.PUBLIC, codeModel.VOID , methodName);
		technologyTemplate.constructReset(resetMethod);
	}
	
	protected void writeFiles() throws Exception{
		String lString = learner.toString().toLowerCase();
		if(copyRequired == true) {
			FileManager.copyFromTo(ILoc.FILES_FOLDER+"/"+lString, buildDirectory);
		}
		FileManager.buildToPath(buildDirectory);
	}
	
	public IWrap wrapper() {
		return new DefaultWrapper();
	}
	
	public String wrap(types.IInput input) {
		return wrapper.wrap(input);
	}
	
	public String wrap(types.IOutput output) {
		return wrapper.wrap(output);
	}
	
	public String wrap(types.IMessage message) {
		if(message instanceof types.IInput) {
			return wrap((types.IInput) message);
		} else
		if(message instanceof types.IOutput) {
			return wrap((types.IOutput) message);
		}
		Err.handleInternal("Message is neither input nor output");
		return null;
	}
}
