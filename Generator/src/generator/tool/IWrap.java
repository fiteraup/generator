package generator.tool;

public interface IWrap {
	public String wrap(types.IInput input);
	public String wrap(types.IOutput output);
	public String wrap(types.IMessage message);
}
