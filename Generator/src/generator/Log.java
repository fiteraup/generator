package generator;

import java.io.PrintStream;

/**
 * Log class. A light alternative to log4j. Like log4j, it prepends the line and class name to each logged message.
 */
public class Log {
	enum Level {
	WARN,
	LOG,
	ERROR
	}
	public static void warn(String message) {
		log(Level.WARN, message);
	}
	
	public static void log(String message) {
		log(Level.LOG, message);
	}

	public static void err(String message) {
		log(Level.ERROR, message);
	}
	
	private static void log(Level level, String message) {
		log(level, message, System.out);
	}
	
	/** Logs message prepending location of log invocation. To retrieve the location from which the log was called, 
	 * it navigates through the stack until it gets outside of the Log/Exception classes. */ 
	private static void log(Level level, String message, PrintStream writer) {
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		StackTraceElement relevantStackTraceElement = null;
		for(StackTraceElement element : stackTrace) {
			// nifty hardcoded way to get the correct stacktrace
			if(!element.getMethodName().equals("getStackTrace") && !element.getClassName().contains("Log") && !element.getClassName().contains("Err")) {
				relevantStackTraceElement = element;
				break;
			}
		}
		writer.println(level.name()+" (" + relevantStackTraceElement.getClassName() + ";" + relevantStackTraceElement.getMethodName() + ";" + relevantStackTraceElement.getLineNumber() + "): "+ message);
	}
}
