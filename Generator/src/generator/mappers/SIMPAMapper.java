package generator.mappers;

import tools.HTTPRequest;
import tools.HTTPResponse;
import tools.Utils;
import automata.efsm.Parameter;
import automata.efsm.ParameterizedInput;
import automata.efsm.ParameterizedOutput;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;

import drivers.efsm.EFSMDriver.Types;
import drivers.efsm.real.HighWebDriver;
import drivers.efsm.real.LowWebDriver;

public class SIMPAMapper extends AbstractMapper{

	public final JClass httpRequestClass;
	public final JClass httpResponseClass;
	public final JClass lowWebDriverClass;
	public final JClass highWebDriverClass;
	public final JClass outputType;
	public final JClass inputType;
	public final JClass paramClass;
	public final JClass utilsClass;
	public final JClass typesClass;
	
	
	public SIMPAMapper(JCodeModel cm) {
		super(cm);
		httpRequestClass = parseClass(cm, HTTPRequest.class);
		httpResponseClass = parseClass(cm, HTTPResponse.class);
		lowWebDriverClass = parseClass(cm, LowWebDriver.class);
		highWebDriverClass = parseClass(cm, HighWebDriver.class);
		inputType = parseClass(cm,ParameterizedInput.class);
		outputType = parseClass(cm,ParameterizedOutput.class);
		paramClass = parseClass(cm,Parameter.class);		
		utilsClass = parseClass(cm, Utils.class);
		typesClass = parseClass(cm, Types.class);
	}
}
