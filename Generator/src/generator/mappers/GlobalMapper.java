package generator.mappers;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;

/**
 * Create JClass and JTypes so that we don't have to include/ refer to them all in the Generator classes. 
 * The global mapper makes available commonly used types and all template classes have access to it. 
 * @author Paul
 *
 */
public class GlobalMapper extends AbstractMapper{
	
	public final JClass stringClass;
	public final JClass abstractListClass;
	public final JClass arrayListClass;
	public final JClass abstractMapClass;
	public final JClass hashMapClass;
	public final JClass treeMapClass;
	public final JClass systemClass;
	public final JClass exceptionClass;
	public final JClass outClass;
	public final JClass integerClass;
	public final JClass objectClass;
	public final JClass patternClass;
	public final JClass matcherClass;
	public final JClass arraysClass;
	
	public GlobalMapper(JCodeModel cm){
		super(cm);
		stringClass = parseClass(cm,String.class);
		arrayListClass = parseClass(cm,ArrayList.class);
		arraysClass = parseClass(cm, Arrays.class);
		abstractListClass = parseClass(cm, List.class);
		systemClass = parseClass(cm, System.class);
		outClass = parseClass(cm, PrintStream.class);
		abstractMapClass = parseClass(cm, Map.class);
		hashMapClass = parseClass(cm, HashMap.class);
		treeMapClass = parseClass(cm, TreeMap.class);
		exceptionClass = parseClass(cm, Exception.class);
		integerClass = parseClass(cm, Integer.class);
		objectClass = parseClass(cm, Object.class);
		patternClass = parseClass(cm, Pattern.class);
		matcherClass = parseClass(cm, Matcher.class);
	}
	
	public JClass createMapClass(JClass mapClass, JClass keyClass, JClass valueClass) {
		JClass resultedClass = null;
		if(abstractMapClass.isAssignableFrom(mapClass) == true) {
			resultedClass = mapClass.narrow(keyClass, valueClass);
		}
		else {
			System.err.println("The class "+mapClass.name()+ " is not a map");
			System.exit(0);
		}
		return resultedClass;
	}
	
}
