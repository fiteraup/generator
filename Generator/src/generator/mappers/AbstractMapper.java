package generator.mappers;

import java.util.HashMap;
import java.util.Map;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JType;

/***
 * An abstract mapper providing parse functionality with the facility of memory, so when the jclasses generated 
 * are stored in a map with their associated classes and retrieved from that, whenever asked for again. 
 * <p>
 * The mappers help move all class dependencies on outside libraries (like tomte/RMI libraries) to this package. 
 */
public abstract class AbstractMapper {
	
	private static final Map<Class<?>,JType> c2tMap = new HashMap<Class<?>,JType>();
	private static final Map<Class<?>,JClass> c2cMap = new HashMap<Class<?>,JClass>();
	
	private static void handleClassParseException(Exception e, Class <?> c) {
		System.err.println("Error parsing "+c.getName() + " to a J type");
		e.printStackTrace();
		System.exit(0);
	}
	
	public static JType parseType(JCodeModel cm,Class<?> c){
		JType inferredType = null;
		try {
			if(c2tMap.containsKey(c)) {
				inferredType = c2tMap.get(c);
			}
			else {
				inferredType = cm._ref(c);
				c2tMap.put(c, inferredType);
			}
		} 
		catch(Exception e) {
			handleClassParseException(e, c);
		}
		return inferredType;
	}
	
	public static JClass parseClass(JCodeModel cm, Class<?>c){
		JClass inferredClass = null;
		try {
			if(c2cMap.containsKey(c)) {
				inferredClass = c2cMap.get(c);
			}
			else {
				inferredClass = cm.ref(c);
				c2cMap.put(c, inferredClass);
			}
		} 
		catch(Exception e) {
			handleClassParseException(e, c);
		}
		return inferredClass;
	}
	
	public AbstractMapper(JCodeModel cm) {
		
	}
	
	public JClass getJClass(JCodeModel cm, Class<?> c) {
		return parseClass(cm, c);
	}
}
