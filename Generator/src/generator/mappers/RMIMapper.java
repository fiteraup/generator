package generator.mappers;

import java.rmi.Naming;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JType;

public class RMIMapper extends AbstractMapper{

	public final JType rmiNamingType;
	public final JClass rmiNamingClass;
	
	public RMIMapper(JCodeModel cm) {
		super(cm);
		rmiNamingType = parseType(cm, Naming.class);
		rmiNamingClass = parseClass(cm, Naming.class);
	}
}
