package generator.mappers;

import util.InputAction;
import util.OutputAction;
import util.Parameter;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JType;

public class TOMTEMapper extends AbstractMapper {

	public final JType outputType;
	public final JType inputType;
	public final JClass paramClass;

	public TOMTEMapper(JCodeModel cm){
		super(cm);
		inputType = parseType(cm,InputAction.class);
		outputType = parseType(cm,OutputAction.class);
		paramClass = parseClass(cm,Parameter.class);
	}
}
