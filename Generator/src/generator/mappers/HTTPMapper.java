package generator.mappers;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import tools.CookieManager;
import tools.HTTPRequest;
import tools.HTTPResponse;
import tools.TCPSend;
import tools.loggers.LogManager;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;

public class HTTPMapper extends AbstractMapper{
	public final JClass httpRequestClass;
	public final JClass httpResponseClass;
	public final JClass cookieManagerClass;
	public final JClass methodClass;
	public final JClass versionClass;
	public final JClass logManagerClass;
	public final JClass tcpSendClass;
	public final JClass base64Encoder;
	public final JClass base64Decoder;
	public HTTPMapper(JCodeModel cm) {
		super(cm);
		httpRequestClass = parseClass(cm,HTTPRequest.class);
		httpResponseClass = parseClass(cm,HTTPResponse.class);
		cookieManagerClass = parseClass(cm,CookieManager.class);
		logManagerClass = parseClass(cm, LogManager.class);
		tcpSendClass = parseClass(cm, TCPSend.class);
		methodClass = parseClass(cm, HTTPRequest.Method.class);
		versionClass = parseClass(cm, HTTPRequest.Version.class);
		base64Encoder = parseClass(cm, BASE64Encoder.class);
		base64Decoder = parseClass(cm, BASE64Decoder.class);
	}

}
