package generator;

import generator.mappers.HTTPMapper;
import generator.technology.http.LoggerTemplate;
import generator.technology.http.RequestTemplate;
import generator.technology.http.ResponseTemplate;

public class Factory implements IGlobal{
	private static HTTPMapper httpMapper = null;
	private static HTTPMapper getHTTPMapper() {
		if(httpMapper == null) {
			httpMapper = new HTTPMapper(codeModel);
		}
		return httpMapper;
	}
	
	public static RequestTemplate getRequestTemplate() {
		return new RequestTemplate(getHTTPMapper());
	}
	
	public static ResponseTemplate getResponseTemplate() {
		return new ResponseTemplate(getHTTPMapper());
	}
	
	public static LoggerTemplate getLoggerTemplate() {
		return new LoggerTemplate(getHTTPMapper());
	}
}
