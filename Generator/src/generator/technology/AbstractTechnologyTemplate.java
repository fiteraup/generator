package generator.technology;

import exceptions.Err;
import generator.naming.ITechnologyNaming;
import generator.tool.IInput;
import generator.tool.IOutput;
import generator.tool.IWrap;
import options.Options;
import parsers.IParser;
import types.Interface;
import types.InterfaceProvider;

import com.sun.codemodel.JCatchBlock;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JTryBlock;
import com.sun.codemodel.JVar;

public abstract class AbstractTechnologyTemplate<T extends Interface> implements ITechnologyNaming,
		ITechnologyTemplate {
	protected IInput in;
	protected IOutput out;
	protected IWrap wrapper;
	protected JDefinedClass sut;
	protected T intrf;
	
	public AbstractTechnologyTemplate() {
		
	}
	
	
	protected abstract IParser getParser(Options options);
	
	protected Interface parse(Options options) {
		IParser parser = getParser(options);
		Interface intrf = null;
		try {
			intrf = parser.parse(options);
		} catch(Exception e) {
			Err.handleParseException(e, this.getClass().getSimpleName());
		}
		return intrf;
	}
	
	public InterfaceProvider getInterfaceProvider() {
		return intrf;
	}

	@SuppressWarnings("unchecked")
	public void prepareTemplate(ICallback callback, Options options)  {
		in = callback.abstractInput();
		out = callback.abstractOutput();
		sut = callback.sut();
		intrf = (T)parse(options);
		wrapper = callback.wrapper();
	}

	protected void catchException(JTryBlock context) {
		JCatchBlock catchBlock = context._catch(globalMapper.exceptionClass);
		JVar x = catchBlock.param("_x");
		catchBlock.body().invoke(globalMapper.systemClass.staticRef("err"), "println").arg(x);
		catchBlock.body().staticInvoke(globalMapper.systemClass,"exit").arg(JExpr.lit(0));
	}
	
	public String wrap(types.IInput input) {
		return wrapper.wrap(input);
	}
	
	public String wrap(types.IOutput output) {
		return wrapper.wrap(output);
	}
}
