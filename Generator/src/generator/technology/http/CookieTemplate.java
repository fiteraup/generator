package generator.technology.http;

import generator.naming.IHTTPNaming;

import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldRef;
import com.sun.codemodel.JInvocation;

public class CookieTemplate implements IHTTPNaming{
	private final JFieldRef cookie;
	public CookieTemplate(JFieldRef cookie) {
		this.cookie = cookie;
	}
	
	public JInvocation updateCookies(JExpression response) {
		return cookie.invoke("updateCookies").arg(response.invoke("getHeader").arg(SET_COOKIE));
	}
	
	public JInvocation getCookieLine() {
		return cookie.invoke("getCookieLine");
	}
	
	public JInvocation reset() {
		return cookie.invoke("reset");
	}
}
