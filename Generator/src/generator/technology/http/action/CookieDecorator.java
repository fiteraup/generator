package generator.technology.http.action;

import generator.technology.http.CookieTemplate;
import generator.technology.http.RequestTemplate;
import types.http.HTTPAction;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JExpr;

public class CookieDecorator extends RequestActionDecorator {

		private CookieTemplate cookieManager;

		public CookieDecorator(RequestTemplate requestTemplate, CookieTemplate cookieTemplate) {
			super(requestTemplate);
			this.cookieManager = cookieTemplate;
		}

		public void decorate(JBlock context, HTTPAction action) {
			context.add(requestor.addHeader(JExpr.lit("Cookie"), cookieManager.getCookieLine()));
		}
		
		
}
