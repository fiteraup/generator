package generator.technology.http.action;

import types.Parameter;
import types.http.HTTPAction;
import generator.technology.http.RequestTemplate;
import generator.tool.IInput;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JVar;

public class ParamDecorator extends RequestActionDecorator {

	private JVar input;
	private IInput in;
	public ParamDecorator(RequestTemplate requestTemplate, JVar inputVar, IInput in) {
		super(requestTemplate);
		this.input = inputVar;
		this.in = in;
	}
	
	public void decorate(JBlock context, HTTPAction action) {
		for(Parameter param : action.getParameters()) {	
			addDataToReq(context, JExpr.lit(param.name), in.getParameterValue(input, param));
		}
	}
	
	private void addDataToReq(JBlock contextBlock, JExpression action, JExpression value) {
		contextBlock.add(requestor.addData(action, value));
	}
}
