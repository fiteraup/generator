package generator.technology.http.action;

import generator.technology.http.RequestTemplate;
import types.http.HTTPAction;
import types.http.HTTPKeyValuePair;

import com.sun.codemodel.JBlock;

public class KeyValueDecorator extends RequestActionDecorator{

	public KeyValueDecorator(RequestTemplate requestTemplate) {
		super(requestTemplate);
	}

	public void decorate(JBlock context, HTTPAction action) {
		if(action.getKeyValuePairs() != null) {
			for(HTTPKeyValuePair param : action.getKeyValuePairs()) {
				context.add(requestor.addData(param.getKey(), param.getValue()));
			}
		}
	}
}
