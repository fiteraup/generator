package generator.technology.http.action;

import generator.technology.http.IActionDecorator;
import generator.technology.http.RequestTemplate;

public abstract class RequestActionDecorator implements IActionDecorator{
	protected RequestTemplate requestor;
	public RequestActionDecorator(RequestTemplate requestTemplate) {
		this.requestor = requestTemplate;
	}
}
