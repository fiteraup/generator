package generator.technology.http.action;

import generator.technology.http.IActionDecorator;
import generator.technology.http.RequestTemplate;
import types.http.HTTPAction;
import types.http.HTTPInterface;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JExpr;

public class AuthDecorator extends RequestActionDecorator implements IActionDecorator{

	public AuthDecorator(RequestTemplate requestTemplate) {
		super(requestTemplate);
	}

	public void decorate(JBlock context, HTTPAction action) {
		HTTPInterface httpInterface = (HTTPInterface) action.getOwner();
		if( httpInterface.getAuth() != null) {
			String authString = httpInterface.getAuth().getAuthString();
			requestor.addHeader(JExpr.lit("Authorization") , JExpr.lit(authString));
		}
	}

}
