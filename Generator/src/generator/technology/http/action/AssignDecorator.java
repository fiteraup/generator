package generator.technology.http.action;

import types.http.HTTPAction;
import generator.technology.http.RequestTemplate;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JVar;

public class AssignDecorator extends RequestActionDecorator {
	private String link;
	private JVar req;
	
	public AssignDecorator(JVar req, RequestTemplate requestTemplate, String link) {
		super(requestTemplate);
		this.link = link;
		this.req = req;
	}
	
	public void decorate(JBlock context, HTTPAction action) {
		context.assign(this.req, requestor.newReq(context,action.getName(), action.methodType, link));
	}

}
