package generator.technology.http.action;

import generator.technology.http.IActionDecorator;
import generator.technology.http.ResponseDecorator;
import generator.technology.http.ResponseTemplate;

import com.sun.codemodel.JVar;


public abstract class ResponseActionDecorator extends ResponseDecorator implements IActionDecorator {
	public ResponseActionDecorator(JVar res, ResponseTemplate responder) {
		super(res, responder);
	}
}