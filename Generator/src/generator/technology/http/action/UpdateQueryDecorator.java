package generator.technology.http.action;

import generator.IGlobal;
import generator.technology.http.IActionDecorator;
import generator.technology.http.ResponseTemplate;

import java.util.List;
import java.util.Map;

import types.http.HTTPAction;
import types.http.HTTPKeyQueryPair;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JConditional;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JForEach;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;

public class UpdateQueryDecorator extends ResponseActionDecorator implements IActionDecorator, IGlobal {

		private Map<HTTPKeyQueryPair, JFieldVar> queryFields;
		private JMethod queryResponseMethod = null;
		private JMethod indexOfMethod = null;
		private JDefinedClass sut = null;

		public UpdateQueryDecorator(JVar res, JDefinedClass sut, ResponseTemplate responder, Map<HTTPKeyQueryPair, JFieldVar> queryFields) {
			super(res, responder);
			this.queryFields = queryFields;
			this.sut = sut;
		}

		public void decorate(JBlock context, HTTPAction action) {
			for(HTTPKeyQueryPair pair : action.getUpdatedQueries()) {
				JVar field = queryFields.get(pair);
				List<String> hints = pair.getQuery().getHints();
				String param = pair.getQuery().getName();
				if(hints != null && hints.size() > 0) {
					JExpression hintVar = createArray(hints);
					JExpression contentVar = responder.content(res);
					JExpression paramVar = JExpr.lit(param);
					constructQueryResponse(sut);
					context.assign(field, JExpr.invoke(queryResponseMethod).
							arg(contentVar).arg(paramVar).arg(hintVar));
				}
			}
		}		
		
		public JExpression createArray(List<String> list) {
			JInvocation invoc = globalMapper.arraysClass.staticInvoke("asList");
			for(String string : list) {
				invoc.arg(JExpr.lit(string));
			}
			return invoc;
		}
		
		private void constructQueryResponse(JDefinedClass context) {
			constructIndexOf(context);
			if(queryResponseMethod != null) return;
			queryResponseMethod = context.method(JMod.PRIVATE, globalMapper.stringClass, "queryResponse");
			JVar content = queryResponseMethod.param(globalMapper.stringClass, "content");
			JVar param = queryResponseMethod.param(globalMapper.stringClass, "paramName");
			JVar hints = queryResponseMethod.param(globalMapper.abstractListClass.narrow(globalMapper.stringClass), "hints");
			JVar value = queryResponseMethod.body().decl(globalMapper.stringClass, "value", JExpr.lit(""));
			JVar poz = queryResponseMethod.body().decl(codeModel.INT, "poz");
			JForEach jFor = queryResponseMethod.body().forEach(globalMapper.stringClass, "hint", hints);
			jFor.body().assign(poz, content.invoke("indexOf").arg(JExpr.ref("hint")));
			jFor.body().assign(content, content.invoke("substring").arg(poz.plus(JExpr.ref("hint").invoke("length"))));
			checkBadPosition(jFor.body(), poz);
			queryResponseMethod.body().assign(poz, content.invoke("indexOf").arg(param.plus(JExpr.lit("="))));
			checkBadPosition(queryResponseMethod.body(), poz);
			JInvocation newContent = content.invoke("substring").arg(poz.plus(param.invoke("length")).plus(JExpr.lit(1)));
			queryResponseMethod.body().assign(content, newContent);
			queryResponseMethod.body().assign(poz, JExpr.invoke(indexOfMethod).arg(JExpr.lit("\"|&|>")).arg(content));
			checkBadPosition(queryResponseMethod.body(), poz);
			queryResponseMethod.body().assign(value, content.invoke("substring").arg(JExpr.lit(0)).arg(poz));
			queryResponseMethod.body()._return(value);
		}
		
		private void checkBadPosition(JBlock body, JVar poz) {
			JConditional jIf = body._if(poz.eq(JExpr.lit(-1)));
			jIf._then()._return(JExpr.lit(""));
		}
		
		private void constructIndexOf(JDefinedClass context) {
			if(indexOfMethod != null) return;
			indexOfMethod = context.method(JMod.PRIVATE, codeModel.INT, "indexOf");
			JVar regex = indexOfMethod.param(globalMapper.stringClass, "regex");
			JVar string = indexOfMethod.param(globalMapper.stringClass, "string");
			JVar poz = indexOfMethod.body().decl(codeModel.INT, "pos", JExpr.lit(-1));
			JInvocation getMatcher = globalMapper.patternClass.staticInvoke("compile").arg(regex).invoke("matcher").arg(string);	
			JVar matcher = indexOfMethod.body().decl(globalMapper.matcherClass, "matcher", getMatcher);
			JConditional jIf = indexOfMethod.body()._if(matcher.invoke("find"));
			jIf._then().assign(poz, matcher.invoke("start"));
			indexOfMethod.body()._return(poz);
		}
	}
