package generator.technology.http.action;

import generator.technology.http.RequestTemplate;

import java.util.Map;

import types.http.HTTPAction;
import types.http.HTTPKeyQueryPair;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;

public class KeyQueryDecorator extends RequestActionDecorator{

	private Map<HTTPKeyQueryPair, JFieldVar> fields;

	public KeyQueryDecorator(RequestTemplate requestTemplate, Map<HTTPKeyQueryPair, JFieldVar> fields) {
		super(requestTemplate);
		this.fields = fields; 
	}

	public void decorate(JBlock context, HTTPAction action) {
		if(action.getUpdatedQueries() != null) {
			for(HTTPKeyQueryPair pair : action.getUpdatedQueries()) {
				JFieldVar fieldVar = fields.get(pair);
				context.add(requestor.addData(JExpr.lit(pair.getParameterName()), fieldVar));
			}
		}		
	}
}
