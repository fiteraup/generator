package generator.technology.http;

import com.sun.codemodel.JVar;

public abstract class RequestDecorator{
	protected JVar req;
	protected RequestTemplate requestor;
	public RequestDecorator(JVar req, RequestTemplate requestTemplate) {
		this.req =req;
		this.requestor = requestTemplate;
	}
}
