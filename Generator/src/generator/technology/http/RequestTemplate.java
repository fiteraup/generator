package generator.technology.http;

import generator.mappers.HTTPMapper;
import types.http.HTTPAction.Method;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JVar;

public class RequestTemplate {
	private final HTTPMapper httpMapper;
	private JInvocation requestInv;
	private JVar request;
	public RequestTemplate(HTTPMapper httpMapper) {
		this.httpMapper = httpMapper;
	}
	
	public JClass type() {
		return httpMapper.httpRequestClass;
	}
	
	public JInvocation addData(JExpression key, JExpression value) {
		return request.invoke("addData").arg(key).arg(value);
	}
	
	public JInvocation addData(String keyVal, String valVal) {
		return request.invoke("addData").arg(JExpr.lit(keyVal)).arg(JExpr.lit(valVal));
	}
	
	public JInvocation getContent() {
		return request.invoke("getContent");
	}
	
	/**
	 * Ugly, I know.
	 */
	public JInvocation updateUrl(JExpression req) {
		return req.invoke("setUrl").arg(req.invoke("getUrl").plus(JExpr.lit("&")).plus(getContent()));
	}
	
	public JInvocation addHeader(JExpression key, JExpression value) {
		return request.invoke("addHeader").arg(key).arg(value);
	}
	
	public JInvocation newReq(JVar req, Method methodType, String link) {
		JExpression methType = httpMapper.methodClass.staticRef(methodType.name());
		JExpression version = httpMapper.versionClass.staticRef("v11");
		requestInv = JExpr._new(httpMapper.httpRequestClass).arg(methType).arg(JExpr.lit(link)).arg(version);
		return requestInv;
	}
	
	public JInvocation newReq(JBlock context, String id, Method methodType, String link) {
		JExpression methType = httpMapper.methodClass.staticRef(methodType.name());
		JExpression version = httpMapper.versionClass.staticRef("v11");
		requestInv = JExpr._new(httpMapper.httpRequestClass).arg(methType).arg(JExpr.lit(link)).arg(version);
		request = context.decl(type(), id, requestInv);
		return requestInv;
	}
	
}
