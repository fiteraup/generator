package generator.technology.http;

import exceptions.Err;
import generator.mappers.HTTPMapper;
import generator.naming.IHTTPNaming;
import generator.naming.ITechnologyNaming;
import generator.technology.AbstractTechnologyTemplate;
import generator.technology.ICallback;
import generator.technology.ITechnologyTemplate;

import java.util.List;

import options.Options;
import parsers.IParser;
import parsers.web.StructuredYamlParser;
import parsers.web.UnstructuredYamlParser;
import types.http.HTTPAction;
import types.http.HTTPInterface;
import types.http.HTTPPage;

import com.sun.codemodel.JCase;
import com.sun.codemodel.JConditional;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldRef;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JSwitch;
import com.sun.codemodel.JVar;

public class HTTPTemplate extends AbstractTechnologyTemplate<HTTPInterface> implements IHTTPNaming, ITechnologyNaming, ITechnologyTemplate{

	public HTTPTemplate() {
		super();
	}
	
	private HTTPMapper httpMapper = new HTTPMapper(codeModel);
	private LoggerTemplate logger = new LoggerTemplate(httpMapper);
	private ResponseTemplate responder = new ResponseTemplate(httpMapper);
	private RequestTemplate requestor = new RequestTemplate(httpMapper);
	private Decorator decorator;
	private CookieTemplate cookieManager;
	private JMethod TCPSendMethod;
	private JMethod initConnectionMethod;
	
	private String serviceLink;
	private int systemPort;
	private String systemHost;
	
	
	@Override
	public void prepareTemplate(ICallback callback, Options options) {
		super.prepareTemplate(callback, options);
		this.serviceLink = options.httpOptions.systemPath;
		this.systemHost = options.httpOptions.systemHost;
		this.systemPort = Integer.valueOf(options.httpOptions.systemPort);
		this.decorator = new Decorator(intrf, callback, requestor, responder);
	}

	@Override
	public void constructFields(JDefinedClass context) {
		JFieldRef cookieManagerField = JExpr._this().ref(context.field(JMod.PRIVATE, httpMapper.cookieManagerClass, "cookieManager", JExpr._new(httpMapper.cookieManagerClass)));
		cookieManager = new CookieTemplate(cookieManagerField);
	}
	
	@Override
	public void constructConstructor(JMethod context) {
		initConnectionMethod = sut.method(JMod.PUBLIC, codeModel.VOID, "initConnection");
		constructInitConnection(initConnectionMethod);
		context.body().invoke(initConnectionMethod);
	}
	
	private void constructInitConnection(JMethod context) {
		context.body().add(logger.logInfo("Initializing connection to the system"));
		List<HTTPAction> initActions = intrf.getInitActions();
		JVar reqVar = context.body().decl(requestor.type(), "request", 
				JExpr._null());
		JVar resVar = context.body().decl(responder.type(), "response", 
				JExpr._null());
		for(HTTPAction action : initActions) {
			decorator.assignDecorator(reqVar, serviceLink);
			decorator.keyValueDecorator(reqVar);
			decorator.finalizeDecorator(reqVar);
			decorator.cookieDecorator(reqVar, cookieManager);
			decorator.authDecorator(reqVar);
			decorator.decorate(context.body(), action);
			JInvocation webInv = constructExecuteWebInv();
			context.body().assign(resVar, webInv.arg(reqVar));
			decorator.updateQueryDecorator(resVar);
			decorator.decorate(context.body(), action);
			context.body().add(cookieManager.updateCookies(resVar));
		}
		context.body().add(logger.logInfo("Ready to infer"));
	}
	
	private void constructAbstractToConcrete(JMethod context) {
		JVar pi = context.param(in.getType(), "pi");
		JVar req = context.body().decl(requestor.type(), "req", JExpr._null());
		context.body().add(logger.logInfo(JExpr.lit("Abstract :").plus(pi)));
		JSwitch jSwitch = context.body()._switch(in.getInputSymbol(pi));
		for(HTTPAction action : intrf.getActions()) {
			JCase jCase = jSwitch._case(JExpr.lit(wrapper.wrap(action)));
			decorator.assignDecorator(req, serviceLink);
			decorator.paramDecorator(req, pi);
			decorator.keyValueDecorator(req);
			decorator.keyQueryDecorator(req);
			decorator.referQueryDecorator(req);
			decorator.finalizeDecorator(req);
			decorator.authDecorator(req);
			decorator.cookieDecorator(req, cookieManager);
			decorator.decorate(jCase.body(), action);
			jCase.body()._break();
		}
		jSwitch._default().body().add(logger.logError(JExpr.lit("AbstractToConcrete method is missing for symbol:").plus(in.getInputSymbol(pi))));
		JConditional jIf = context.body()._if(req.ne(JExpr._null()));
//		decorator.authDecorator(req);
//		decorator.cookieDecorator(req, cookieManager);
		jIf._else().add(logger.logError(JExpr.lit("Abstract to concrete is undefined for ").plus(pi)));
		context.body()._return(req);
	}
	
	private JExpression contains(JExpression resp, String content) {
		return responder.contains(resp, content);
	}
	
	private void constructConcreteToAbstract(JMethod context) {
		JVar resp = context.param(responder.type(), "resp");
		JVar po = context.body().decl(out.getType(), "po", JExpr._null());
		JVar param = context.body().decl(out.getParamType(), "param", JExpr._null());
		context.body().add(cookieManager.updateCookies(resp));
		JConditional cond = context.body()._if(resp.eq(JExpr._null()).cor(responder.code(resp).ne(JExpr.lit(200))));
		cond._then().assign(po, out.createNew());
		JConditional elsifBlock = cond;
		for(HTTPPage page : intrf.getPages()) {
			elsifBlock = elsifBlock._elseif(contains(resp, page.discerningString));
			JExpression codeString = responder.codeString(resp);
			elsifBlock._then().assign(param, out.createParam( codeString, 0, types.Type.STRING));
			
			elsifBlock._then().assign(po, out.createNewList(wrapper.wrap(page), param));
		}
		elsifBlock._else().add(logger.logInfo("Concrete to abstract information is missing"));
		
		context.body().add(logger.logInfo(JExpr.lit("Abstract :").plus(po)));
		context.body()._return(po);
	}

	@Override
	public void constructReset(JMethod context) {
		List<HTTPAction> resetActions = intrf.getResetActions();
		JVar reqVar = context.body().decl(requestor.type(), "request", 
				JExpr._null());
		JVar resVar = context.body().decl(responder.type(), "response", 
				JExpr._null());
		for(HTTPAction action : resetActions) {
			decorator.assignDecorator(reqVar, serviceLink);
			decorator.keyValueDecorator(reqVar);
			decorator.referQueryDecorator(reqVar);
			decorator.finalizeDecorator(reqVar);
			decorator.cookieDecorator(reqVar, cookieManager);
			decorator.authDecorator(reqVar);
			decorator.decorate(context.body(), action);
			JInvocation webInv = constructExecuteWebInv();
			context.body().assign(resVar, webInv.arg(reqVar));
			context.body().add(cookieManager.updateCookies(resVar));
		}
	}

	@Override
	public void constructExecute(JMethod context) {
		JMethod absToConcrete = sut.method(JMod.PRIVATE, httpMapper.httpRequestClass, "defaultAbstractToConcrete");
		constructAbstractToConcrete(absToConcrete);
		JMethod concToAbstract = sut.method(JMod.PRIVATE, out.getType(), "defaultConcreteToAbstract");
		constructConcreteToAbstract(concToAbstract);
		JInvocation webEx = constructExecuteWebInv();
		constructExecute(context, absToConcrete, concToAbstract, webEx);
	}
	
	private JInvocation constructExecuteWebInv() {
		if(TCPSendMethod == null) {
			TCPSendMethod = sut.method(JMod.PRIVATE, httpMapper.httpResponseClass, "sendRequest");
			constructExecuteWeb(TCPSendMethod);
		}
		return JExpr.invoke(TCPSendMethod);
	}
	
	private void constructExecute(JMethod context, JMethod absToConc, JMethod concToAbs, JInvocation webInv) {
		JVar pi = context.param(in.getType(), "pi");
		JVar req = context.body().decl(requestor.type(), "req", JExpr.invoke(absToConc).arg(pi));
		JVar resp = context.body().decl(responder.type(), "response", webInv.arg(req));
		JVar po = context.body().decl(out.getType(), "po", JExpr.invoke(concToAbs).arg(resp));
		context.body()._return(po);
	}
	
	private void constructExecuteWeb(JMethod context) {
		JVar request = TCPSendMethod.param(httpMapper.httpRequestClass, "request");
		JInvocation tcpInvoke = httpMapper.tcpSendClass.staticInvoke("Send").arg(JExpr.lit(systemHost)).arg(JExpr.lit(systemPort)).arg(request);
		JVar link = TCPSendMethod.body().decl(globalMapper.stringClass, "link", tcpInvoke);
		TCPSendMethod.body()._return(JExpr._new(httpMapper.httpResponseClass).arg(link));
	}

	@Override
	protected IParser getParser(Options options) {
		IParser parser = null;
		switch(options.httpOptions.descriptionFile) {
		case Options.HTTP.STRUCTURAL:
			parser = new StructuredYamlParser();
		break;
		case Options.HTTP.UNSTRUCTURAL:
			parser = new UnstructuredYamlParser();
		break;
		default:
			Err.handleInternal("Option description is not recognized in "+this.getClass());
		break;
		}
		return parser;
	}
}
