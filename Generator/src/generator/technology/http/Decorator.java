package generator.technology.http;

import generator.Factory;
import generator.IGlobal;
import generator.technology.ICallback;
import generator.technology.http.action.AssignDecorator;
import generator.technology.http.action.AuthDecorator;
import generator.technology.http.action.KeyQueryDecorator;
import generator.technology.http.action.KeyValueDecorator;
import generator.technology.http.action.ParamDecorator;
import generator.technology.http.action.ReferQueryDecorator;
import generator.technology.http.action.UpdateQueryDecorator;
import generator.technology.http.intrf.LogDecorator;
import generator.technology.http.intrf.StatementDecorator;
import generator.tool.IInput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import types.http.HTTPAction;
import types.http.HTTPInterface;
import types.http.HTTPKeyQueryPair;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JStatement;
import com.sun.codemodel.JVar;

public class Decorator implements IGlobal{
	private IInput in;
	private RequestTemplate requestor;
	private ResponseTemplate responder;
	private List<IActionDecorator> activeActionDecorators;
	private List<IInterfaceDecorator> activeInterfaceDecorators;
	private Map<HTTPKeyQueryPair, JFieldVar> queryFields;
	private JDefinedClass sut;
	
	public Decorator(HTTPInterface intrf, ICallback cb, RequestTemplate requestor, ResponseTemplate responder) {
		this.in = cb.abstractInput();
		this.requestor = requestor;
		this.activeActionDecorators = new ArrayList<IActionDecorator>();
		this.activeInterfaceDecorators = new ArrayList<IInterfaceDecorator>();
		this.queryFields = new HashMap<HTTPKeyQueryPair, JFieldVar>();
		this.responder = responder;
		this.sut = cb.sut();
		constructQueryFields(sut, intrf);
	}
	
	private void constructQueryFields(JDefinedClass context, HTTPInterface intrf) {
		System.out.println(queryFields);
		for(HTTPAction action : intrf.getAllActions()) {
			for(HTTPKeyQueryPair queryPair : action.getUpdatedQueries()) {
				if(queryFields.containsKey(queryPair) == false) {
					System.out.println(queryPair.getParameterName());
					JFieldVar queryField = context.field(JMod.PRIVATE, globalMapper.stringClass, queryPair.getParameterName().toLowerCase());
					queryFields.put(queryPair, queryField);
				}
			}
		}
	}
	
	public Decorator paramDecorator(JVar req, JVar input) {
		activeActionDecorators.add(new ParamDecorator(requestor, input, in));
		return this;
	}
	
	public Decorator keyValueDecorator(JVar req) {
		activeActionDecorators.add(new KeyValueDecorator(requestor));
		return this;
	}
	
	public Decorator keyQueryDecorator(JVar req) {
		activeActionDecorators.add(new KeyQueryDecorator(requestor, queryFields));
		return this;
	}
	
	public Decorator assignDecorator(JVar req, String link) {
		activeActionDecorators.add(new AssignDecorator(req, requestor, link));
		return this;
	}
	
	public Decorator updateQueryDecorator(JVar res) {
		activeActionDecorators.add(new UpdateQueryDecorator(res, sut, responder, queryFields));
		return this;
	}
	
	public Decorator referQueryDecorator(JVar req) {
		activeActionDecorators.add(new ReferQueryDecorator(requestor));
		return this;
	}
	
	public Decorator finalizeDecorator(JVar req) {
		//activeActionDecorators.add(new FinalizeDecorator(req));
		return this;
	}
	
	public Decorator logDecorator(String message) {
		activeInterfaceDecorators.add(new LogDecorator(Factory.getLoggerTemplate(), message));
		return this;
	}
	
	public Decorator authDecorator(JVar req) {
		activeActionDecorators.add(new AuthDecorator(requestor));
		return this;
	}
	
	public void decorate(JBlock context, HTTPInterface httpInterface) {
		for(IInterfaceDecorator decorator : activeInterfaceDecorators) {
			decorator.decorate(context, httpInterface);
		}
		System.out.println(activeInterfaceDecorators);
		activeInterfaceDecorators.clear();
	}
	
	public Decorator cookieDecorator(JVar req, CookieTemplate cookieTemplate) {
		//activeActionDecorators.add(new CookieDecorator(req, requestor, cookieTemplate));
		return this;
	}
	
	public Decorator statementDecorator(JStatement statement) {
		activeInterfaceDecorators.add(new StatementDecorator(statement));
		return this;
	}
	
	public void decorate(JBlock context, HTTPAction action) {
		for(IActionDecorator decorator : activeActionDecorators) {
			decorator.decorate(context, action);
		}
		activeActionDecorators.clear();
	}
	
	public void decorate(JBlock context, List<HTTPAction> actions) {
		for(HTTPAction action : actions) {
			for(IActionDecorator decorator : activeActionDecorators) {
				decorator.decorate(context, action);
			}
		}
		activeActionDecorators.clear();
	}
	
//	private class CookieDecorator extends RequestDecorator {
//		
//		private CookieTemplate cookieManager;
//
//		public CookieDecorator(JVar req, CookieTemplate cookieTemplate) {
//			super(req);
//			this.cookieManager = cookieTemplate;
//		}
//
//		public void decorate(JBlock context, HTTPAction action) {
//			context.add(requestor.addHeader(req, JExpr.lit("Cookie"), cookieManager.getCookieLine()));
//		}
//	}
	
//	private class FinalizeDecorator extends RequestDecorator {
//		public FinalizeDecorator(JVar req) {
//			super(req);
//		}
//		public void decorate(JBlock context, HTTPAction action) {
//			if(action.methodType == HTTPAction.Method.GET && hasValuePairs(action)) {
//				context.add(requestor.updateUrl(req));
//			}
//		}
//		
//		public boolean hasValuePairs(HTTPAction action) {
//			return action.getKeyValuePairs().size() > 0 || action.getReferredQueries().size() > 0;
//		}
//		
//	}
}
