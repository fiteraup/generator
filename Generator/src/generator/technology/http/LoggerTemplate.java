package generator.technology.http;

import generator.mappers.HTTPMapper;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JStatement;

public class LoggerTemplate {
	private final JClass loggerClass;
	public LoggerTemplate(HTTPMapper httpMapper) {
		loggerClass = httpMapper.logManagerClass;
	}
	
	public JStatement logInfo(String information) {
		return loggerClass.staticInvoke("logInfo").arg(JExpr.lit(information));
	}
	
	public JStatement logError(String information) {
		return loggerClass.staticInvoke("logError").arg(JExpr.lit(information));
	}

	public JStatement logConcrete(String information) {
		return loggerClass.staticInvoke("logConcrete").arg(JExpr.lit(information));
	}
	
	public JStatement logInfo(JExpression information) {
		return loggerClass.staticInvoke("logInfo").arg(information);
	}
	
	public JStatement logError(JExpression information) {
		return loggerClass.staticInvoke("logError").arg(information);
	}
	
	public JStatement logConcrete(JExpression information) {
		return loggerClass.staticInvoke("logConcrete").arg(information);
	}
}
