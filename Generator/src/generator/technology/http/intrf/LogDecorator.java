package generator.technology.http.intrf;

import types.http.HTTPInterface;

import com.sun.codemodel.JBlock;

import generator.technology.http.IInterfaceDecorator;
import generator.technology.http.LoggerTemplate;

public class LogDecorator implements IInterfaceDecorator{
	private LoggerTemplate loginManager;
	private String message;
	public LogDecorator(LoggerTemplate loginManager, String message) {
		this.loginManager = loginManager;
		this.message = message;
	}

	public void decorate(JBlock context, HTTPInterface httpInterface) {
		context.add(loginManager.logInfo(message));
	}
}
