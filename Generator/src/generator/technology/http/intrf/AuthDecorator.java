package generator.technology.http.intrf;

import types.http.HTTPInterface;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JVar;

import generator.technology.http.IInterfaceDecorator;
import generator.technology.http.RequestTemplate;

public class AuthDecorator extends RequestInterfaceDecorator implements IInterfaceDecorator{

	public AuthDecorator(JVar req, RequestTemplate requestTemplate) {
		super(req, requestTemplate);
	}

	public void decorate(JBlock context, HTTPInterface httpInterface) {
		if( httpInterface.getAuth() != null) {
			String authString = httpInterface.getAuth().getAuthString();
			requestor.addHeader(JExpr.lit("Authorization") , JExpr.lit(authString));
		}
	}

}
