package generator.technology.http.intrf;

import generator.technology.http.CookieTemplate;
import generator.technology.http.RequestTemplate;
import types.http.HTTPInterface;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JVar;

public class CookieDecorator extends RequestInterfaceDecorator {

		private CookieTemplate cookieManager;

		public CookieDecorator(JVar req, RequestTemplate requestTemplate, CookieTemplate cookieTemplate) {
			super(req,requestTemplate);
			this.cookieManager = cookieTemplate;
		}
		
		@Override
		public void decorate(JBlock context, HTTPInterface htttpInterface) {
			  context.add(requestor.addHeader(JExpr.lit("Cookie"), cookieManager.getCookieLine()));
		}
}
