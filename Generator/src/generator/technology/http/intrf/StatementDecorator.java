package generator.technology.http.intrf;

import types.http.HTTPInterface;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JStatement;

import generator.technology.http.IInterfaceDecorator;

public class StatementDecorator implements IInterfaceDecorator{
	
	private JStatement statement;

	public StatementDecorator(JStatement statement) {
		this.statement = statement;
	}

	public void decorate(JBlock context, HTTPInterface htttpInterface) {
		context.add(statement);
	}

}
