package generator.technology.http.intrf;

import generator.technology.http.IInterfaceDecorator;
import generator.technology.http.RequestDecorator;
import generator.technology.http.RequestTemplate;

import com.sun.codemodel.JVar;

public abstract class RequestInterfaceDecorator extends RequestDecorator implements IInterfaceDecorator{
	public RequestInterfaceDecorator(JVar req, RequestTemplate requestTemplate) {
		super(req, requestTemplate);
	}
}
