package generator.technology.http;

import com.sun.codemodel.JVar;


public abstract class ResponseDecorator{
	protected JVar res;
	protected ResponseTemplate responder;
	public ResponseDecorator(JVar res, ResponseTemplate responder) {
			this.res =res;
			this.responder = responder;
	}
}