package generator.technology.http;

import types.http.HTTPAction;

import com.sun.codemodel.JBlock;

public interface IActionDecorator {
	public void decorate(JBlock context, HTTPAction action);
}