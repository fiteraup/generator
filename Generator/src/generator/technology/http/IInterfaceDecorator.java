package generator.technology.http;

import types.http.HTTPInterface;

import com.sun.codemodel.JBlock;

public interface IInterfaceDecorator {
	public void decorate(JBlock context, HTTPInterface htttpInterface);
}
