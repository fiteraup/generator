package generator.technology.http;

import generator.mappers.HTTPMapper;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;

public class ResponseTemplate {
	private final HTTPMapper httpMapper;
	public ResponseTemplate(HTTPMapper httpMapper) {
		this.httpMapper = httpMapper;
	}
	
	public JClass type() {
		return httpMapper.httpResponseClass;
	}
	
	public JInvocation contains(JExpression resp,String content) {
		return resp.invoke("getContent").invoke("contains").arg(JExpr.lit(content));
	}
	
	public JInvocation code(JExpression resp) {
		return resp.invoke("getCode");
	}
	
	public JInvocation codeString(JExpression resp) {
		return resp.invoke("getCodeString");
	}
	
	public JInvocation content(JExpression req) {
		return req.invoke("toString");
	}
}
