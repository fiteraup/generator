package generator.technology;

import generator.IGlobal;
import options.Options;
import types.InterfaceProvider;

import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;


public interface ITechnologyTemplate extends IGlobal{
	
	void prepareTemplate(ICallback callback, Options options);
	
	InterfaceProvider getInterfaceProvider();
	
	void constructFields(JDefinedClass context);
	
	void constructConstructor(JMethod context);
	
	void constructExecute(JMethod context);
	
	void constructReset(JMethod context);
}
