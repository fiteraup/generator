package generator.technology.rmi;

import generator.mappers.RMIMapper;
import generator.naming.IRMINaming;
import generator.technology.ICallback;
import generator.technology.plain.INTERFACETemplate;
import options.Options;
import parsers.ParsedAttributes;

import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JTryBlock;
import com.sun.codemodel.JType;


public class RMITemplate extends INTERFACETemplate implements IRMINaming{


	private static final RMIMapper rmiMapper = new RMIMapper(codeModel); 
	
	private JFieldVar serverField;
	private JType interfaceType;
	private String serviceLink;
	
	public RMITemplate() {
		super();
	}
	
	protected JFieldVar getObjectField() {
		return serverField;
	}
	
	public void prepareTemplate(ICallback callback, Options options) {
		super.prepareTemplate(callback, options);
		Class<?> interfaceClass = (Class<?>) intrf.parsedAttributes.get(ParsedAttributes.COMPILED_CLASS);
		this.interfaceType = codeModel._ref(interfaceClass);
		this.serviceLink = options.rmiOptions.getLink();
	}

	@Override
	public void constructFields(JDefinedClass context) {
		serverField = context.field(JMod.PRIVATE, interfaceType, SERVER_FIELD);
	}

	@Override
	public void constructConstructor(JMethod ctor) {
		ctor.body().staticInvoke(globalMapper.systemClass, SYSTEM_SETSM_METHOD).arg(JExpr._null());
		JTryBlock tryBlock = ctor.body()._try();
		tryBlock.body().assign(serverField, JExpr.cast(interfaceType, rmiMapper.rmiNamingClass.staticInvoke(NAMING_LOOKUP_METHOD).arg(serviceLink)));
		tryBlock.body().invoke(serverField, SERVER_RESET_METHOD);
		catchException(tryBlock);
	}

	@Override
	public void constructReset(JMethod context) {
		JTryBlock tryBlock = context.body()._try();
		tryBlock.body().invoke(serverField, resetMethod);
		catchException(tryBlock);
	}
}
