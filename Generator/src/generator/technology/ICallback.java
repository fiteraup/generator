package generator.technology;

import generator.tool.IInput;
import generator.tool.IOutput;
import generator.tool.IWrap;

import com.sun.codemodel.JDefinedClass;

public interface ICallback {
	//public JExpression getParameter (Message message, JVar params, int index);
	public IInput abstractInput();
	public IOutput abstractOutput();
	public JDefinedClass sut();
	public IWrap wrapper();
}
