package generator.technology.plain;

import generator.naming.IPLAINNaming;
import generator.technology.ICallback;
import options.Options;
import parsers.ParsedAttributes;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;

public class PLAINTemplate extends INTERFACETemplate implements IPLAINNaming{

	private JFieldVar instanceField;
	private JClass instanceType;
	private String resetMethod;
	
	public PLAINTemplate() {
		super();
	}

	protected JFieldVar getObjectField() {
		return instanceField;
	}
	
	public void prepareTemplate(ICallback callback, Options options) {
		super.prepareTemplate(callback, options);
		Class<?> interfaceClass = (Class<?>) intrf.parsedAttributes.get(ParsedAttributes.COMPILED_CLASS);
		this.instanceType = codeModel.ref(interfaceClass);
		this.resetMethod = options.plainOptions.resetName;
	}

	@Override
	public void constructFields(JDefinedClass context) {
		instanceField = context.field(JMod.PRIVATE, instanceType, SERVER_FIELD);
	}

	@Override
	public void constructConstructor(JMethod ctor) {
		ctor.body().assign(instanceField, JExpr._new(instanceType));
	}

	@Override
	public void constructReset(JMethod context) {
		context.body().invoke(instanceField, resetMethod);
	}

}
