package generator.technology.plain;

import exceptions.Err;
import generator.TypeMapper;
import generator.technology.AbstractTechnologyTemplate;
import generator.technology.ICallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import options.Options;
import parsers.IParser;
import parsers.intrf.InterfaceParser;
import types.Message;
import types.Parameter;
import types.Type;
import types.intrf.CompositeParameter;
import types.intrf.PLAINInput;
import types.intrf.PLAINInterface;
import types.intrf.PLAINMessage;
import types.intrf.PLAINOutput;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JCase;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JSwitch;
import com.sun.codemodel.JTryBlock;
import com.sun.codemodel.JVar;

public abstract class INTERFACETemplate extends AbstractTechnologyTemplate<PLAINInterface> {

	protected String resetMethod;
	
	public INTERFACETemplate() {
		super();
	}
	
	protected abstract JFieldVar getObjectField();
	class IndexHolder {
		int index = 0;
	}
	private void setParameters(JBlock context, JVar paramList, JExpression output, Map<Parameter,JVar> paramMap, Parameter param, IndexHolder holder) {
		if(param.type == Type.COMPOSITE) {
			CompositeParameter compParam = (CompositeParameter) param;
			for(Parameter subParam : compParam.parameters) {
				JInvocation invocation = JExpr.invoke(JExpr.invoke(output, "getClass"),"getFields");
				JExpression field = invocation.component(JExpr.lit(subParam.index));
				JExpression result = field.invoke("get").arg(output);
				context.add(field.invoke("setAccessible").arg(JExpr.lit(true)));
				setParameters(context, paramList, result, paramMap, subParam, holder);
			}
		} 
		else {
			JClass paramType = TypeMapper.getType(param);
			JExpression result = JExpr.cast(paramType, output);
			context.assign(paramMap.get(param), out.createParam(result, holder.index, param.type));
			context.add(paramList.invoke("add").arg(paramMap.get(param)));
			holder.index ++;
		}
	}
	
	
	private JInvocation constructInvocation(JInvocation invocation, JVar params, Parameter [] inputParameters) {
		for(Parameter param : inputParameters) {
			if(param.type == Type.COMPOSITE) {
				CompositeParameter compParam = (CompositeParameter) param;
				JClass compositeType = globalMapper.getJClass(codeModel, compParam.compositeType);
				JInvocation newInvocation = JExpr._new(compositeType);
				newInvocation = constructInvocation(newInvocation, params, compParam.parameters);
				invocation = invocation.arg(newInvocation);
			}
			else {
				invocation = invocation.arg(in.getParameterValue(params, param));
			}
		}
		return invocation;
	}
	
	public JInvocation constructResponse(PLAINInput message, JVar params) {
		JInvocation serverCall = getObjectField().invoke(message.getName());
		List<Parameter> msgParameters = message.getInputParameters();
		Parameter [] parameters = msgParameters.toArray(new Parameter[msgParameters.size()] );
		serverCall = constructInvocation(serverCall, params, parameters);
		return serverCall;
	}
	
	public void prepareTemplate(ICallback callback, Options options) {
		super.prepareTemplate(callback, options);
		this.intrf = (PLAINInterface) parse(options);
	}
	
	private Map<Parameter,JVar> declareOutputParams(JBlock context) {
		Map<Parameter,JVar> simpleParams = new HashMap<Parameter,JVar>();
		for(PLAINMessage output : intrf.getPLAINOutputs()) {
			for(Parameter param : output.getPrimitiveParameters()) {
				JVar po = context.decl(out.getParamType(), param.name, JExpr._null());
				simpleParams.put(param, po);
			}
		}
		return simpleParams;
	}
	
	public List<Parameter> getParameters(Message message) {
		List<Parameter> parameters = null;
		if(message instanceof PLAINMessage) {
			parameters = ((PLAINMessage) message).getPrimitiveParameters();
		} else {
			Err.invalidMessageParsed(message, "plain interface");
		}
		
		return parameters;
	}
	
	
	
	@Override
	public void constructExecute(JMethod executeMethod) {
		JVar pi = executeMethod.param(in.getType(), "pi");
		JVar output = executeMethod.body().decl(globalMapper.objectClass, "output", JExpr._null());
		//JVar result = executeMethod.body().decl(out.getParamType(), "result", JExpr._null());
		JVar po = executeMethod.body().decl(out.getType(), "po", JExpr._null() );
		JClass paramListType = globalMapper.arrayListClass.narrow(out.getParamType());
		JVar params = executeMethod.body().decl(paramListType, "params", JExpr._new(paramListType));
		Map<Parameter,JVar> paramMap = declareOutputParams(executeMethod.body());
		JTryBlock tryBlock = executeMethod.body()._try();
		JInvocation piInvoc =  in.getInputSymbol(pi);
		JSwitch msgSwitch = tryBlock.body()._switch(piInvoc);
		for(PLAINInput inputMessage : intrf.getPLAINInputs()) {
			JCase msgCase = msgSwitch._case(JExpr.lit(wrap(inputMessage)));
			//msgCase.body().assign(po, out.createNew(inputMessage.wrap()));
			JInvocation invocation = constructResponse( inputMessage, pi);
			PLAINOutput outputMessage = intrf.getPLAINOutput(inputMessage); 
			Parameter outputParam = outputMessage.getOutputParameter();
			if(outputParam.type != Type.VOID) {
				msgCase.body().assign(output,invocation);
				setParameters(msgCase.body(), params, output, paramMap, outputParam, new IndexHolder());
			}
			msgCase.body().assign(po, out.createNewList(wrap(outputMessage), params));
			msgCase.body()._break();
		}
		catchException(tryBlock);
		executeMethod.body()._return(po);
	}
	
	protected IParser getParser(Options options) {
		return new InterfaceParser();
	}
}
