package generator.naming;

public interface ITOMTENaming extends IToolNaming {
	/**
	 * The names of the methods defined in the SutInterface. These should be implemented by all Suts.
	 */
	public static final String PARSE_METHOD = "processSymbol";
	public static final String RESET_METHOD = "reset";
	
	/**
	 * Helper methods for extracting int/string
	 */
	public static final String GETINT_METHOD = "getIntParam";
	public static final String GETSTRING_METHOD = "getString";
	public static final String GETBOOL_METHOD = "getBooleanParam";
	
	/**
	 * Methods called on the method wrapper. Their names should match names of the InputAction.
	 */
	public static final String GETMETHODNAME_METHOD = "getMethodName";
	public static final String GETPARAM_METHOD = "getParameters";
	
	/**
	 * Concrete name variables used when generating the parse_method method.
	 */
	public static final String PARSE_INPUT_PARAM = "methodWrapper";
	public static final String PARSE_NAME_FIELD = "msgName";
	public static final String PARSE_PARAMS_FIELD = "params";
	public static final String PARSE_RESPONSE_FIELD = "response";
	public static final String PARSE_RESULT_FIELD = "result";
}
