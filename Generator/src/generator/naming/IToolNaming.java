package generator.naming;

import generator.IGlobal;

/**
 * A few default global tool naming conventions.
 */
public interface IToolNaming extends IGlobal{
	/**
	 * Where the generated files will be placed
	 */
	public static final String DESTINATION_PATH = "./generated";
	
	/**
	 * A place where files are stored for tools that are used by the generated SUTs.
	 * These files will be copied in the DESTINATION folder after the code generation
	 * has ended.
	 */
	public static final String FILES_PATH = "./files"; 
}
