package generator.naming;

public interface IPLAINNaming {
	/**
	 * Naming for variables specific to plain interfacing.
	 */
	public static final String SERVER_FIELD = "system";
}
