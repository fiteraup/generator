package generator.naming;

public interface IRMINaming extends IPLAINNaming {
	/**
	 * Naming for variables specific to RMI interfacing.
	 */
	public static final String SERVER_FIELD = "server";
	public static final String SERVER_RESET_METHOD = "reset";
	public static final String SYSTEM_SETSM_METHOD = "setSecurityManager";
	public static final String SYSTEM_PRINTLN_METHOD = "out.println";
	public static final String NAMING_LOOKUP_METHOD = "lookup";
	
	public static final String PARSE_INPUT_PARAM = "methodWrapper";
	public static final String PARSE_NAME_FIELD = "msgName";
	public static final String PARSE_PARAMS_FIELD = "params";
	public static final String PARSE_RESPONSE_FIELD = "response";
	public static final String PARSE_RESULT_FIELD = "result";
	public static final String GETPARAM_METHOD = "getParameters";
}
