package generator.naming;


public interface ISIMPANaming extends IToolNaming {
	
	/**
	 * The names of the methods that a driver should implement. These have to be in accordance with the RealDriver
	 * interface.
	 */
	public static final String GETPARAMNAMES_METHOD = "getParameterNames";
	public static final String EXECUTE_METHOD = "execute";
	public static final String EXECUTEWEB_METHOD = "executeWeb";
	public static final String INITCONNECTION_METHOD = "initConnection";
	public static final String GETDEFPARAMVALUES_METHOD = "getDefaultParamValues";
	public static final String GETOUTPUTSYMBOLS_METHOD = "getOutputSymbols";
	public static final String GETINPUTSYMBOLS_METHOD = "getInputSymbols";
	public static final String ABSTOCONC_METHOD = "abstractToConcrete";
	public static final String CONCTOABS_METHOD = "concreteToAbstract";
	public static final String RESET_METHOD = "reset";
	public static final String GETSYSNAME_METHOD = "getSystemName";
	
	/**
	 * The names of significant local variables used throughout the methods.
	 */
	public static final String OUTPUTSYMBOLS_FIELD = "methOutputs";
	public static final String INPUTSYMBOLS_FIELD = "methInputs";
	public static final String DEFPARAMVALUES_FIELD = "defaultParamValues";
	public static final String DEFPARAMNAMES_FIELD = "defaultParamNames";
}
