package generator;

import generator.mappers.GlobalMapper;

import com.sun.codemodel.JCodeModel;

/**
 * The IGlobal interface is implemented by all tool and technology templates. Thus, all of the associated classes
 * have access to the global codeModel field as well as the globalMapper, needed for code generation.
 */
public interface IGlobal {
	public static final JCodeModel codeModel = new JCodeModel();
	public static final GlobalMapper globalMapper = new GlobalMapper(codeModel);	

	public static enum Method {
		ABSTRACTTOCONCRETE,
		CONCRETETOABSTRACT,
		INITCONNECTION,
		EXECUTEWEB,
		EXECUTE
	}
	
	public static enum Field {
		COOKIEMANAGER,
		BASICAUTH,
		SYSTEMHOST,
		SYSTEMPORT
	}
}
