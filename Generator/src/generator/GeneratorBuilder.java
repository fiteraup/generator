package generator;

import generator.technology.ITechnologyTemplate;
import generator.technology.http.HTTPTemplate;
import generator.technology.plain.PLAINTemplate;
import generator.technology.rmi.RMITemplate;
import generator.tool.IGenerator;
import generator.tool.simpa.SIMPATemplate;
import generator.tool.tomte.TOMTETemplate;
import options.Options;
import options.Technology;
import options.Tool;

/**
 * Main builder class which instantiates templates corresponding to the technology and tool selected, coalesces them into a code generator
 * and then executes the assembly.
 * 
 * It makes a pretty nice use of the builder pattern. :)
 */
public class GeneratorBuilder {
	protected ITechnologyTemplate technologyTemplate = null;
	protected IGenerator toolTemplate = null;
	
	public GeneratorBuilder technology(Technology technology) {
		switch(technology) {
			case HTTP:
				technologyTemplate = new HTTPTemplate();
				break;
			case RMI:
				technologyTemplate = new RMITemplate();
				break;
			case PLAIN:
				technologyTemplate = new PLAINTemplate();
				break;
			default:
				technologyTemplate = null;
				break;
		}
		return this;
	}

	public GeneratorBuilder learner(Tool tool) {
		switch(tool) {
			case TOMTE:
				toolTemplate = new TOMTETemplate();
				break;
			case SIMPA:
				toolTemplate = new SIMPATemplate();
				break;
			default:
				break;
		}
		return this;
	}


	public GeneratorBuilder() {
		super();
	}
	
	
	
	public boolean canStart() {
		return toolTemplate != null && technologyTemplate != null;
	}
	
	public void constructSut(Options options) throws Exception {
		if(canStart() == false) {
			Log.err("technology or learner not set");
		} 
		else {
			Log.log("Started constructing SUT");
			long start = System.currentTimeMillis();
			toolTemplate.constructSUT(options, technologyTemplate);
			long stop = System.currentTimeMillis();
			Log.log("Finished constructing SUT in "+ (stop-start) + " miliseconds");
		}
	}
}
