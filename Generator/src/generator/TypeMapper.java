package generator;

import types.Parameter;
import types.intrf.CompositeParameter;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;

import exceptions.Err;

public class TypeMapper implements IGlobal{
	public static JClass getType(Parameter param) {
		switch(param.type) {
		case COMPOSITE:
			return globalMapper.getJClass(codeModel, ((CompositeParameter) param).compositeType);
		case INT:
			return globalMapper.integerClass;
		case STRING:
			return globalMapper.stringClass;
		default:
			return null;
		}
	}
	
	public static JExpression getValueFromString(String stringValue, Parameter param) {
		switch(param.type) {
		case COMPOSITE:
			Err.handleNotSupported(" mapping to composite values ");
			return null;
		case INT:
			return JExpr.lit(Integer.valueOf(stringValue));
		case STRING:
			return JExpr.lit(stringValue);
		default:
			return null;
		}
	}
	
}
