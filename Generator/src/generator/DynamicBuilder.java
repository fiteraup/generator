package generator;

import options.Technology;
import options.Tool;
import exceptions.Err;
import generator.technology.ITechnologyTemplate;
import generator.tool.IToolTemplate;

/**
 * We use inheritance, so that we don't extract an abstract class since we have plenty of those already.
 * Unlike the normal GeneratorBuilder, this builder will not have to be changed, provided the naming/placement conventions are kept.
 * These conventions are: 
 * 	for each enumeration class $aspect:
 * 		  the generator template for $aspect is located in generator.$aspect.$elementOfAspect
 * 		  where: generator.$aspect is a package containing all the classes for generating code for the described aspect
 * 			     $elementOfAspect is an element of the $aspect enumeration
 * 
 * Currently, the aspects are technology and (learner) tool.  
 */
public class DynamicBuilder extends GeneratorBuilder {
	public String createPath(Enum<?> enumVal) {
		String enumClassPath = "generator."+enumVal.getClass().getSimpleName().toLowerCase()+"."+enumVal.name().toLowerCase();
		enumClassPath += "."+enumVal.name().toUpperCase() + "Template";
		return enumClassPath;
	}
	
	public <T> T instantiateTemplate(Enum<?> enumVal, Class<T> interfaceKlass) {
		T ret = null;
		try {
		String techClassPath = createPath(enumVal);
		Object objVal = Class.forName(techClassPath).newInstance();
		ret = interfaceKlass.cast(objVal);
		} catch(Exception ex) {
			Err.handleImplementationError(enumVal, ex);
		}
		return ret;
	}
	
	public GeneratorBuilder technology(Technology technology) {
		this.technologyTemplate = instantiateTemplate(technology, ITechnologyTemplate.class);
		return this;
	}
	
	public GeneratorBuilder learner(Tool tool) {
		this.toolTemplate = instantiateTemplate(tool, IToolTemplate.class);
		return this;
	}
	
}
