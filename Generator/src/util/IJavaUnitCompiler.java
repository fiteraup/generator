package util;

public interface IJavaUnitCompiler {
	public Class<?> compileJavaUnit(String javaUnitPath, String classPath, String buildPath) throws Exception;
}
