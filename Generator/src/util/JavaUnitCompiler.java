package util;

import exceptions.JavaFileDoesNotExistException;
import exceptions.PathDoesntPointToJavaFileException;
import generator.Log;

import java.io.File;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Scanner;

import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

/**
 * A compiler for a single Java Unit file. It compiles the target unit file (and consequently, the java files it depends on). 
 * A bit limited as of now (classPath is a single string) but it is working. 
 * It is important to note that java classes not referred by the to-be-compiled class will not be compiled.  
 * Also of note is that compiled classes cannot be found by the default class loader. To load these classes, you need an
 * URLClassLoader with the build path forming the URL and the parent loader set to null. 
 */
public class JavaUnitCompiler implements IJavaUnitCompiler{
	private String packageString = null;
	
	public Class<?> compileJavaUnit(String javaUnitPath, String classPath, String buildPath) throws Exception {
		Log.log("Compiling " + javaUnitPath + " classpath " + classPath + " and build directory " + buildPath);
		File sourceFile = getJavaFile(javaUnitPath, classPath);
		File buildDir = getDirectory(buildPath);
		System.out.println(classPath);
		File classPathDir = getDirectory(classPath);
		compileJavaFile(sourceFile, classPathDir, buildDir);
		Class<?> klass = fetchCompiledClass(sourceFile, buildDir);
		return klass;
	}
	
	private File getJavaFile(String javaUnitName, String classPath) throws Exception {
		if(javaUnitName == null || javaUnitName.indexOf(".java") == -1) {
			throw new PathDoesntPointToJavaFileException(javaUnitName);
		}
		File sourceFile = new File(classPath, javaUnitName);
		if (sourceFile.exists() == false) {
			
			sourceFile = new File(classPath, packageString);
			if(sourceFile.exists() == false) {
				throw new JavaFileDoesNotExistException(javaUnitName, classPath);
			}
		}
		return sourceFile;
	}
//	
//	private String javaNameToPath(String javaUnitName) {
//		String packageString = javaUnitName.substring(0, javaUnitName.indexOf(".java"));
//		String javaPath = packageString.replace(".", File.separator);
//		return javaPath + ".java";
//	} 
	
	private String getPackageString(File sourceFile) throws Exception {
		Scanner scanner = new Scanner(sourceFile);
		String matchedLine = scanner.findWithinHorizon("(?<=package)[^;]*", 0);
		scanner.close();
		return matchedLine == null ? null : matchedLine.trim();
	}
	
	private String getClassNameFromPath(String javaPath) {
		int start = javaPath.lastIndexOf(File.separator);
		int finish = javaPath.indexOf(".java");
		start = start == -1 ? 0 : start + 1;
		String className = javaPath.substring(start, finish);
		return className;
	}
	
	private File getDirectory(String dirPath) throws Exception{
		File root = new File(dirPath);
		
		if(root.exists() == false) {
			root.mkdir();
		} else if(root.isDirectory() == false) {
			Log.err(root.getPath() + " already exists and is not a file");
			System.exit(0);
		}
		return root;
	}
	
	private void compileJavaFile(File javaFile, File classPath, File buildDir) {
		Log.log("Compiling " + javaFile + " classpath " + classPath.getPath() + " and build directory " + buildDir.getPath());
		
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(
				null, null, null);
		System.setProperty("java.class.path",System.getProperty("java.class.path") + ";.");
		Iterable<? extends JavaFileObject> compilationUnits = fileManager
				.getJavaFileObjectsFromFiles(Arrays.asList(javaFile));
		StringWriter compOutput = new StringWriter();
		CompilationTask compTask = compiler.getTask(compOutput, fileManager, null, Arrays.asList("-d", buildDir.getPath(), "-cp",classPath.getPath()), null, compilationUnits);
		if(!compTask.call()) {
			Log.err("Compilation unsuccessful "+compOutput.toString());
			System.exit(0);
		}
	}
	
	private Class<?> fetchCompiledClass(File compiledJavaFile, File buildDir) throws Exception{
		File root = buildDir;
		URLClassLoader classLoader = URLClassLoader
				.newInstance(new URL[] { root.toURI().toURL() });
		String packageString = getPackageString(compiledJavaFile);
		String className = getClassNameFromPath(compiledJavaFile.getPath());
		String fullClassName = packageString == null ? className : packageString +"."+ className;
		Class<?> klass = Class.forName(fullClassName, true, classLoader);
		return klass;
	}
}
