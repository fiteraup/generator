package options;

public enum Technology {
	PLAIN,
	RMI,
	WSDL,
	HTTP,
}
