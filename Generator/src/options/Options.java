package options;

import generator.ILoc;
import options.Settings.Contingency;

/**
 * Options configuration class. Each aspect $aspect has a field selected$Aspect. 
 * <p>
 * Each element $element of aspect $aspect should have an associated upper($element)
 * class which should inherit the GLOBAL class, and a $elementOptions of that respective type. The inner class should define
 * configuration parameters for $element.
 * <p>
 * Each configuration parameter has attributes, as defined in {@link options.Settings}.  
 */
public class Options {
	public Tool selectedLearner = null;
	public Technology selectedTechnology = null;
	public GLOBAL globalOptions = null;
	public PLAIN plainOptions = null;
	public RMI rmiOptions = null;
	public SIMPA simpaOptions = null;
	public HTTP httpOptions = null;
	public WSDL wsdlOptions = null;
	public TOMTE tomteOptions = null;	
	public static final String YES = "yes";
	public static final String NO = "no";
	
	public static class GLOBAL{
		@Settings(choice = {YES, NO}, defaultString = NO)
		public String generateSupport;
		// running support is not generally provided, except for Tomte + Java Plain combination
		@Settings(defaultString = "generated")
		public String buildPath;
		@Settings(choice = {YES, NO}, defaultString = YES)
		public String runSetup;
		@Settings(defaultString = "lib")
		public String libPath;
		@Settings(defaultString = "Main.java")
		public String generatedClassName;
	}
	
	public static class PLAIN extends GLOBAL{
		public String className = "package1.package2.ClassName.java";
		@Settings(defaultString = ILoc.DEFAULT_CLASSPATH)
		public String classPath = "path/where/class/is/located";
		@Settings(contingency = Contingency.OPTIONAL)
		public String resetName = "reset";
	}
	
	public static class RemoteService extends GLOBAL{
		public String systemPort = "8090";
		public String systemHost = "systemHost";
		public String systemPath = "systemPath";
		public String getLink() {
			return "//"+systemHost+":"+systemPort+"/"+systemPath;
		}
	}
	
	/***
	 * Technologies supported (also see main.Technology.java)
	 */
	
	public static class RMI extends RemoteService{
		public String interfaceName = "package1.package2.IntarfaceName.java";
		public String resetName = "reset";
		@Settings(defaultString = ILoc.DEFAULT_CLASSPATH)
		public String classPath = "path/where/class/is/located";
	}
	
	public static class WSDL extends RemoteService{
		
	}
	
	public static class HTTP extends RemoteService{
		public static final String STRUCTURAL = "STRUCTURAL";
		public static final String UNSTRUCTURAL = "UNSTRUCTURAL";
		//@Settings(contingency = Contingency.OPTIONAL)
		public String descriptionFile = "web.yaml";
		@Settings(choice = {STRUCTURAL,UNSTRUCTURAL}, defaultString = STRUCTURAL)
		public String descriptionType = "";
	}
	
	/***
	 * Tools supported (also see main.Tool.java)
	 */
	public static class TOMTE{
		@Settings(contingency = Contingency.OPTIONAL)
		public String configFile = "config.yaml";
	}
	
	public static class SIMPA{
		
	}
}
