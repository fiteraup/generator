package options;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * Provides annotations for each configuration parameter. Given the annotations, corresponding checks and parsings are done. 
 * <p>
 * Any new annotation requires changes in {@link options.OptionsReader} in order to interpret the new option. 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Settings {
	public enum Contingency {OPTIONAL, MANDATORY};
	Contingency contingency() default Contingency.OPTIONAL;
	String defaultString() default "";
	String [] choice() default {};
}
