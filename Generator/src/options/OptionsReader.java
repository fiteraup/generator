package options;

import generator.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * The Options Reader class is meant to read options from a property file, options that drive the generation process.
 * Current form of the property file is the standard Java one, but other technologies can be incorporated as well. 
 * <p>
 * To configure the options for each tool, see {@link options.Options}.
 */
public class OptionsReader {
	
	/** Only used for logging */
	private static Object selectedOptions = null;
	
	private static void handleInternal(Exception internalException) {
		Log.err("Internal error");
		internalException.printStackTrace();
		System.exit(0);
	}
	
	private static void handleNoProperties(FileNotFoundException fileNotFoundException, String propertiesFile) {
		Log.err("The required property file: "+ propertiesFile + " could not be found ");
		fileNotFoundException.printStackTrace();
		System.exit(0);
	} 
	
	private static void handleNoOptionFieldDefined(NoSuchFieldException noSuchFieldException, String expectedField) {
		Log.err("The required options field with name " + expectedField+ " in Options is not defined");
		noSuchFieldException.printStackTrace();
		System.exit(0);
	}
	
	public static void handleClassNotFound(ClassNotFoundException classNotFoundException, String expectedOptionType) {
		Log.err("The options type with name " + expectedOptionType+ " is not defined in Options. Define it");
		classNotFoundException.printStackTrace();
		System.exit(0);
	}
	
	private static void handleOptionNotInList(String option, String [] optionList, Field field) {
		Log.err("Option "+option+" cannot be found in "+Arrays.asList(optionList) + " for property "+field.getName());
		System.exit(0);
	}
	
	private static void handleMalformedOptionClass(Exception e, String option) {
		Log.err("Option class "+option+" is malformed and cannot be dynamically instantiated");
		e.printStackTrace();
		System.exit(0);
	}
	
	public static Options instantiateOptions(Enum<?> type) {
		try {
			@SuppressWarnings("unchecked")
			Class<? extends Options> optionsClass =  ((Class<? extends Options>) Class.forName(Options.class.getCanonicalName()+"#"+type.name().toUpperCase()));
			return optionsClass.newInstance();
		} catch (ClassNotFoundException e) {
			handleClassNotFound(e, type.name());
		} catch (Exception e) {
			handleMalformedOptionClass(e, type.name());
		}
		return null;
	}
	
	/***
	 * Fetches the list of strings that form an enumeration for a given class.
	 * @param c - The given class, should be an enumeration.
	 * @return - THe list of strings.
	 */
	public static List<String> getEnumStrings(Class<?> c) {
		ArrayList<String> enumStrings = new ArrayList<String>();
		for(Field field : c.getFields()) {
			if(field.isEnumConstant()) {
				enumStrings.add(field.getName());
			}
		}
		return enumStrings;
	}
	
	/***
	 * Checks to see whether an enumeration contains the given property. If not, displays an error and exits.
	 * @param c - The enumeration class.
	 * @param propertyValue - The property searched for.
	 */
	public static void checkEnumProperty(Class<?> c, String propertyValue) {
		List<String> enumStrings = getEnumStrings(c);
		if(!enumStrings.contains(propertyValue)) {
			System.err.println("Option " + propertyValue +" for "+c.getSimpleName().toLowerCase()+" not supported");
			System.exit(0);
		} 
	}
	
	/**
	 * Gets the property string associated with the field. The used convention is that all properties as 
	 * found in the properties file are lower case.
	 * @param field
	 * @return
	 */
	public static String fieldString(Field field) {
		return field.getName().toLowerCase();
	}
	
	/***
	 * Fetches the fields that make up the required properties. Static fields are
	 * excluded, as they are used to store constants.
	 * @param options Expected to be an Options object
	 * @return fields The fields that are associated with required properties.
	 */
	private static Field [] getOptionFields(Object options) {
		List<Field> fields = new ArrayList<Field>();
		for(Field field : options.getClass().getFields()) {
			if(!Modifier.isStatic(field.getModifiers())) {
				fields.add(field);
			}
		}
		return fields.toArray(new Field [fields.size()]);
	}
	
	/***
	 * Fills the options instance with the given properties. The properties must be in accordance with the options.Options
	 * structure. In this regard, it checks this compliance, signals and terminates reading in the case of
	 * missing information/ information mismatch.
	 * @param options Expected to be an Options object
	 * @param props Property object to be checked.
	 */
	public static void fillOptions(Object options, Properties props) {
		try {
			selectedOptions = options;
			for(Field field : getOptionFields(options)) {
				if ( field.isAnnotationPresent(Settings.class)) {
					Settings settings = field.getAnnotation(Settings.class);
					String fieldValue = null;
					switch(settings.contingency()) {
					case OPTIONAL:
						fieldValue = weakFetch(props, fieldString(field));
						break;
					case MANDATORY:
						fieldValue = strongFetch(props, fieldString(field));
						break;
					}
					if(fieldValue == null) {
						fieldValue = settings.defaultString();
					}
					if(settings.choice().length > 0) {
						boolean isValidChoice = false;
						for(String choice : settings.choice()) {
							if(choice.compareToIgnoreCase(fieldValue) == 0) {
								isValidChoice = true;
								break;
							}
						}
						if(isValidChoice == false) {
							handleOptionNotInList(fieldValue, settings.choice(), field);
						}
					}
					if(fieldValue == null && settings.defaultString().length() > 0) {
						fieldValue = settings.defaultString();
					} 
					if(fieldValue != null) {
						fieldValue = fieldValue.trim();
					}
					field.set(options, fieldValue);
				}
				else {
					field.set(options, strongFetch(props, fieldString(field)).trim());
				}
			}
		}catch(Exception reflexionException) {
			handleInternal(reflexionException);
		}
	}
	
	/**
	 * Fetches a property, terminating in case it doesn't find it.
	 */
	private static String strongFetch(Properties props, String propertyName) {
		String result = weakFetch(props, propertyName);
		if ( result == null) {
			Log.err("Required property \""+propertyName +"\" for options class "+selectedOptions.getClass().getSimpleName());
			System.exit(0);
		}
		return result;
	}
	
	/**
	 * Fetches a property, does not terminate the application if it is unable to find it.
	 */
	private static String weakFetch(Properties props, String propertyName) {
		String result = props.getProperty(propertyName.toLowerCase());
		return result;
	}

	/***
	 * Loads an Options object on the basis of its field structure with properties. The fields to be updated,
	 * are selected by the enumerable object passed in enumerable.
	 */
	private static void loadDelegate(Options options, String optionName, Properties props) throws Exception{
		String expectedOptionsField = optionName.toLowerCase()+"Options";
		try {
			Field referredField = Options.class.getDeclaredField(expectedOptionsField);
			Class<?> optionType  = referredField.getType();
			Object optionInstance = optionType.newInstance();
			referredField.set(options, optionInstance);
			fillOptions(optionInstance, props);
		}
		catch(NoSuchFieldException noSuchFieldException) {
			handleNoOptionFieldDefined(noSuchFieldException, expectedOptionsField);
		}
	}
	
	/**
	 * Loads properties from a file located at the given propertiesPath.
	 */
	private static Properties loadProperties(String propertiesFilePath) {
		Properties props = new Properties();
		try {
			props.load(new BufferedReader(new FileReader(propertiesFilePath)));
		} catch (IOException e) {
			if (e instanceof FileNotFoundException) {
				handleNoProperties((FileNotFoundException) e, propertiesFilePath);
			}
			e.printStackTrace();
		}
		return props;
	}
	
	/**
	 * Loads the options from the property file located at PROPERTIES_FILE.
	 */
	public static Options loadOptions(String propertiesFilePath) {
		Options options = null;
		
		try{
			Properties props = loadProperties(propertiesFilePath);
			options = new Options();
			options.selectedTechnology = Technology.valueOf(strongFetch(props, "technology").toUpperCase());
			options.selectedLearner = Tool.valueOf(strongFetch(props, "learner").toUpperCase());
			loadDelegate(options, "global", props);
			loadDelegate(options, options.selectedTechnology.toString(), props);
			loadDelegate(options, options.selectedLearner.toString(), props);
		}catch(Exception internalException) {
			handleInternal(internalException); 
		}
		return options;
	}
	
	public static void main(String args[]) {
		OptionsReader.loadOptions("sut.properties");
	}
}
