package main;


import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import options.Options;
import util.IJavaUnitCompiler;
import util.JavaUnitCompiler;

/**
 * Compiles and runs the freshly generated setup. Currently, we only run the resulting SUT. For learners like Tomte, which communicate via socket interface, the built sut
 * listens to a port. Tomte should then be run to communicate with the sut/start learning. Future work is to automate this step so that the learner is 
 * started programmatically after the sut is run. 
 */
public class SetupRunner {
	
	private String buildPath;
	private String libPath;
	private boolean isEnabled;
	private String mainClass;

	public SetupRunner(Options options) {
		loadOptions(options);
	}
	
	private void loadOptions(Options options) {
		buildPath = options.globalOptions.buildPath;
		libPath = options.globalOptions.libPath;
		isEnabled = Options.YES.compareToIgnoreCase(options.globalOptions.runSetup) == 0;
		mainClass = options.globalOptions.generatedClassName;
	}
	
	public void runSetup() throws Exception{
		if(isEnabled) {
			launch(mainClass, libPath, buildPath, true);
		}
	}
	
	private void launch(String mainClass, String libPath, String buildPath, boolean isCompiled) throws Exception {
		IJavaUnitCompiler compiler = new JavaUnitCompiler();
		Class<?> compiledMainClass = compiler.compileJavaUnit(mainClass, buildPath, buildPath);
		// parent is null so that we never use the system class loader, only the class loader at the URL
		URLClassLoader classLoader = new URLClassLoader(new URL[]{new File(buildPath).toURI().toURL()}, null);   
		compiledMainClass = classLoader.loadClass(compiledMainClass.getCanonicalName()); // load the right class
		Method main = compiledMainClass.getMethod("main",new String []{}.getClass());
		main.invoke(compiledMainClass, (Object)new String [0]);
		classLoader.close();
	}
}
