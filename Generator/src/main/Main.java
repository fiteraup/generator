package main;

import generator.GeneratorBuilder;
import options.Options;
import options.OptionsReader;

public class Main {
	public static String DEFAULT_PROPERTIES_FILE = "sut.properties";
/**
 * The main generator method. It definitely looks simple and nice. It currently belies the slight mess there is in the generator templates but yeah.
 * Anyway, the tool generates learner setups semi-automatically for given aspects. These aspects are: {@link options.Technology} (Java plain, RMI...) and {@link options.Tool} (Tomte, Simpa...)
 * <p>
 * The main process flow is: 
 * <ul>
 *  <li> information is read from sut.properties file and options are loaded by the OptionsReader class
 *  <li> the builder is instantiated and given the configuration aspects described in the options
 *  <li> the builder constructs the sut on the basis of the parsed options
 *  <li> a setup runner then runs the generated setup
 * </ul> 
 * 
 * <b>Run:</b> java main.Main (property_file_path)?
 * 
 * @throws Exception The system for handling exceptions is pretty simple. Generally, exceptions are caught, reported and then the learning is aborted/ program
 * is terminated. 
 */
	public static void main(String[] args) throws Exception{
		String propFilePath = null;
		if(args.length > 0) {
			propFilePath = args[0];
		} else {
			propFilePath = DEFAULT_PROPERTIES_FILE;
		}
		Options options = OptionsReader.loadOptions(propFilePath);
		GeneratorBuilder builder = new GeneratorBuilder();
		builder.technology(options.selectedTechnology).learner(options.selectedLearner);
		builder.constructSut(options);
		SetupRunner runner = new SetupRunner(options);
		runner.runSetup();
	}
}
