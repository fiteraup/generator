package exceptions;

public class JavaFileDoesNotExistException extends Exception {
	private static final long serialVersionUID = 1L;
	private String interfaceName;
	private String classPath;
	public JavaFileDoesNotExistException (String interfaceName, String classPath) {
		this.interfaceName = interfaceName;
		this.classPath = classPath;
	}
	public String toString() {
		return "Unable to locate interface at path "+interfaceName + " in "+ classPath;
	}
}
