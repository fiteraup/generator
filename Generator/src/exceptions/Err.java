package exceptions;

import generator.Log;
import types.Message;

public class Err {
	public static void handleInterfaceParseException(Class<?> klass) {
		Log.err("Unsupported class hierarchy in "+klass.getSimpleName());
		System.exit(0);
	}
	
	public static void invalidMessageParsed(Message message, String context) {
		Log.err("The message "+message+ " cannot be parsed in a "+context+" context");
		System.exit(0);
	}
	
	public static void handleInternal(Exception e) {
		Log.err("Internal exception ");
		e.printStackTrace();
		System.exit(0);
	}
	
	public static void handleInternal(String exceptionMessage) {
		Log.err("[INTERNAL]:"+exceptionMessage);
		System.exit(0);
	}
	
	public static void handleImplementationError(Enum<?> enumVal, Exception e) {
		Log.err("Functionality for "+enumVal.name() + " has not yet been implemented");
		e.printStackTrace();
		System.exit(0);
	}
	
	//Yaml parser handling
	public static void handleNoFileFound(String filePath) {
		Log.err("File at given path:"+filePath + " does not exist");
		System.exit(0);
	}
	
	public static void handleNotFoundField(String context, String field) {
		Log.err("Required field "+field + " in " + context +" could not be found");
		System.exit(0);
	}
	
	public static void handleNotFoundAttribute(String context, String attribute) {
		Log.err("Required attribute "+attribute + " in " + context +" could not be found");
		System.exit(0);
	}
	
	//Technology template level parsing
	public static void handleParseException(Exception e, String classContext) {
		Log.err("Exception when parsing was generated in "+classContext);
		e.printStackTrace();
		System.exit(0);
	}

	public static void handleInvalidConnection(Message message, boolean isInput) {
		Log.err(message.getName() + " is of the wrong type and cannot be connected as" +
				(isInput?"INPUT":"OUTPUT"));
		System.exit(0);
	}

	public static void handleDuplicateParameter(String parameterName) {
		Log.err("The parameter "+parameterName + " has been defined more than one time");
		System.exit(0);
	}

	public static void handleQueryNotDefined(String queryString) {
		Log.err("The query on the parameter "+queryString+ " has not been defined ");
		System.exit(0);
	}

	public static void handleNotSupported(String unsupportedFeature) {
		Log.err("The feature "+unsupportedFeature + " is unsupported");
		System.exit(0);
	}
}
