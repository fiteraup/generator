package exceptions;

public class PathDoesntPointToJavaFileException extends Exception {
	private static final long serialVersionUID = 1L;
	private String pathName;
	public PathDoesntPointToJavaFileException (String path) {
		this.pathName = path;
	}
	public String toString() {
		return "The path " + pathName + " does not point to a java file. (it should end in .java)";
	}
}
