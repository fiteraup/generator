package sut;

import util.InputAction;
import util.OutputAction;

public interface SutInterface {

	public abstract void reset();

	public abstract OutputAction processSymbol(InputAction methodWrapper);

}