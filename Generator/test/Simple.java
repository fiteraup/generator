package sut;

/**
 * Test class for Tomte and also to see if the generation tool works with plain classes.
 */

public class Simple {
	private final static int UNDEF = -1;
	private final static int SUCCESS = 1;
	private final static int FAILURE = 0;
	private int sessionID = UNDEF;
	private int userID = UNDEF, userPWD = UNDEF;
	private boolean logged = false;

	public Simple() {
		reset();
	}
	
	public void reset() {
		this.logged = false;
	}

	public int logout() {
		if (logged == true) {
			logged = false;
			sessionID = UNDEF;
			return SUCCESS;
		}
		return FAILURE;
	}

	public int login() {
		if (logged == false) {
			logged = true;
			sessionID = 0;//newSessionID();
			return SUCCESS;
		}
		return FAILURE;
	}
	
}