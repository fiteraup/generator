package sut;

/**
 * Test class for Tomte and also to see if the generation tool works with plain classes.
 */

public class Session {
	private final static int UNDEF = -1;
	private final static int SUCCESS = 1;
	private final static int FAILURE = -2;
	private int sessionID = UNDEF;
	private int userID = UNDEF, userPWD = UNDEF;
	private boolean logged = false;

	public Session() {
		reset();
	}
	
	public void reset() {
		this.logged = false;
		this.sessionID = UNDEF;
		this.userID = UNDEF;
		this.userPWD = UNDEF;
	}

	public int logout() {
		if (logged == true) {
			logged = false;
			sessionID = UNDEF;
			return SUCCESS;
		}
		return FAILURE;
	}

	public int register(int userID, int userPWD) {
		if(!isDef(this.userID) && !isDef(this.userPWD)) {
			this.userID = userID;
			this.userPWD = userPWD;
			return SUCCESS;
		}
		return FAILURE;
	}

	public int login(int userID, int userPWD) {
		if (logged == false && this.userID == userID && this.userPWD == userPWD) {
			logged = true;
			sessionID = newSessionID();
			return sessionID;
		}
		return FAILURE;
	}

	public int getResource(int sessionID) {
		if (isDef(sessionID) && this.sessionID == sessionID) {
			return SUCCESS;
		}
		return FAILURE;
	}

	public int newSession() {
		if (sessionID == -1) {
			sessionID = 1;
			return sessionID;
		}
		return FAILURE;
	}

	private boolean isDef(int value) {
		return value != UNDEF;
	}

	private int newSessionID() {
		return (int) (Math.random() * 1000) + 1;
	}
	
	public static void main(String[] args) {
		Session session = new Session();
		int sessionID;
		log(session.login(10, 10));
		log(session.register(10, 10));
		log(sessionID = session.login(10, 10));
		log(session.getResource(100));
		log(session.getResource(sessionID));
		log(session.logout());
		log(session.getResource(sessionID));
	}
	
	private static void log(int msg) {
		System.out.println(msg);
	}
}