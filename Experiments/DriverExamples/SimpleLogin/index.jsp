<http>
	<body>
	<%!
		enum Page {
			LOGIN,
			HOME, 
			PROFILE
		}
		
		class IndexControl {
			HttpServletRequest request = null;
			JspWriter out = null;
			HttpSession session = null;
			String lastAction;
			Page currentPage;
			Page nextPage;
			IndexControl(HttpServletRequest request, JspWriter out, HttpSession session) {
				this.request = request;
				this.out = out;
				this.session = session;
				this.currentPage = Page.LOGIN;
			}
			
			void main() throws Exception {
				String action = request.getParameter("action");
				wr(action);
				lastAction = action;
			}
			
			
			Page login() throws Exception{
				String employee_id = request.getParameter("employee_id");
				String password = request.getParameter("password");
				if (employee_id != null && password != null) {
					if (employee_id.toLowerCase().trim().equals("101") && password.toLowerCase().trim().equals("larry")) 
					{
						session.setAttribute("employee_id", employee_id);
						return Page.HOME;
					}
				}
				return Page.LOGIN;
			}
			
			Page logout() throws Exception{
				session.setAttribute("employee_id",null);
				return Page.LOGIN;
			}
			
			Page showProfile() throws Exception {
				Object employee_id = session.getAttribute("employee_id");
				if(employee_id != null) {
					return Page.PROFILE;
				}
				return Page.HOME;
			}
			
			Page doAction() throws Exception{
				String action = request.getParameter("action");
				Page nextPage = Page.LOGIN;
				if(action == null) {
					nextPage = Page.LOGIN;
				} else if(action.equals("Login")) {
					nextPage = login();
				} else if(action.equals("Logout")) {
					nextPage = logout();
				} else if(action.equals("ShowProfile")) {
					nextPage = showProfile();
				} else if(action.equals("Home")) {
					nextPage = Page.HOME;
				}
				return nextPage;
			}
			
			void wr(String paragraph) throws Exception{
				out.print("<p><b>"+paragraph+"</b></p>");
			}
		}
		
		void view(HttpServletRequest  request,JspWriter out) throws Exception{
			out.println("Login page<br/>");
		}
	%> 
	<% 
		IndexControl ic = new IndexControl(request,out,session);
		switch(ic.doAction()) {
			case HOME:
			%>
			<p>HOME PAGE</p>
			</br>
			<p>Hello <%= session.getAttribute("employee_id") %> </p>
			</br>
			<form id="form1" name="form1" method="post" action="index.jsp">
			<input type="submit" name="action" value="ShowProfile"/>
			<form id="form2" name="form2" method="post" action="index.jsp">
			<input type="submit" name="action" value="Logout"/>
			<%
			break;
			case LOGIN:
			%>
			<p>LOGIN PAGE</p>
			<form id="form1" name="form1" method="post" action="index.jsp">
			    	<label>
			      	<select name="employee_id">
			      	
						<option value="101">Larry Stooge (employee)</option>
						
						<option value="102">Moe Stooge (manager)</option>
					</select>
					</label>
				<br>
			    	<label>Password
			    		<input name="password" type="password" size="10" maxlength="8" />
					</label>
				<br>
					<input type="submit" name="action" value="Login"/>
			</form>
			<%
			break;
			case PROFILE:
			%> 
			<p>PROFILE PAGE</p>
			</br>
			<p>Showing profile for <%= session.getAttribute("employee_id") %> </p>
			<form id="form1" name="form1" method="post" action="index.jsp">
			<input type="submit" name="action" value="Home"/>
			<form id="form2" name="form2" method="post" action="index.jsp">
			<input type="submit" name="action" value="Logout"/>
			</br>
			<%
		}
		
	%>
	
	</body>
</html>