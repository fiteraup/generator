package drivers.efsm.real.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import drivers.efsm.real.LowWebDriver;

import tools.HTTPRequest;
import tools.HTTPRequest.Method;
import tools.HTTPRequest.Version;
import tools.HTTPResponse;
import tools.Utils;
import tools.loggers.LogManager;
import automata.efsm.Parameter;
import automata.efsm.ParameterizedInput;
import automata.efsm.ParameterizedOutput;

public class SimpleLoginDriver extends LowWebDriver {
	
	private String basicAuth = "Basic Z3Vlc3Q6Z3Vlc3Q="; // guest:guest in base64
	private String screen = null;
	
	public SimpleLoginDriver() {
		super();
		this.systemHost = "localhost";
		this.systemPort = 8090;
		initConnection();
	}

	public void initConnection() {
		LogManager.logInfo("Initializing connection to the system");
		//HTTPRequest res = new HTTPRequest(Method.POST, "/sim/index.jsp", Version.v11);
		HTTPRequest res = new HTTPRequest("http://localhost:8090/sim/index.jsp");
		res.addData("action", "Logout");
		res.addHeader("Authorization", basicAuth);
		res.addHeader("Cookie", cookie.getCookieLine());
		//res = new HTTPRequest("http://localhost:8090/sim/index.jsp");
		HTTPResponse response = executeWeb(res);
		cookie.updateCookies(response.getHeader("Set-Cookie"));
		LogManager.logInfo("Ready to infer");
	}

	public ArrayList<String> getInputSymbols() {
		ArrayList<String> is = new ArrayList<String>();
		is.add("Login");
		is.add("Logout");
		is.add("ShowProfile");
		return is;
	}

	public ArrayList<String> getOutputSymbols(){
		ArrayList<String> os = new ArrayList<String>();
		os.add("homePage");
		os.add("loginPage");
		os.add("profilePage");
		return os;
	}
	
	@Override
	public String getSystemName() {
		return "Simple login system";
	}

	public HashMap<String, List<ArrayList<Parameter>>> getDefaultParamValues(){
		HashMap<String, List<ArrayList<Parameter>>> defaultParamValues = new HashMap<String, List<ArrayList<Parameter>>>();		
		ArrayList<ArrayList<Parameter>> params = null;
		
		//Login
		{
			params = new ArrayList<ArrayList<Parameter>>();
			params.add(Utils.createArrayList(new Parameter("intruder", Types.STRING), new Parameter("mock", Types.STRING)));
			params.add(Utils.createArrayList(new Parameter("101", Types.STRING), new Parameter("larry", Types.STRING)));
			defaultParamValues.put("Login", params);		
		}
		
		//Profile
		{
			params = new ArrayList<ArrayList<Parameter>>();
			params.add(Utils.createArrayList(new Parameter("s", Types.STRING)));
			defaultParamValues.put("ShowProfile",params);
		}
		
		//Logout
		{
			params = new ArrayList<ArrayList<Parameter>>();
			params.add(Utils.createArrayList(new Parameter("l", Types.STRING)));
			defaultParamValues.put("Logout",params);
		}
		
		return defaultParamValues;	
	}

	@Override
	public void reset() {
		super.reset();
		//HTTPRequest res = new HTTPRequest(Method.POST, "/sim/index.jsp", Version.v11);
		HTTPRequest res = new HTTPRequest("http://localhost:8090/sim/index.jsp");
		res.addData("action", "Logout");
		cookie.reset();
		executeWeb(res);
	}
	
	public HTTPRequest abstractToConcrete(ParameterizedInput pi){
		HTTPRequest req = null;
		if (!pi.isEpsilonSymbol()){
			LogManager.logInfo("Abstract : " + pi);			
			req = new HTTPRequest(Method.POST, "/sim/index.jsp", Version.v11);
			req.addData("action", pi.getInputSymbol());
			if (pi.getInputSymbol().equals("Login")){
				req.addData("employee_id", pi.getParameterValue(0));
				req.addData("password", pi.getParameterValue(1));
			}
			req.addHeader("Authorization", basicAuth);
			req.addHeader("Cookie", cookie.getCookieLine());
		}else{
			LogManager.logError("AbstractToConcrete for Epsilon symbol is impossible in " + pi.getInputSymbol());
		}
		return req;
	}	
	
	public ParameterizedOutput concreteToAbstract(HTTPResponse resp){
		ParameterizedOutput po = null;
		cookie.updateCookies(resp.getHeader("Set-Cookie"));
		if (resp == null || resp.getCode()==404 || resp.getCode()==503 || resp.getCode()==500){
			po = new ParameterizedOutput();
		}else if (resp.getCode() == 200){
			po = new ParameterizedOutput();
			System.out.println(resp.getContent());
			if(resp.getContent().contains("HOME")) {
				po = new ParameterizedOutput("homePage");
				po.getParameters().add(new Parameter(resp.getCodeString(), Types.STRING));
			} else if (resp.getContent().contains("LOGIN")) {
				po = new ParameterizedOutput("loginPage");
				po.getParameters().add(new Parameter(resp.getCodeString(), Types.STRING));
			} else if (resp.getContent().contains("PROFILE")) {
				po = new ParameterizedOutput("profilePage");
				po.getParameters().add(new Parameter(resp.getCodeString(), Types.STRING));
			}
			else{
				LogManager.logError("ConcreteToAbstract method is missing for this page");
				LogManager.logConcrete(resp.toString());
			}
		}
		 
		LogManager.logInfo("Abstract : " + po);
		return po;		
	}

	@Override
	public TreeMap<String, List<String>> getParameterNames() {
		TreeMap<String, List<String>> defaultParamNames = new TreeMap<String, List<String>>();
		defaultParamNames.put("Login", Utils.createArrayList("employee_id", "password"));
		defaultParamNames.put("ShowProfile", Utils.createArrayList("showProfile1"));
		defaultParamNames.put("Logout", Utils.createArrayList("logout1"));
		
		defaultParamNames.put("homePage", Utils.createArrayList("codeHome"));
		defaultParamNames.put("loginPage", Utils.createArrayList("codeLogin"));
		defaultParamNames.put("profilePage", Utils.createArrayList("codeProfile"));
		return defaultParamNames;
	}
}
