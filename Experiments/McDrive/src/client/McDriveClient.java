package client;

import java.rmi.Naming;
import java.rmi.RMISecurityManager;

import pub.McDriveServer;

public class McDriveClient 
{
	
	private static boolean checkException(Exception e, String message) {
		return e.getMessage().toLowerCase().contains(message.toLowerCase());
	}

	public static void main ( String [] args)
	{
		try {
			
			McDriveServer server = (McDriveServer) Naming.lookup("//localhost:2020/McDriveServer");
			server.IPay();
			server.IPay();
			server.IBuy();
			server.IBuy();
			server.IBuy();
			System.out.println("Success "+server);
		} 
		catch(Exception e)
		{
			System.err.println(checkException(e,"OInsufficient"));
		}
	}
}
