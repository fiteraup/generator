package sut;

import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.util.ArrayList;
import java.util.List;

import sut.SutInterface;
import sut.Main;
import pub.McDriveServer;
import abslearning.trace.InputAction;
import abslearning.trace.IntegerParamValue;
import abslearning.trace.OutputAction;
import abslearning.trace.Parameter;


public class Sut implements SutInterface{

	private McDriveServer server = null;
	
	public Sut() 
	{
		if (System.getSecurityManager() == null) {
			System.setProperty("java.security.policy", "drive.policy");
			System.setSecurityManager(new RMISecurityManager());
			System.setSecurityManager(null);
		}
		try {
			server = (McDriveServer) Naming
					.lookup("//localhost:2020/McDriveServer");
			//server.reset();
			server.reset();
		} catch (Exception e) {
			System.err.println("[McDriveClient] Error: " + e.getMessage());
		}
	}
	// private URL url;
	// private HttpURLConnection conn;

	// wrapper to simplify some long writing
	private static Integer getIntParm(List<Parameter> params, int pos) {
		return ((IntegerParamValue) params.get(pos).getConcreteValue())
				.getValue().intValue();
	}
	
	private static boolean checkException(Exception e, String message)
	{
		return e.getMessage().toLowerCase().contains(message.toLowerCase());
	}

	public void reset() {

		try {
			server.reset();
		} catch (Exception e) {
			System.err.println("[McDriveClient] Error: " + e.getMessage());
		}
	}

	public OutputAction processSymbol(InputAction methodWrapper) {
		String msgName = methodWrapper.getMethodName();
		System.out.println("Received " + msgName);
		List<Parameter> params = methodWrapper.getParameters();
		String response = "OOK";
		List<Parameter> result = new ArrayList<Parameter>();
		try{
			switch(msgName) {
			case "IPay":
				if(params.size() == 0) {
					server.IPay();
				} 
				else {
					int amount = getIntParm(params, 0);
					server.IPay(amount);
				}
				break;
			case "IBuy":
				try {
					if(params.size() == 0) {
						server.IBuy();
					} 
					else {
						int amount = getIntParm(params, 0);
						server.IBuy(amount);
					}
				} 
				catch(Exception e) {
					if(checkException(e,"OInsufficient"))
					{
						response = "OInsufficient";
					}
					else
					{
						throw e;
					}
				}
				break;
			case "IBuyForId":
				int id = getIntParm(params, 0);
				try {
					server.IBuyForId(id);
				} 
				catch(Exception e) {
					if(checkException(e,"OInsufficient"))
					{
						response = "OInsufficient";
					}
					else
					{
						throw e;
					}
				}
				break;
			case "IPayForId":
				id = getIntParm(params, 0);
				server.IPayForId(id);
				break;
			default:
				response = "OIllegal";
				break;
			}
		}
		catch(Exception e) {
			response = "OError";
		}
		System.out.println("Outputed " + response);
		return new OutputAction(response, result);
	}
	
	public static void main(String args []) {
		Main.run(args, new Sut());
	}

}
