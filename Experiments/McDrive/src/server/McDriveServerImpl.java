package server;

import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import pub.McDriveServer;


public class McDriveServerImpl extends UnicastRemoteObject implements McDriveServer{
	
	private static final long serialVersionUID = 1L;
	private static final int port = 2020;
	private int balance = 0;
	private final int limit = 2;
	private final List<Integer> idList = new ArrayList<Integer>();
	private int id = Integer.MIN_VALUE;
	
	public McDriveServerImpl() throws RemoteException 
	{
		super();
	}

	public void IBuy() throws RemoteException {
		if(balance > 0) {
			System.out.println("bought unit");
			balance --;
		} else {
			System.out.println("out of money");
			throw new RemoteException("OInsufficient");
		}
	}
	
	public void IPay() throws RemoteException {
		if (balance < limit) {
			System.out.println("paid unit");
			balance ++;
		} else {
			System.out.println("pay saturation");
		}
	}
	
	public void IBuyForId(int id) throws RemoteException {
		if(idList.contains(id)) {
			idList.remove(new Integer(id));
		}
		else {
			throw new RemoteException("OInsufficient");
		}
	}
	
	public void IPayForId(int id) throws RemoteException {
		if(!idList.contains(id)) {
			idList.add(new Integer(id));
		}
	}
	
	public void IBuy(int amount) throws RemoteException {
		if (amount >= 0 && amount < balance) {
			System.out.println("bought "+ amount + " amount");
			balance = balance - amount;
		}
		else
		{
			System.out.println("out of money");
			throw new RemoteException("OInsufficient");
		}
	}
	
	public void IPay(int amount) throws RemoteException {
		System.out.println("paid "+ amount + " amount");
		if(amount > 0)
		{
			balance = balance + amount;
			if (balance > limit)
			{
				balance = limit;
			}
		}
	}
	
	public void reset() throws RemoteException {
		System.out.println("reset");
		idList.clear();
		id = Integer.MIN_VALUE;
		balance = 0;
	}

	
	public static void main (String [] args)
	{
		File file = new  File("drive.policy");
		System.out.println(file.exists());
		try 	
		{
			Registry registry = LocateRegistry.createRegistry(port);
			McDriveServer server = new McDriveServerImpl();
			registry.rebind("McDrive" +
					"Server", server);
			System.out.println("McDriveServer started.");
		} 
		catch (Exception e)
		{
			System.err.println("[McDriveServer]Error: "+ e.getMessage());
		}
	}
}
