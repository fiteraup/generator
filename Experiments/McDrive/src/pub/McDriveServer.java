package pub;
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface McDriveServer extends Remote {
	public void IPay (int amount) throws RemoteException;
	public void IBuy (int amount) throws RemoteException;
	public void IPay () throws RemoteException;
	public void IBuy () throws RemoteException;
	public void IPayForId (int id) throws RemoteException;
	public void IBuyForId (int id) throws RemoteException;
	public void reset() throws RemoteException;
}
