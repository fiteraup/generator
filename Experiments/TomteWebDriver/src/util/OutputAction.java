package util;

import java.util.Arrays;
import java.util.List;

public class OutputAction extends Action {

	public OutputAction(String methodName, List<Parameter> parameters) {
		super(methodName, parameters);
	}
	
	public OutputAction(String methodName, Parameter parameter) {
		super(methodName, Arrays.asList(parameter));
	}

	public OutputAction(Action action) {
		super(action);
	}
	
	public OutputAction(String action) {
		super(action);
	}
}
