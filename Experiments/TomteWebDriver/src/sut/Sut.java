
package sut;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import tools.CookieManager;
import tools.HTTPRequest;
import tools.HTTPResponse;
import tools.TCPSend;
import tools.loggers.LogManager;
import util.InputAction;
import util.OutputAction;
import util.Parameter;

public class Sut
    implements SutInterface
{

    private String basicAuth = "Basic Z3Vlc3Q6Z3Vlc3Q=";
    private CookieManager cookieManager = new CookieManager();
    private int systemPort = 8080;
    private String systemHost = "localhost";
    private String screen;
    private String restart;
    private static HashMap<Integer, String> getStringMap = new HashMap<Integer, String>();
    private static HashMap<String, Integer> mapStringMap = new HashMap<String, Integer>();

    public Sut() {
        this.systemHost = "localhost";
        this.systemPort = 8080;
        initConnection();
    }

    private int indexOf(String regex, String string) {
        int pos = -1;
        Matcher matcher = Pattern.compile(regex).matcher(string);
        if (matcher.find()) {
            pos = matcher.start();
        }
        return pos;
    }

    private String queryResponse(String content, String paramName, List<String> hints) {
        String value = "";
        int poz;
        for (String hint: hints) {
            poz = content.indexOf(hint);
            content = content.substring((poz + hint.length()));
            if (poz == -1) {
                return "";
            }
        }
        poz = content.indexOf((paramName +"="));
        if (poz == -1) {
            return "";
        }
        content = content.substring(((poz + paramName.length())+ 1));
        poz = indexOf("\"|&|>", content);
        if (poz == -1) {
            return "";
        }
        value = content.substring(0, poz);
        return value;
    }

    public void initConnection() {
        LogManager.logInfo("Initializing connection to the system");
        HTTPRequest request = null;
        HTTPResponse response = null;
        request = new HTTPRequest(HTTPRequest.Method.GET, "/WebGoat/attack", HTTPRequest.Version.v11);
        request.addHeader("Authorization", this.basicAuth);
        request.addHeader("Cookie", this.cookieManager.getCookieLine());
        response = sendRequest(request);
        this.cookieManager.updateCookies(response.getHeader("Set-Cookie"));
        request = new HTTPRequest(HTTPRequest.Method.POST, "/WebGoat/attack", HTTPRequest.Version.v11);
        request.addData("start", "Start Webgoat");
        request.addHeader("Authorization", this.basicAuth);
        request.addHeader("Cookie", this.cookieManager.getCookieLine());
        response = sendRequest(request);
        screen = queryResponse(response.toString(), "Screen", Arrays.asList("Stage 1: Stored XSS"));
        restart = queryResponse(response.toString(), "Screen", Arrays.asList("Stage 1: Stored XSS"));
        this.cookieManager.updateCookies(response.getHeader("Set-Cookie"));
        LogManager.logInfo("Ready to infer");
    }

    private HTTPResponse sendRequest(HTTPRequest request) {
        String link = TCPSend.Send("localhost", 8080, request);
        return new HTTPResponse(link);
    }

    public void reset() {
        HTTPRequest request = null;
        HTTPResponse response = null;
        request = new HTTPRequest(HTTPRequest.Method.GET, "/WebGoat/attack", HTTPRequest.Version.v11);
        request.addData("menu", "900");
        request.addData("Screen", screen);
        request.addData("Restart", restart);
        request.addHeader("Authorization", this.basicAuth);
        request.addHeader("Cookie", this.cookieManager.getCookieLine());
        response = sendRequest(request);
        this.cookieManager.updateCookies(response.getHeader("Set-Cookie"));
    }

    public OutputAction processSymbol(InputAction pi) {
        HTTPRequest req = defaultAbstractToConcrete(pi);
        HTTPResponse response = sendRequest(req);
        OutputAction po = defaultConcreteToAbstract(response);
        return po;
    }

    private HTTPRequest defaultAbstractToConcrete(InputAction pi) {
        HTTPRequest req = null;
        LogManager.logInfo(("Abstract :"+ pi));
        switch (pi.getMethodName()) {
            case "ILOGIN":
                req = new HTTPRequest(HTTPRequest.Method.POST, "/WebGoat/attack", HTTPRequest.Version.v11);
                req.addData("employee_id", this.getId(this.getIntParam(pi, 0)));
                req.addData("password", this.getPwd(this.getIntParam(pi, 1)));
                req.addData("action", "Login");
                req.addData("menu", "900");
                req.addData("Screen", screen);
                break;
            case "IVIEWPROFILE":
                req = new HTTPRequest(HTTPRequest.Method.POST, "/WebGoat/attack", HTTPRequest.Version.v11);
                req.addData("employee_id", this.getString(this.getIntParam(pi, 0)));
                req.addData("action", "ViewProfile");
                req.addData("menu", "900");
                req.addData("Screen", screen);
                break;
            case "IEDITPROFILE":
                req = new HTTPRequest(HTTPRequest.Method.POST, "/WebGoat/attack", HTTPRequest.Version.v11);
                req.addData("employee_id", this.getId(this.getIntParam(pi, 0)));
                req.addData("action", "EditProfile");
                req.addData("menu", "900");
                req.addData("Screen", screen);
                break;
            case "IXSSPROFILE":
                req = new HTTPRequest(HTTPRequest.Method.POST, "/WebGoat/attack", HTTPRequest.Version.v11);
                req.addData("employee_id", this.getId(this.getIntParam(pi, 0)));
                req.addData("address1", this.getString(this.getIntParam(pi, 1)));
                req.addData("action", "UpdateProfile");
                req.addData("menu", "900");
                req.addData("address2", "New York, NY");
                req.addData("ccn", "2578546969853547");
                req.addData("ccnLimit", "5000");
                req.addData("description", "Does not work well with others");
                req.addData("disciplinaryDate", "10106");
                req.addData("disciplinaryNotes", "Constantly harassing coworkers");
                req.addData("firstName", "Larry");
                req.addData("lastName", "Stooge");
                req.addData("manager", "101");
                req.addData("phoneNumber", "443-689-0192");
                req.addData("salary", "55000");
                req.addData("ssn", "386-09-5451");
                req.addData("startDate", "1012000");
                req.addData("title", "Technician");
                req.addData("Screen", screen);
                break;
            case "ILOGOUT":
                req = new HTTPRequest(HTTPRequest.Method.POST, "/WebGoat/attack", HTTPRequest.Version.v11);
                req.addData("employee_id", this.getId(this.getIntParam(pi, 0)));
                req.addData("action", "Logout");
                req.addData("menu", "900");
                req.addData("Screen", screen);
                break;
            default:
                LogManager.logError(("AbstractToConcrete method is missing for symbol:"+ pi.getMethodName()));
        }
        if (req!= null) {
            req.addHeader("Authorization", this.basicAuth);
            req.addHeader("Cookie", this.cookieManager.getCookieLine());
        } else {
            LogManager.logError(("Abstract to concrete is undefined for "+ pi));
        }
        return req;
    }
    
    List<Integer> ids = Arrays.asList(10);
    private String getId(Integer intValue) {
    	if(ids.contains(intValue)) {
    		return "101";
    	} else {
    		return "666";
    	}
    }
    
    private String getPwd(Integer intValue) {
    	if(ids.contains(intValue)) {
    		return "larry";
    	} else {
    		return "cramer";
    	}
    }

    private String getString(Integer intValue) {
        String stringValue = null;
        if (getStringMap.containsKey(intValue)) {
            stringValue = getStringMap.get(intValue);
        } else {
            stringValue = (""+ intValue);
            getStringMap.put(intValue, stringValue);
        }
        return stringValue;
    }

    private int getIntParam(InputAction params, int pos) {
        Integer integerValue = params.getParameters().get(pos).getValue();
        return integerValue;
    }

    private OutputAction defaultConcreteToAbstract(HTTPResponse resp) {
        OutputAction po = null;
        Parameter param = null;
        this.cookieManager.updateCookies(resp.getHeader("Set-Cookie"));
        if ((resp == null)||(resp.getCode()!= 200)) {
            po = new OutputAction("OERROR");
        } else {
            if (resp.getContent().contains("Staff Listing Page")) {
                param = new Parameter(mapString(resp.getCodeString()), 0);
                po = new OutputAction("OLISTING", param);
            } else {
                if (resp.getContent().contains("<div id=\"lesson_login\">")) {
                    param = new Parameter(mapString(resp.getCodeString()), 0);
                    po = new OutputAction("OHOME", param);
                } else {
                    if (resp.getContent().contains("UpdateProfile")) {
                        param = new Parameter(mapString(resp.getCodeString()), 0);
                        po = new OutputAction("OEDITIONPAGE", param);
                    } else {
                        if (resp.getContent().contains("Credit Card Limit")) {
                            param = new Parameter(mapString(resp.getCodeString()), 0);
                            po = new OutputAction("OPROFILEPAGE", param);
                        } else {
                            LogManager.logInfo("Concrete to abstract information is missing");
                        }
                    }
                }
            }
        }
        LogManager.logInfo(("Abstract :"+ po));
        return po;
    }

    private Integer mapString(String stringValue) {
//        Integer intValue = null;
//        if (!mapStringMap.containsKey(stringValue)) {
//            intValue = mapStringMap.size();
//            intValue = intValue ++;
//            mapStringMap.put(stringValue, intValue);
//        } else {
//            intValue = mapStringMap.get(stringValue);
//        }
        return 0;//intValue;
    }

}
