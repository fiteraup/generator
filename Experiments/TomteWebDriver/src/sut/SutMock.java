package sut;

import java.util.Arrays;
import java.util.List;

import util.InputAction;
import util.OutputAction;
import util.Parameter;

public class SutMock implements SutInterface{
	private static class Page {
		public static String HOME = "OHOME";
		public static String LISTING = "OLISTING";
		public static String EDIT = "OEDITIONPAGE";
	}
	
	boolean logged;
	List<Integer> ids = Arrays.asList(11,12);
	public void reset() {
		logged = false;
	}

	@Override
	public OutputAction processSymbol(InputAction input) {
		String method = input.getMethodName();
		OutputAction output = null;
		switch(method) {
		case "ILOGIN":
			int selectedId = getIntParam(input,0);
			int selectedPwd = getIntParam(input,1);
			if(ids.contains(selectedId) && selectedId == selectedPwd) {
				logged = true;
				output = new OutputAction(Page.LISTING,new Parameter(0,0));
			} else {
				output = new OutputAction(Page.HOME,new Parameter(0,0));
			}
			break;
		case "ILOGOUT":
			logged = false;
			output = new OutputAction(Page.HOME,new Parameter(0,0));
			break;
		case "IEDITPROFILE":
			output = logged ? new OutputAction(Page.EDIT,new Parameter(0,0)) : new OutputAction(Page.HOME,new Parameter(0,0));
			break;
		}
		System.out.println("----------------------");
		System.out.println(input.getValuesAsString());
		System.out.println(output.getValuesAsString());
		System.out.println("----------------------");
		return output;
	}
	
    private int getIntParam(InputAction params, int pos) {
        Integer integerValue = params.getParameters().get(pos).getValue();
        return integerValue;
    }


}
