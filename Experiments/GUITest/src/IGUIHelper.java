import java.awt.LayoutManager;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;


public interface IGUIHelper {

	public JPanel createPanel(LayoutManager lm);

	public JTextField createTextField(String text);

	public JTextField createTextField(int number);

	public JLabel createLabel(String label);
	
	public <T> JList<T> createList(List<T> list);
	
	public <T> JComboBox<T> createComboBox(List<T> list);
	
}