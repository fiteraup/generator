import java.awt.LayoutManager;
import java.util.List;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class GUIHelper implements IGUIHelper {
	public JPanel createPanel(LayoutManager lm) {
		return new JPanel(lm);
	}
	
	public JTextField createTextField(String text) {
		return new JTextField(text);
	}
	
	public JTextField createTextField(int number) {
		return new JTextField(number);
	}

	public JLabel createLabel(String label) {
		return new JLabel(label);
	}

	public <T> JList<T> createList(List<T> list) {
		JList<T> jList = new JList<T>();
		jList.setListData(new Vector<T>(list));
		return jList;
	}
	
	public <T> JComboBox<T> createComboBox(List<T> options) {
		JComboBox<T> jComboBox = new JComboBox<T>();
		for(T option : options) {
			jComboBox.addItem(option);
		}
		return jComboBox;
	}
}
