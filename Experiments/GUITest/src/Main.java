import java.util.Arrays;
import java.util.List;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import net.miginfocom.swing.MigLayout;
import net.miginfocom.layout.LayoutUtil;

public class Main {
	private static List<String> learners = Arrays.asList("TOMTE","SIMPA");
	private static List<String> technologies = Arrays.asList("RMI","HTTP","WSDL");
	
	public static void main(String args[]) {
		IGUIHelper gui = new GUIHelper();
		MigLayout migLayout = new MigLayout("","[]10[]10","[]10[]");
		JPanel p = gui.createPanel(migLayout);
		JComboBox<String> selectLearner = gui.createComboBox(learners);
		JComboBox<String> selectTechnology = gui.createComboBox(technologies);
		migLayout.addLayoutComponent(selectTechnology, "wrap");
		migLayout.addLayoutComponent(selectLearner, "wrap");
		p.setVisible(true);
		
		JFrame frame = new JFrame();
		frame.add(p);
		frame.setVisible(true);
	}
}
